from flask import Blueprint,request,current_app
from api.models.db import db
from api.models.user import User
from api.models.project import Project
from api.models.worksheet import Bug
from api.models.transform import model_to_dict
from api.common.util import handle_input,string_to_list,list_to_string

import json
from .auth import verify_token
import re

bug = Blueprint('bug',__name__, url_prefix='/flow/bug',template_folder='templates')

@bug.route('/bug_add',methods={'GET','POST'})
def bug_add():
    error = None
    if request.method == 'POST':
        data = json.loads(request.get_data().decode('utf-8'))
        current_app.logger.debug('bug.add收到的数据:{}'.format(data))
        bug = data.get('bug')
        project = bug.get('project')
        env = bug.get('env')
        service_list = bug.get('service_list')
        service_to_string = ",".join(service_list)
        name = bug.get('name')
        user = request.headers.get('username')

        try:
            item = Bug(
                project=project,
                env=env,
                service_list=service_to_string,
                name=name,
                create_user=user,
                status='创建'
            )
            db.session.add(item)
            db.session.commit()
            return json.dumps({"ok":"添加成功"})
        except Exception as e:
            current_app.logger.error(e)
            error = '未知异常'+str(e)  
    return json.dumps({"fail":error})

@bug.route('/bug_update', methods=('GET', 'POST'))
def bug_update():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('bug.update收到的数据:{}'.format(data))
    bug = data.get('bug')
    id = bug.get('id')
    project = bug.get('project')
    env = bug.get('env')
    service_list = bug.get('service_list')
    service_to_string = ",".join(service_list)
    name = bug.get('name')
    content = bug.get('content')
    status = bug.get('status')

    try:
        Bug.query.filter(Bug.id==id).update({'project':project,'service_list':service_to_string,'env':env,'name':name,'content':content,'status':status})
        db.session.commit()
        return json.dumps({"ok":"更新成功"})
    except Exception as e:
        return json.dumps({"fail":"更新失败"})

@bug.route('/bug_list',methods={'GET','POST'})
@verify_token
def bug_list():
    sql = db.session.query(Bug).order_by(Bug.create_time.desc())
    bug_list = []
    results = sql.all()  
    if len(results) >0:
        for item in results:
            bug = model_to_dict(item)
            bug['service_list'] = string_to_list(item.service_list)
            bug_list.append(bug)
    return json.dumps(bug_list)

#查询未完成的需求
@bug.route('/not_finished_bug',methods={'GET','POST'})
def not_finished_bug():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('not_finished_bug收到的数据:{}'.format(data))
    project = data.get('project')
    results = db.session.query(Bug.id,Bug.name) \
        .filter(Bug.status != '已修复',Bug.project == project) \
            .all()
    nf_bug_list = []
    if len(results) >0:
        for result in results:
            item = {
                "id": result.id,
                "name": result.name
            }
            nf_bug_list.append(item)
        return json.dumps(nf_bug_list)
    else:
        return json.dumps({"fail":"获取bug失败"})

def get_bug_by_id(ids):
    if len(ids) > 0:
        results = db.session.query(Bug.id,Bug.name).filter(Bug.id.in_(ids)).all()
        bug_list = []
        if len(results) >0:
            for result in results:
                item = {
                    "id": result.id,
                    "name": result.name
                }
                bug_list.append(item)
        return bug_list
    else:
        return []
