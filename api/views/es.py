# https://elasticsearch-py.readthedocs.io/en/latest/
# https://www.cnblogs.com/shaosks/p/7592229.html
from flask import Blueprint, request,current_app
import json
from datetime import datetime
from api.common.util import  utc_to_local,string_to_list
from api.models.db import db
from api.models.es import Es_Cluster
from api.controllers.es import  Elastic,TrafficStat
import re
# from api.controllers.email import send_mail

es = Blueprint('es',__name__,url_prefix='/admin/es')
# 设置项目nginx入口索引的名字
api_index = "prod-proxy-nginx-*"
nginx_index = "logstash-*"

def result_to_list(obj):
    lists = []
    for item in obj:
        elem = item[0]
        lists.append(elem)
    return lists

#纯数据库查询管理，不需要封装
@es.route('/get_es_name_list',methods=('GET','POST'))
def get_es_name_list():
    results = db.session.query(Es_Cluster.name).order_by(Es_Cluster.create_time.desc()).all()
    es_cluster_names = result_to_list(results)
    current_app.logger.debug(es_cluster_names)
    return json.dumps(es_cluster_names)

@es.route('/get_es_index_list',methods=('GET','POST'))
def get_es_index_list():
    try:
        cluster_name = request.headers.get('es_cluster_name')
        client = Elastic(cluster_name)
        indices = client.index_list()
        return json.dumps(indices, indent=4)
    except Exception as e:
        print(e)
        return json.dumps({"fail":"获取es索引列表失败"})

def checkIndex(name):
    '''
    curl -XDELETE 'http://host.IP.address:9200/logstash-*'
    curl -XDELETE http://192.168.11.51:30272/test-flask-k8s_2020.11
    判断name是否是当月，或者是当天的不能删除
    m = pattern.match('ll-aa-2020.11.12')
    print(m.groups())
    ('-', '.', '11', '.', '12')

    m = pattern.match('ll-aa-2020.11')
    print(m.groups())
    ('-', '.', '11', None, '')
    '''
    #pattern = re.compile(r'\S+(\-|\_)[1-9][0-9]{3}(\.|\-)([0-9]{2})(\.|\-)?([0-9]{0,2})$')
    pattern = re.compile(r'\S+(\-|\_)[1-9][0-9]{3}(\.|\-)?([0-9]{2})(\.|\-)?([0-9]{0,2})$')
    m = pattern.match(name)
    # print(len(m.groups()))
    # print(m.group(0),m.group(1),m.group(2),m.group(3),m.group(4),m.group(5))
    # groupNum = len(m.groups())
    month = 3
    day = 5
    # if groupNum == 5:
    #     month = 3
    #     day = 5
    # else:
    #     print("模式匹配失败")
    #     return False
    if m == None:
        return False
    # 只含月份，至少保留当月数据
    elif m.group(month) !=''and  m.group(day)=='' :
        month = int(m.group(month))
        current_month = datetime.now().month
        print(month,current_month)
        if month == current_month:
            return False
    # 含天，至少保留7天数据
    elif m.group(month) !=''and  m.group(day)!='':
        day = int(m.group(day))
        current_day = datetime.now().day
        if day > current_day - 7:
            return False
    return True

@es.route('/delete_multi_index', methods=('GET', 'POST'))
def delete_multi_index():
    try:
        cluster_name = request.headers.get('es_cluster_name')
        data = json.loads(request.get_data().decode('utf-8'))
        indices = data['index_list']
        client = Elastic(cluster_name)
        not_allow_list = [] #(没有时间，当月或者近7天数据不允许删除)
        success_list = []
        fail_list = []
        for idx in indices:
            if not checkIndex(idx):
                not_allow_list.append(idx)
            else:
                if(client.delete_index(idx)):
                    success_list.append(idx)
                else:
                    fail_list.append(idx)
        result = {
            "success_list": success_list,
            "not_allow_list":not_allow_list,
            "fail_list": fail_list
        }
        return json.dumps(result,indent=4)
    except Exception as e:
        print(e)

    return json.dumps({"fail":"删除索引失败"})

@es.route("/get_mapping",methods=('GET','POST'))
def get_mapping():
    cluster_name = request.headers.get('es_cluster_name')
    data = json.loads(request.get_data().decode("utf-8"))
    index_name = data.get('index_name')
    client = Elastic(cluster_name)
    result = client.get_index_mapping(idx=index_name)
    print(result)
    return json.dumps(result)

@es.route("/get_response_stat",methods=('GET','POST'))
def get_response_stat():
    cluster_name = request.headers.get('es_cluster_name')
    data = json.loads(request.get_data().decode("utf-8"))
    from_time = data.get('from')
    to_time = data.get('to')
    body = {
        "_source":"response",
        "query":{
            "bool":{
                "must":{"match_all":{}},
                "filter":{
                    "range":{
                        "@timestamp":{
                            "gte":from_time,
                            "lte":to_time
                        }
                    }
                }
            }
        },
        "size":0,
        "aggs":{
            "group_by_response":{
                "terms":{
                    "field": "response.keyword"
                }
            }
        }
    }
    client = Elastic(cluster_name)
    res = client.search_data(idx=api_index,body=body)
    # {'took': 0, 'timed_out': False, '_shards': {'total': 1, 'successful': 1, 'skipped': 0, 'failed': 0},
    #  'hits': {'total': {'value': 106, 'relation': 'eq'}, 'max_score': None, 'hits': []}, 'aggregations': {
    #     'group_by_response': {'doc_count_error_upper_bound': 0, 'sum_other_doc_count': 0,
    #                           'buckets': [{'key': '200', 'doc_count': 86}, {'key': '500', 'doc_count': 13},
    #                                       {'key': '499', 'doc_count': 3}, {'key': '503', 'doc_count': 3},
    #                                       {'key': '401', 'doc_count': 1}]}}}
    total = res['hits']['total']['value']
    data = res['aggregations']['group_by_response']['buckets']
    sorted_data = sorted(data,key=lambda t:t['doc_count'],reverse=False)
    status_code_list = list(map(lambda x: x['key'],sorted_data))
    dataset = list(map(lambda  x: [x['key'],x['doc_count'],format(x['doc_count']/total*100,'.0f')],sorted_data))
    print(dataset)
    result = {
        'total':total,
        'dataset':dataset,
        "status_code_list":status_code_list,
    }
    return json.dumps(result,indent=4)

@es.route("/get_api_stat",methods=('GET','POST'))
def get_api_stat():
    cluster_name = request.headers.get('es_cluster_name')
    data = json.loads(request.get_data().decode("utf-8"))
    from_time = data.get('from')
    to_time = data.get('to')
    body = {
        "_source":"response",
        "query":{
            "bool":{
                "must":{"match_all":{}},
                "filter":{
                    "range":{
                        "@timestamp":{
                            "gte":from_time,
                            "lte":to_time
                        }
                    }
                }
            }
        },
        "size":0,
        "aggs":{
            "group_by_request":{
                "terms":{
                    "field": "request.keyword"
                }
            }
        }
    }
    client = Elastic(cluster_name)
    res = client.search_data(idx=api_index,body=body)

    total = res['hits']['total']['value']
    data = res['aggregations']['group_by_request']['buckets'][:10]
    sorted_data = sorted(data,key=lambda t:t['doc_count'],reverse=False)
    api_list = list(map(lambda x: x['key'],sorted_data))

    dataset = list(map(lambda  x: [x['key'],x['doc_count'],format(x['doc_count']/total*100,'.0f')],sorted_data))
    print(dataset)
    result = {
        'total':total,
        'dataset':dataset,
        "api_list":api_list,
    }
    return json.dumps(result,indent=4)

@es.route("/get_request_stat",methods=('GET','POST'))
def get_request_stat():
    cluster_name = request.headers.get('es_cluster_name')
    data = json.loads(request.get_data().decode("utf-8"))
    from_time = data.get('from')
    to_time = data.get('to')
    # print(from_time,to_time)
    body = {
        "query":{
            "bool":{
                "must":{"match_all":{}},
                "filter":{
                    "range":{
                        "@timestamp":{
                            "gte":from_time,
                            "lte":to_time
                        }
                    }
                }
            }
        },
        "size":0,
        "aggs":{
            "request_over_time":{
                "date_histogram":{
                    "field": "@timestamp",
                    "fixed_interval": "30m"
                }
            }
        }
    }
    client = Elastic(cluster_name)
    res = client.search_data(idx=api_index,body=body)

    total = res['hits']['total']['value']
    data = res['aggregations']['request_over_time']['buckets']
    dataset =  list(map(lambda x:[utc_to_local(x['key_as_string']),x['doc_count']],data))
    # print(dataset)
    result = {
        'total':total,
        'dataset':dataset,
    }
    return json.dumps(result,indent=4)

@es.route("/get_client_stat",methods=('GET','POST'))
def get_client_stat():
    cluster_name = request.headers.get('es_cluster_name')
    data = json.loads(request.get_data().decode("utf-8"))
    from_time = data.get('from')
    to_time = data.get('to')
    body = {
        "_source":"clientip",
        "query":{
            "bool":{
                "must":{"match_all":{}},
                "filter":{
                    "range":{
                        "@timestamp":{
                            "gte":from_time,
                            "lte":to_time
                        }
                    }
                }
            }
        },
        "size":0,
        "aggs":{
            "group_by_clientip":{
                "terms":{
                    "field": "clientip.keyword"
                }
            }
        }
    }
    client = Elastic(cluster_name)
    res = client.search_data(idx=api_index,body=body)
    total = res['hits']['total']['value']
    data = res['aggregations']['group_by_clientip']['buckets'][:10]
    sorted_data = sorted(data,key=lambda t:t['doc_count'],reverse=False)
    clientip_list = list(map(lambda x: x['key'],sorted_data))

    dataset = list(map(lambda  x: [x['key'],x['doc_count'],format(x['doc_count']/total*100,'.0f')],sorted_data))
    # print(dataset)
    result = {
        'total':total,
        'dataset':dataset,
        "clientip_list":clientip_list,
    }
    return json.dumps(result,indent=4)

def format_float(f, num):
    '''
    :param f:
    :param num: 保留小数位数
    :return:
    '''
    if f == None:
        return 0
    else:
        str="%.{}f".format(num)
        return  float(str % f)

@es.route("/get_requesttime_stat",methods=('GET','POST'))
def get_requesttime_stat():
    cluster_name = request.headers.get('es_cluster_name')
    data = json.loads(request.get_data().decode("utf-8"))
    from_time = data.get('from')
    to_time = data.get('to')
    body = {
        "_source":"clientip",
        "query":{
            "bool":{
                "must":{"match_all":{}},
                "filter":{
                    "range":{
                        "@timestamp":{
                            "gte":from_time,
                            "lte":to_time
                        }
                    }
                }
            }
        },
        "size":0,
        "aggs":{
            "requesttime_percentiles":{
                "percentiles":{
                    "field": "requesttime"
                }
            }
        }
    }
    client = Elastic(cluster_name)
    res = client.search_data(idx=api_index,body=body)

    total = res['hits']['total']['value']
    values = res['aggregations']['requesttime_percentiles']['values']
    data = []
    for key in values:
        print(key,values[key])
        data.append((float(key),values[key]))
    # print(data)
    # bug 数据为none，TypeError: must be real number, not NoneType
    # [(1.0, None), (5.0, None), (25.0, None), (50.0, None), (75.0, None), (95.0, None), (99.0, None)]
    t_data = list(map(lambda x:[100-x[0],format_float(x[1],4)],data))
    result = {
        'total':total,
        'dataset':t_data,
    }
    return json.dumps(result,indent=4)


# @es.route("/get_requesttime_ranks",methods=('GET','POST'))
# def get_requesttime_ranks():
#     cluster_name = request.headers.get('es_cluster_name')
#     data = json.loads(request.get_data().decode("utf-8"))
#     from_time = data.get('from')
#     to_time = data.get('to')
#     body = {
#         "_source":"clientip",
#         "query":{
#             "bool":{
#                 "must":{"match_all":{}},
#                 "filter":{
#                     "range":{
#                         "@timestamp":{
#                             "gte":from_time,
#                             "lte":to_time
#                         }
#                     }
#                 }
#             }
#         },
#         "size":0,
#         "aggs":{
#             "requesttime_ranks":{
#                 "percentile_ranks":{
#                     "field": "requesttime",
#                     "values":[1,3]
#                 }
#             }
#         }
#     }
#     client = Elastic(cluster_name)
#     res = client.search_data(idx=api_index,body=body)
#
#     total = res['hits']['total']['value']
#     values = res['aggregations']['requesttime_ranks']['values']
#     print(values)
#
#     # 0.2  74.18660303609379  <0.2s %
#     # 1.0  81.95121957751222  < 1s  %
#     # 5.0  91.49966376001284  < 5s  %
#     # 10.0 94.7320116402192   < 10s %
#     if values.get('1.0') and values['3.0']:
#         if  values.get('1.0') < 90  or  values['3.0'] < 100:
#             print("sendmail")
#             content = json.dumps(values)
#             send_mail('18688376362@163.com','接口耗时异常',content)
#
#     # # print(data)
#     # # bug 数据为none，TypeError: must be real number, not NoneType
#     # # [(1.0, None), (5.0, None), (25.0, None), (50.0, None), (75.0, None), (95.0, None), (99.0, None)]
#     # t_data = list(map(lambda x:[100-x[0],format_float(x[1],4)],data))
#     # result = {
#     #     'total':total,
#     #     'dataset':t_data,
#     # }
#     return json.dumps({"ok":"123"})
#     # return json.dumps(result,indent=4)

# 0.2  74.18660303609379  <0.2s %
# 1.0  81.95121957751222  < 1s  %
# 5.0  91.49966376001284  < 5s  %
# 10.0 94.7320116402192   < 10s %
# @es.route("/get_requesttime_ranks",methods=('GET','POST'))
# def get_requesttime_ranks():
#     cluster_name = request.headers.get('es_cluster_name')
#     data = json.loads(request.get_data().decode("utf-8"))
#     from_time = data.get('from')
#     to_time = data.get('to')
#     # values = [1.0,3.0]
#     values = string_to_list(data.get('values','1.0,3.0'))
#     print(values)
#     es = Elastic(cluster_name)
#
#     ts = TrafficStat(from_time,to_time,es,api_index)
#     results = ts.requesttime_ranks(values)
#     print(results)
#     if results.get(values[0]) and results.get(values[1]):
#         if  results.get(values[0]) < 90  or  results.get(values[1]) < 100:
#             print("sendmail")
#             content = json.dumps(results)
#             send_mail('18688376362@163.com','接口耗时异常',content)
#     return json.dumps({"ok":"123"})



@es.route("/sort_requesttime",methods=('GET','POST'))
def sort_requesttime():
    cluster_name = request.headers.get('es_cluster_name')
    data = json.loads(request.get_data().decode("utf-8"))
    from_time = data.get('from')
    to_time = data.get('to')
    body = {
        "_source": ["request","requesttime"],
        "query":{
          "bool":{
            "must":{"match_all":{}},
            "filter": [
              {
                "range": {
                  "@timestamp": {
                    "gte": from_time,
                    "lte": to_time
                  }
                }
              },
              {
                "range":{
                  "requesttime":{
                    "gte": 0.2
                  }
                }
              }
            ]
          }
        },
        "size":20,
        "sort":{
          "requesttime":{
            "order":"desc"
          }
        }
    }
    client = Elastic(cluster_name)
    res = client.search_data(idx=api_index,body=body)
    total = res['hits']['total']['value']
    hits = res['hits']['hits']
    data = []
    for hit in hits:
        # "_source" : {
        #   "request" : "/admin/perm/role_list",
        #   "requesttime" : 60.007
        # },
        source = hit["_source"]
        data.append(source)
    return json.dumps(data)


@es.route("/get_city_stat",methods=('GET','POST'))
def get_city_stat():
    cluster_name = request.headers.get('es_cluster_name')
    data = json.loads(request.get_data().decode("utf-8"))
    from_time = data.get('from')
    to_time = data.get('to')
    body = {
        "_source":"response",
        "query":{
            "bool":{
                "must":{"match_all":{}},
                "filter":{
                    "range":{
                        "@timestamp":{
                            "gte":from_time,
                            "lte":to_time
                        }
                    }
                }
            }
        },
        "size":0,
        "aggs":{
            "group_by_city":{
                "terms":{
                    "field": "geoip.city_name.keyword"
                }
            }
        }
    }
    client = Elastic(cluster_name)
    res = client.search_data(idx=nginx_index,body=body)
    # print(res)
    #

    # total = res['hits']['total']['value']
    total = res['hits']['total']
    data = res['aggregations']['group_by_city']['buckets']
    sorted_data = sorted(data,key=lambda t:t['doc_count'],reverse=False)

    dataset = list(map(lambda  x: {"name":x['key'],"value":x['doc_count']},sorted_data))
    print(dataset)
    result = {
        'total':total,
        'dataset':dataset,
    }
    print(result)
    return json.dumps(result,indent=4)
    # return json.dumps({"ok": "123"})