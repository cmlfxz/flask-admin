from flask import current_app,request,Blueprint
from api.models.user import User,Role
from api.models.db import db
from api.common.util import my_encode,my_decode,handle_input
from api.models.transform import model_to_dict

from sqlalchemy.exc import IntegrityError,InvalidRequestError

import json

user = Blueprint('user',__name__, url_prefix='/admin/user',template_folder='templates')

@user.route('/user_list',methods={'GET','POST'})
def user_list():
    rset = db.session.query(User).order_by(User.id.asc()).all()
    user_list = []
    for item in rset:
        user = model_to_dict(item)
        # user['password'] = my_decode(item.password)
        user_list.append(user)
    return json.dumps(user_list)

@user.route('/add_user',methods={'GET','POST'})
def add_user():
    error = None
    if request.method == 'POST':
        data = json.loads(request.get_data().decode('utf-8'))
        current_app.logger.debug('user_add收到的数据:{}'.format(data))
        user = data.get('user')
        username = user.get('username')
        pwd = user.get('password')
        password =  my_encode(pwd)
        role = user.get('role')
        group = user.get('group')
        qq = user.get('qq')
        weixin = user.get('remark')
        dingding = user.get('dingding')
        email = user.get('email')
        phone = user.get('phone')
        remark = user.get('remark')

        try:
            item = User(username=username,password=password,role=role,group=group,qq=qq,\
                            weixin=weixin,dingding=dingding,email=email,phone=phone,remark=remark)
            db.session.add(item)
            db.session.commit()
            return json.dumps({"ok":"添加菜单成功"})
        except Exception as e:
            current_app.logger.error(e)
            error = 'Exception'   
    return json.dumps({"fail":error})

@user.route('/delete_user', methods=('GET', 'POST'))
def delete_user():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('delete_user收到的数据:{}'.format(data))
    id = handle_input(data.get('id'))
    try:
        user = User.query.filter(User.id==id).first()
        db.session.delete(user)
        db.session.commit()
        return json.dumps({"ok":"删除成功"})
    except Exception as e:
        return json.dumps({"fail":"删除失败","error":e})

@user.route('/get_role_name',methods={'GET','POST'})
def get_role_name():
    rset = db.session.query(Role.name).all()
    
    role_name_list = []
    for item in rset:
        role_name_list.append(item.name)
    print(role_name_list)
    return json.dumps(role_name_list)


@user.route('/role_list',methods=('GET','POST'))
def role_list():
    rset = db.session.query(Role).all()
    role_list = []
    if len(rset) > 0:
        for item in rset:
            role = model_to_dict(item)
            role_list.append(role)

    return json.dumps(role_list)

#更新角色权限
@user.route('/update_role_perm',methods={'GET','POST'})
def update_role_perm():
    data = json.loads(request.get_data().decode("utf-8"))
    current_app.logger.info("update_role_perm收到数据:{}".format(data))
    id = data.get('id')
    perms  = data.get('perms')
    perms_all = data.get('perms_all')

    sql = db.session.query(Role).filter(Role.id == id)
    if sql.first():
        try:
            sql.update({"perms":perms,"perms_all":perms_all}) 
            return json.dumps({"ok":"保存用户权限数据成功"})
        except Exception as e:
            current_app.logger.error("保存用户权限数据失败:"+str(e))
    return json.dumps({"fail":"保存用户权限数据失败"})

#获取角色权限，只需返回叶子结点,编辑角色时，菜单树展开只需要叶子结点
@user.route('/get_role_perm',methods={'GET','POST'})
def get_role_perm():
    data = json.loads(request.get_data().decode("utf-8"))
    current_app.logger.info("get_role_perm收到数据:{}".format(data))
    role_id = data.get('role_id')
    role_name = data.get('role_name')
    # 获取角色权限列表
    perms = []
    if role_name == "超管" or role_name == "超级管理员":
        #获取超管的权限id
        result = db.session.query(Menu.menu_id).filter(Menu.parent_id==0).all()
        for item in result:
            perms.append(item.menu_id)
    else:
        rset = db.session.query(Role.perms).filter(Role.id==role_id).first()
        if len(rset) >0:
            perms = rset.perms.split(',')
    return json.dumps(perms)


# 获取角色权限树
def get_role_perms(role_nane):
    result = db.session.query(Role.perms_all).filter(Role.name == role_nane).first()
    if result:
        perms_all = result.perms_all.split(",")
        if len(perms_all) < 1:
            return None
        else:
            return perms_all
    else:
        return None

# 获取角色权限列表
def get_role_perm_menu(role_nane):
    result = db.session.query(Role.perms_all).filter(Role.name == role_nane).first()
    if result:
        perms_all = result.perms_all.split(",")
        if len(perms_all) < 1:
            return None
        else:
            temp  = list(filter(lambda x: int(x)< 100000,perms_all))
            return list(map(lambda x: int(x),temp))
    else:
        return None

#获取角色功能列表
def get_role_perm_operation(role_nane):
    result = db.session.query(Role.perms_all).filter(Role.name == role_nane).first()
    if result:
        perms_all = result.perms_all.split(",")
        if len(perms_all) < 1:
            return None
        else:
            temp = list(filter(lambda x: int(x)>= 100000,perms_all))
            return list(map(lambda x: int(x)-100000,temp))
    else:
        return None