from flask import Blueprint, request,current_app,jsonify,make_response

from functools import update_wrapper

import flask_sqlalchemy 
import requests
import simplejson as json
from api.models.db import db
from api.models.k8s import Cluster
from api.models.es import Es_Cluster,Index
from api.models.setting import Menu
from api.models.project import  Env,Project
from api.common.util import *
from api.models.transform import model_to_dict
import base64,sys,os
import requests,json
from datetime import datetime
from io import StringIO
import re


admin = Blueprint('admin',__name__,url_prefix='/admin')

def result_to_list(obj):
    lists = []
    for item in obj:
        elem = item[0]
        lists.append(elem)
    return lists


@admin.route('/get_cluster_name_list',methods=('GET','POST'))
def get_cluster_name_list():
    results  = db.session.query(Cluster.cluster_name).filter(Cluster.status==1).all()
    cluster_names = result_to_list(results)
    current_app.logger.debug(cluster_names)
    return json.dumps(cluster_names)

@admin.route('/cluster_list',methods=('GET','POST'))
def cluster_list():
    sql = db.session.query(Cluster).order_by(Cluster.create_time.desc())
    result = sql.all()
    cluster_list = []
    for item in result:
        cluster_list.append(model_to_dict(item))
    return json.dumps(cluster_list)

@admin.route('/cluster_disable',methods=('GET','POST'))
# @login_required
def cluster_disable():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug("cluster_disable收到数据:{}".format(data))
    id = handle_input(data.get('id'))
    cluster = Cluster.query.filter(Cluster.id == id)
    cluster.update({'status':0})
    db.session.commit()
    return jsonify({"msg":"ok"})

@admin.route('/cluster_enable',methods=('GET','POST'))
# @login_required
def cluster_enable():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug("cluster_disable收到数据:{}".format(data))
    id = handle_input(data.get('id'))
    cluster = Cluster.query.filter(Cluster.id == id)
    cluster.update({'status':1})
    db.session.commit()
    return jsonify({"msg":"ok"})

@admin.route('/cluster_create',methods=('GET','POST'))
# @login_required
def cluster_create():
    current_app.logger.debug('进入')
    if request.method == 'POST':
        f = request.files.get('cluster_config')   
        cluster_name = request.form['cluster_name']
        cluster_type= request.form['cluster_type']
        current_app.logger.debug("cluster_name:{}, cluster_type:{}".format(cluster_name,cluster_type))
        if not f or not cluster_name or not cluster_type:
            return jsonify({'msg':'fail','error':'配置文件,名字,类型不允许为空'})
        path =  "upload/cluster/"
        if(not os.path.exists(path)):
            os.makedirs(path)
        file_path = os.path.join(path,cluster_name+".config")
        f.save(file_path)
        
        with open(file_path, mode='r', encoding='UTF-8') as file:
            cluster_config = my_encode(file.read())
            
        sql = Cluster.query.filter(Cluster.cluster_name==cluster_name)
        if sql.first():
            try:
                sql.update({'update_time':datetime.now(),'cluster_config':cluster_config,'cluster_type':cluster_type})
                db.session.commit()
            except Exception as e:
                print(e)
                return jsonify({'msg':'fail','异常':"{}".format(e)})
        else:
            cluster = Cluster(cluster_name=cluster_name,cluster_config=cluster_config,cluster_type=cluster_type,update_time=datetime.now(),status=0)
            try:
                db.session.add(cluster)
                db.session.commit()
            except Exception as e:
                print(e)
                return jsonify({'msg':'fail','异常':"{}".format(e)})
    return jsonify({'msg':'ok','file_path':file_path})


# 根据环境获取集群名称列表
@admin.route('/get_cluster_by_env',methods=('GET','POST'))
def get_cluster_by_env(): 
    data = json.loads(request.get_data().decode('utf-8'))
    env_name =  handle_input(data.get('env_name'))
    results = db.session.query(Env.clusters).filter(Env.name == env_name).distinct().first_or_404()
    cluster_names = results[0].split(',')
    return json.dumps(cluster_names,indent=4)






