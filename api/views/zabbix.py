# https://python-jenkins.readthedocs.io/en/latest/examples.html
from flask import Blueprint, request,current_app,jsonify
import requests,json
from datetime import datetime


zabbix = Blueprint('zabbix',__name__,url_prefix='/admin/zabbix')

zabbix_url = 'http://192.168.11.202/api_jsonrpc.php'

def get_zabbix_token():
    url = zabbix_url
    post_headers = {'Content-Type': 'application/json'}
    post_data = {
        "jsonrpc": "2.0",
        "method": "user.login",
        "params": {
            "user": "admin",
            "password": "zabbix"
        },
        "id": 1
    }

    res = requests.post(url, data=json.dumps(post_data), headers=post_headers)
    if res.status_code == 200:
        return 200,res.json()['result']
    else:
        return res.status_code,res.text
def get_zabbix_hosts():
    url = zabbix_url
    status,token = get_zabbix_token()
    if status!=200:
        return json.dumps({"fail":"获取zabbix token失败"})
    post_headers = {'Content-Type': 'application/json'}
    # print(token)
    post_data = {
        "jsonrpc": "2.0",
        "method": "host.get",
        "params": {
            "output": [
                "hostid",
                "host"
            ],
            "selectInterfaces": [
                "interfaceid",
                "ip"
            ]
        },
        "id": 2,
        "auth": token 
    }

    res = requests.post(url, data = json.dumps(post_data), headers = post_headers)
    if res.status_code == 200:
        return 200,res.json()
    else:
        return res.status_code,res.text


@zabbix.route('/get_zabbix_graph',methods=('GET','POST'))
def get_zabbix_graph():
    url = zabbix_url
    status,token = get_zabbix_token()
    if status!=200:
        return json.dumps({"fail":"获取zabbix token失败"})
    post_headers = {'Content-Type': 'application/json'}
    # print(token)
    post_data = {
        "jsonrpc": "2.0",
        "method": "graph.get",
        "params": {
            "output": "extent",
            "hostids":10323,
        },
        "id": 3,
        "auth": token  # 这是第一步获取的身份验证令牌
    }

    res = requests.post(url, data = json.dumps(post_data), headers = post_headers)
    if res.status_code == 200:
        return json.dumps({"data":res.json()})
    else:
        return json.dumps({"fail":"获取zabbix Graph失败"})

def get_host_graph(token,hostid):
    url = zabbix_url
    post_headers = {'Content-Type': 'application/json'}
    post_data = {
        "jsonrpc": "2.0",
        "method": "graph.get",
        "params": {
            "output": "extent",
            "hostids":hostid,
        },
        "id": 3,
        "auth": token  # 这是第一步获取的身份验证令牌
    }

    res = requests.post(url, data = json.dumps(post_data), headers = post_headers)
    if res.status_code == 200:
        return 200,res.json()
    else:
        return res.status_code,res.text

@zabbix.route('/test_create_zabbix_screen',methods=('GET','POST'))
def test_create_zabbix_screen():
    url = zabbix_url
    status,token = get_zabbix_token()
    if status!=200:
        return json.dumps({"fail":"获取zabbix token失败"})
    post_headers = {'Content-Type': 'application/json'}
    print(token)
    post_data = {
        "jsonrpc": "2.0",
        "method": "screen.create",
        "params": {
            "name":'192.168.11.51',
            "hsize": 3,  #列
            "vsize":2,   #行
            "screenitems":[
                {
                    "resourcetype":0,
                    "resourceid": "1449",
                    "rowspan": 1, #占据几行
                    "colspan": 1, #占据几列
                    "width": 500,
                    "height": 100,
                    "x": 0,  #图像坐标
                    "y": 0,
                },
                {
                    'resourcetype': 0,
                    'resourceid': '1616',
                    'rowspan': 1,
                    'colspan': 1,
                    'width': 500,
                    'height': 100,
                    "x": 1,
                    "y": 0,
                }
            ]
            # "screenitems":[{'resourcetype': 0, 'resourceid': '1449', 'rowspan': 1, 'colspan': 1, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1616', 'rowspan': 1, 'colspan': 2, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1608', 'rowspan': 1, 'colspan': 3, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1456', 'rowspan': 2, 'colspan': 1, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1450', 'rowspan': 2, 'colspan': 2, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1451', 'rowspan': 2, 'colspan': 3, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1452', 'rowspan': 3, 'colspan': 1, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1453', 'rowspan': 3, 'colspan': 2, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1454', 'rowspan': 3, 'colspan': 3, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1457', 'rowspan': 4, 'colspan': 1, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1459', 'rowspan': 4, 'colspan': 2, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1458', 'rowspan': 4, 'colspan': 3, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1460', 'rowspan': 5, 'colspan': 1, 'width': 500, 'height': 100}, {'resourcetype': 0, 'resourceid': '1455', 'rowspan': 5, 'colspan': 2, 'width': 500, 'height': 100}]
        },
        "id": 4,
        "auth": token  # 这是第一步获取的身份验证令牌
    }

    res = requests.post(url, data = json.dumps(post_data), headers = post_headers)
    if res.status_code == 200:
        return json.dumps({"data":res.json()})
    else:
        return json.dumps({"fail":"创建zabbix Screen失败"})


def create_zabbix_screen(token,host,cols,rows,screenitems):
    url = zabbix_url
    post_headers = {'Content-Type': 'application/json'}
    # print(token)
    post_data = {
        "jsonrpc": "2.0",
        "method": "screen.create",
        "params": {
            "name": host,
            "hsize": cols,  #列
            "vsize":rows,   #行
            "screenitems":screenitems
        },
        "id": 4,
        "auth": token  # 这是第一步获取的身份验证令牌
    }
    res = requests.post(url, data = json.dumps(post_data), headers = post_headers)
    if res.status_code == 200:
        return 200,True
    else:
        return  res.status_code,res.text

#自动创建所有的主机screen
import math
@zabbix.route('/auto_create_zabbix_screen',methods=('GET','POST'))
def auto_create_zabbix_screen():
    url = zabbix_url
    status,token = get_zabbix_token()
    if status!=200:
        return json.dumps({"fail":"获取zabbix token失败"})
    # 获取主机
    status,host_data = get_zabbix_hosts()
    if status != 200:
        return json.dumps({"fail":"获取zabbix host失败"})
    # for item in host_data['result']:
    #     print(item['hostid'],item['host'])

    #获取每个host的graph列表
    for host_item in host_data['result']:
        host = host_item['host']
        hostid = host_item['hostid']
        status,graph_data = get_host_graph(token,hostid)
        if status != 200:
            return json.dumps({"fail":"zabbix获取{}graph失败".format(host)})
        # print(hostid, host, graph_data['result'])
        graph_id_list = []
        for graph_item in graph_data['result']:
            graph_id_list.append(graph_item['graphid'])
        # print(graph_id_list)

        if len(graph_id_list) > 0:
            # 获取行数,列数=3
            cols = 3
            rows = math.ceil(len(graph_id_list)/cols)
            screenItems = []
            t = 0
            for i in  range(rows):
                for j in range(cols):
                    index = i*cols+j
                    if index > len(graph_id_list)-1:
                        t = 1
                        break;
                    graphid=graph_id_list[index]
                    screenItem = {
                        "resourcetype": 0,
                        "resourceid": graphid,
                        "rowspan": 1,
                        "colspan": 1,
                        "width": 500,
                        "height": 100,
                        "x": j,
                        "y": i,
                    }
                    screenItems.append(screenItem)

                #     退出外层循环
                if t == 1:
                    break;
            try:
                print(host,screenItems)
                status,result = create_zabbix_screen(token,host,cols,rows,screenItems)
                if status == 200:
                    print("创建{} screen成功".format(host))
                else:
                    print("创建{} screen失败:{}".format(host,str(result)))
            except Exception as e:
                print("create_zabbix_screen异常:{}".format(str(e)))

    return json.dumps({"ok":"自动创建screen成功"})
