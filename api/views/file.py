from flask import Blueprint,request,current_app,jsonify,make_response
import os,json
from api.common.util import *
from api.controllers.minio import MinioS3

file = Blueprint('file',__name__, url_prefix='/admin/file',template_folder='templates')


def get_handler():
    handler = None
    storage_type = current_app.config.get('FILE_STORAGE')
    if storage_type == "minio":
        handler = MinioS3()
    return handler

def error_log(msg):
    current_app.logger.error(msg)



# def put_object_to_bucket(bucket_name,file_name,file_path,expires=None):
#     pool = BucketPool()
#     conn = pool.connect()
#     if not conn:
#         msg = "bucket服务器连接异常"
#         error_log(msg)
#         return False,msg
#     bucket = None
#     try:
#         bucket = conn.get_bucket(bucket_name)
#     except Exception as e:
#         print("找不到对应的bucket:",e)
#         bucket = conn.create_bucket(bucket_name)
#     if not bucket:
#         msg = "创建bucket失败"
#         error_log(msg)
#         return False,msg
#     key = bucket.new_key(file_name)
#
#     try:
#         with open(file_path, mode='r', encoding='UTF-8') as file:
#             #这种方式图片文件都可以
#             key.set_contents_from_filename(file_path)
#             key.set_canned_acl('public-read')
#             if not expires:
#                 expires = 0
#             url = key.generate_url(int(expires), query_auth=False, force_http=True)
#             print(url)
#             #这种方式只适合文件
#             # key.set_contents_from_string(file.read())
#         return True,url
#     except Exception as e:
#         msg = "保存文件到bucket失败"
#         error_log(msg)
#         return False,msg
def put_object_to_bucket(bucket_name,file_name,file_path,content_type=None,expires=None):
    handler = get_handler()
    if not handler:
        msg = "bucket服务器连接异常"
        error_log(msg)
        return False,msg
    download_url = handler.put_file_to_bucket(bucket_name=bucket_name,filename=file_name,filepath=file_path,content_type=content_type)
    if download_url:
        return True,download_url
    return False,None

@file.route('/put_object',methods={'GET','POST'})
def put_object():
    try:
        f = request.files.get('file')
        print(f)
        if not f :
            return jsonify({'fail':'文件不允许为空'})
        bucket_name = request.form['bucket_name']
        file_name= request.form['file_name']
        current_app.logger.debug("bucket_name:{}, file_name:{}".format(bucket_name,file_name))
    except Exception as e:
        print(e)
        bucket_name = 'blog-bucket'
        file_name = f.filename
    content_type = f.content_type
    path =  "upload/bucket/"
    if(not os.path.exists(path)):
        os.makedirs(path)
    file_path = os.path.join(path,file_name)
    # print("文件路径:",file_path)
    f.save(file_path)
    ok,result = put_object_to_bucket(bucket_name,file_name,file_path,content_type)
    if ok:
        os.remove(file_path)
        return json.dumps({"ok":"上传文件成功",'download_url':result,"ftype":f.content_type})
    else:
        return json.dumps({"fail":"上传文件失败","reason":result})