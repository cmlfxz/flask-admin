from flask import Blueprint,request,current_app,render_template
from api.models.db import db
from api.models.cmdb import ServerInfo,ServerUpdateLog,ServerLog,ServerUser,Record
from api.models.user import User,Server_User_User
from api.common.util import *
from api.common.host import  *
import json,datetime
from .auth import verify_token
from api.controllers.cmdb import  *
from api.models.transform import model_to_dict
import xlsxwriter
from flask import Response

from concurrent.futures import ThreadPoolExecutor
executor = ThreadPoolExecutor(10)

cmdb = Blueprint('cmdb',__name__,url_prefix='/admin/cmdb')

def convert(obj):
    if obj == '' or obj==None:
        return None
    else:
        return json.loads(obj)
        
@cmdb.route('/record_list',methods={'GET','POST'})
@verify_token
def record_list(): 
    sql = db.session.query(Record).order_by(Record.id.desc()).limit(1000)
    record_list = []
    results = sql.all()  
    for item in results: 
        user = model_to_dict(item)
        record_list.append(user)
    
    return json.dumps(record_list)

@cmdb.route('/server_user_list',methods={'GET','POST'})
@verify_token
def server_user_list(): 
    
    sql = db.session.query(ServerUser)
    server_user_list = []
    results = sql.all()  
    for item in results: 
        user = model_to_dict(item)
        server_user_list.append(user)
    return json.dumps(server_user_list)

# 系统用户列表
@cmdb.route('/server_user_info',methods={'GET','POST'})
# @verify_token
def server_user_info(): 
    data = json.loads(request.get_data().decode('utf-8'))
    username = data.get("username")
    # select su.* from server_user su left join  server_user_user suu  on su.id = suu.server_user_id left join user u on  u.id = suu.user_id where u.username='yy';
    if username:
        serverUser= db.session.query(ServerUser).outerjoin(Server_User_User, ServerUser.id == Server_User_User.server_user_id).\
                outerjoin(User,User.id==Server_User_User.user_id).\
                    filter(User.username==username).first()
        if serverUser:
            return json.dumps(model_to_dict(serverUser))
    # return json.dumps({"fail":"查不到系统账号"})
    return json.dumps({})

# 获取系统推送用户
@cmdb.route('/get_server_user',methods={'GET','POST'})
@verify_token
def get_server_user(): 
    sql = db.session.query(ServerUser.name)
    server_name_list = []
    results = sql.all()  
    for item in results: 
        server_name_list.append(item.name)
    return json.dumps(server_name_list)

@cmdb.route('/server_list',methods={'GET','POST'})
@verify_token
def server_list(): 
    sql = db.session.query(ServerInfo).filter(ServerInfo.hostname!='').order_by(ServerInfo.update_time.desc())
    server_list = []
    results = sql.all()  
    for item in results: 
        server = model_to_dict(item)
        server['system_info'] = convert(item.system_info)
        server['cpu'] = convert(item.cpu)
        server['cpu_load'] = convert(item.cpu_load)
        server['memory'] = convert(item.memory)
        server['device_info'] = convert(item.device_info)
        server['disk_info'] = convert(item.disk_info)
        server['disk_mount_list'] = convert(item.disk_mount_list)
        server_list.append(server)
    return json.dumps(server_list)

@cmdb.route('/init_log_list',methods={'GET','POST'})
def init_log_list():
    rset = db.session.query(ServerLog).order_by(ServerLog.create_time.desc()).limit(1025).all()
    init_log_list = []
    for item in rset:
        log = model_to_dict(item)
        init_log_list.append(log)
    return json.dumps(init_log_list)

#服务器初始化 通过ping 和检测 ssh端口 初始化服务器列表
@cmdb.route('/update_log_list',methods={'GET','POST'})
def update_log_list():
    rset = db.session.query(ServerUpdateLog).order_by(ServerUpdateLog.create_time.desc()).limit(50).all()
    update_log_list = []
    for item in rset:
        log = model_to_dict(item)
        update_log_list.append(log)
    return json.dumps(update_log_list)
            

#服务器初始化 通过ping 和检测 ssh端口 初始化服务器列表
@cmdb.route('/server_host_init',methods={'GET','POST'})
def server_host_init():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug("初始化获取到的数据:"+json.dumps(data))

    ip = data.get('ip')
    mask = data.get('mask')
    port = data.get('port')
    #根据网段和掩码计算网络地址范围
    ip_list = get_ip_segment(ip,mask)
    #去掉首尾一般作为网关的地址
    del ip_list[0]
    del ip_list[len(ip_list)-1]
    try:
        save_init_log("开始检测")
        # 异步操作
        for ip in ip_list:
            executor.submit(checkip, ip, port)
    except Exception as e:
        current_app.logger.debug("服务器初始化异常：",e)
        return jsonify({"fail":"服务器初始化失败"})
    return jsonify({"ok":"服务器初始化正在后台执行"})

@cmdb.route('/batch_push_user',methods={'GET','POST'})
def batch_push_user():
    data = json.loads(request.get_data().decode("utf-8"))
    id = data.get('id')
    result = db.session.query(ServerUser).filter(ServerUser.id==id).first()
    if result:
        server_user = model_to_dict(result)
        bos = BactchOperateServer()
        push_result = bos.push_user(server_user)
        if result == False:
            return json.dumps({"fail": "批量更新失败"})
        return json.dumps({"ok":"后台批量更新中"})
    else:
        return json.dumps({"fail": "查无此系统账号"})

    
@cmdb.route('/batch_delete_user',methods={'GET','POST'})
def batch_delete_user():
    data = json.loads(request.get_data().decode("utf-8"))
    current_app.logger.debug("batch_delete_user获取到前端数据:{}".format(data))
    account = data.get('account')
    bos = BactchOperateServer()
    result = bos.delete_user(account)
    if result == False:
        return json.dumps({"fail":"批量删除账号失败"})
    return json.dumps({"ok":"批量删除账号后台执行中"})

@cmdb.route('/batch_update_server',methods={'GET','POST'})
def batch_update_server():
    bos = BactchOperateServer()
    result = bos.update_sever()
    if result == False:
        return json.dumps({"fail":"批量更新失败"})
    return json.dumps({"ok":"批量更新后台执行中"})


def file_chunk(filepath,chunk_size=512):
    with open(filepath,'rb') as fd:
        while True:
            chunk = fd.read(chunk_size)
            if  chunk:
                yield chunk
            else:
                break


@cmdb.route('/export',methods={'GET','POST'})
def export():
    sql = db.session.query(ServerInfo).filter(ServerInfo.hostname!='').order_by(ServerInfo.update_time.desc())
    server_list = []
    results = sql.all()
    for item in results:
        server = model_to_dict(item)
        server_list.append(server)
    # print(server_list)
    filename = 'server.xlsx'
    dir_path = os.path.dirname(os.path.abspath(__file__))
    static_path = os.path.join(dir_path,'..','..','static')
    download_path =  os.path.join(static_path,'download')
    if not os.path.exists(download_path):
        os.makedirs(download_path)
    filepath = os.path.join(download_path,filename)
    print(filepath)
    workbook = xlsxwriter.Workbook(filepath)  # 建立文件
    worksheet = workbook.add_worksheet()  # 建立sheet， 可以work.add_worksheet('employee')来指定sheet名，但中文名会报UnicodeDecodeErro的错误
    worksheet.set_column('A:F', 40)
    row  = 0
    worksheet.write('A1', '主机名')
    worksheet.write('B1', '状态')
    worksheet.write('C1', '私有IP')
    worksheet.write('D1', '公有IP')
    worksheet.write('E1', '云类别')
    worksheet.write('F1', '系统')
    # worksheet.write('A1', 'CPU核心数/线程数')

    for item in server_list:
        row = row + 1
        worksheet.write(row, 0, item['hostname'])
        worksheet.write(row, 1, item['status'])
        worksheet.write(row, 2, item['private_ip'])
        worksheet.write(row, 3, item['public_ip'])
        worksheet.write(row, 4, item['cloud'])
        worksheet.write(row, 5, item['system_info'])

    workbook.close()

    response =  Response(file_chunk(filepath),content_type='application/octet-stream')
    response.headers['Content-Disposition'] = 'attachment; filename={}'.format(filename)
    return response
    # return Response(file_chunk('server.xlsx'),content_type='application/octet-stream',headers=headers)
