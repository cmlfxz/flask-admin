from flask import Blueprint,request,current_app
from api.models.db import db
from api.models.user import User
from api.models.project import Project
from api.models.worksheet import Requirement
from api.models.transform import model_to_dict
from api.common.util import handle_input,string_to_list,list_to_string

import json
from .auth import verify_token
import re

requirement = Blueprint('requirement',__name__, url_prefix='/flow/requirement',template_folder='templates')

def get_requirement_by_id(ids):
    if len(ids) > 0:
        requirement_list = []
        results = db.session.query(Requirement.id,Requirement.name) \
            .filter(Requirement.id.in_(ids)) \
                .all()
        if results:
            for result in results:
                item = {
                    "id": result.id,
                    "name": result.name
                }
                requirement_list.append(item)
            return requirement_list
    else:
        return []


@requirement.route('/requirement_add',methods={'GET','POST'})
def requirement_add():
    error = None
    if request.method == 'POST':
        data = json.loads(request.get_data().decode('utf-8'))
        current_app.logger.debug('requirement.add收到的数据:{}'.format(data))
        requirement = data.get('requirement')
        project = requirement.get('project')
        service_list = ",".join(requirement.get('service_list'))
        name = requirement.get('name')
        user = request.headers.get('username')
        try:
            item = Requirement(
                project=project,
                service_list=service_list,
                name=name,
                create_user=user,
                status='草稿'
            )
            db.session.add(item)
            db.session.commit()
            return json.dumps({"ok":"添加成功"})
        except Exception as e:
            current_app.logger.error(e)
            error = '未知异常'+str(e)  
    return json.dumps({"fail":error})


@requirement.route('/requirement_update', methods=('GET', 'POST'))
def requirement_update():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('requirement.update收到的数据:{}'.format(data))
    requirement = data.get('requirement')
    id = requirement.get('id')
    project = requirement.get('project')
    service_list = list_to_string(requirement.get('service_list'))
    name = requirement.get('name')
    content = requirement.get('content')
    status = requirement.get('status')
    try:
        Requirement.query.filter(Requirement.id==id) \
            .update({'project':project,'service_list':service_list,'name':name,'content':content,'status':status})
        db.session.commit()
        return json.dumps({"ok":"更新成功"})
    except Exception as e:
        current_app.logger.error("更新数据库异常:"+str(e))
        return json.dumps({"fail":"更新失败"})


@requirement.route('/requirement_list',methods=['GET','POST'])
def requirement_list():
    sql = db.session.query(Requirement).order_by(Requirement.create_time.desc())
    requirement_list = []
    results = sql.all()  
    if len(results) >0:
        for item in results:
            requirement = model_to_dict(item)
            requirement['service_list'] = string_to_list(item.service_list)
            requirement_list.append(requirement)
    return json.dumps(requirement_list)


#查询未完成的需求
@requirement.route('/not_finished_requirement',methods={'GET','POST'})
def not_finished_requirement():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('not_finished_bug收到的数据:{}'.format(data))
    project = data.get('project')
    results = db.session.query(Requirement.id,Requirement.name) \
            .filter(Requirement.status != '已上线',Requirement.project == project) \
                .all()
    nf_requirement_list = []
    if len(results) >0:
        for result in results:
            item = {
                "id": result.id,
                "name": result.name
            }
            nf_requirement_list.append(item)
    return json.dumps(nf_requirement_list)
