from flask import Blueprint,request,current_app
from api.models.db import db
from api.controllers.harbor import HarborApi

import json
from .auth import verify_token
import re

harbor = Blueprint('harbor',__name__, url_prefix='/flow/harbor',template_folder='templates')


def get_service_tag(project,env,service):
    repo = "{}-{}/{}".format(project,env,service)

    client = HarborApi()
    client.login_get_session_id()
    results = client.tags_info(repo)
    sorted_results = sorted(results,key = lambda e:e.__getitem__('name'),reverse=True)
    tags = []
    for item in sorted_results:
        tags.append(item.get('name'))
    return tags[:3]


@harbor.route('/get_tags_v2',methods=('GET','POST'))
def get_tags_v2():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('get_tags_v2收到的数据:{}'.format(data))
    project = data.get('project')
    env = data.get('env')
    services = data.get('services')

    tags = {}
    for service in services:
        service_tag = get_service_tag(project,env,service)

        tags[service] = service_tag
    
    return json.dumps(tags)