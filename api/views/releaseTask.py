# https://python-jenkins.readthedocs.io/en/latest/examples.html
from flask import Blueprint, request,current_app
import requests,json
from api.models.project import Service
from api.models.worksheet import  ReleaseTask
from api.common.util import  error_response,success_response,error_log,SingletonDBPool
from api.controllers.jenkins import JenkinsAPI
from api.models.db import db
import time
from pprint import pprint
from apscheduler.schedulers.background import  BackgroundScheduler
import datetime
from pytz import utc
from api.models.transform import  model_to_dict
from sqlalchemy.sql import func

class DateScheduler():
    def __init__(self):
        self.scheduler = BackgroundScheduler(timezone=utc)
    def add(self,func,kwargs):
        start_time = datetime.datetime.utcnow() + datetime.timedelta(seconds=10)
        # end_time = datetime.datetime.utcnow() + datetime.timedelta(seconds=5*60)
        self.scheduler.add_job(func=func, kwargs=kwargs, trigger='date', run_date=start_time)
        # self.scheduler.add_job(func=func, kwargs=kwargs, trigger='interval',seconds=5,start_date=start_time,end_date=end_time)
    def start(self):
        self.scheduler.start()

releaseTask = Blueprint('releaseTask',__name__,url_prefix='/flow/releaseTask')

env_branch_map = {
    "test":"develop",
    "prod":"master",
}

@releaseTask.route('/job_log',methods=('GET','POST'))
def job_log():
    client = JenkinsAPI()
    data = json.loads(request.get_data().decode('utf-8'))
    project = data.get("project")
    service = data.get("service")
    env = data.get("env")
    job_id = data.get('job_id')
    index = data.get('index')
    branch = env_branch_map.get(env)
    result = db.session.query(Service).filter(Service.project==project,Service.name==service).first()
    if not result:
        return error_response("查无此服务")
    else:
        is_multibranch = result.is_multibranch
        job_name = log_path = None
        if is_multibranch:
            job_name = "{project}-{service}/{branch}".format(project=project, service=service, branch=branch)
            log_path = "{project}-{service}/job/{branch}".format(project=project, service=service, branch=branch)
        else:
            job_name = "{project}-{env}-{service}".format(project=project,env=env,service=service)
            log_path = job_name
        end = False
        result = client.get_job_log(log_path,job_id)
        # lines = result.split('\n')
        # log = lines[index:]
        log = result[index:]
        if build_end(job_name,job_id):
            end = True

        return json.dumps({
            "log":log,
            "current_length":len(log),
            "total_length":len(result),
            "end":end,
        })

def build_end(job_name,job_id):
    client = JenkinsAPI()
    result = client.get_build_info(job_name, job_id)
    build_result = result.get('result')
    if build_result in ['SUCCESS', 'ABORTED', 'FAILURE']:
        return True
    return False


def update_release_status(job_name,job_id,id):
        client = JenkinsAPI()
        total_time = 0
        # SUCCESS ABORTED  FAILURE  None
        while total_time <= 20*60:
            result = client.get_build_info(job_name, job_id)
            build_result = result.get('result')
            duration = result.get('duration')
            if build_result in [ 'SUCCESS','ABORTED','FAILURE']:
                pool = SingletonDBPool()
                session = pool.connect()
                rt = session.query(ReleaseTask).filter(ReleaseTask.id == id)
                if build_result == "FAILURE":
                    rt.update({'status': 2,'duration':duration})
                    session.commit()
                elif build_result == "ABORTED":
                    rt.update({'status': 3, 'duration': duration})
                    session.commit()
                else:
                    rt.update({'status': 99, 'duration': duration})
                    session.commit()
                break
            total_time += 5
            time.sleep(5)
        return json.dumps({"ok":"获取构建信息成功"})

@releaseTask.route('/build_service',methods=('GET','POST'))
def build_service():
    data = json.loads(request.get_data().decode('utf-8'))
    project = data.get("project")
    service = data.get("service")
    env = data.get("env")
    cicd = data.get("cicd",None)
    branch = env_branch_map.get(env)

    result = db.session.query(Service).filter(Service.project==project,Service.name==service).first()
    if not result:
        return error_response("查无此服务")
    else:
        is_multibranch = result.is_multibranch
        git_url = result.git_url
        replicas = result.replicas
        job_name = log_path = None
        if is_multibranch:
            job_name = "{project}-{service}/{branch}".format(project=project, service=service, branch=branch)
            log_path = "{project}-{service}/job/{branch}".format(project=project, service=service, branch=branch)
        else:
            job_name = "{project}-{env}-{service}".format(project=project,env=env,service=service)
            log_path = job_name
        params = {
                'PROJECT': project,
                'SERVICE': service,
                'CODE_URL': git_url,
                'REPLICAS': replicas,
        }
        if cicd:
            params['CICD'] = cicd
        client = JenkinsAPI()
        if not client.check_is_build(job_name):
            job_id = client.build_job_param(job_name,params)
            print(job_id)
            # 发布入库
            rt = ReleaseTask(project = project,env=env,service=service,
                job_id = job_id, status=1,
            )
            try:
                db.session.add(rt)
                db.session.commit()
            except Exception:
                error_log("发布数据写库失败")
                return error_response("发布数据写库失败")
            #  获取id
            result = db.session.query(ReleaseTask.id) \
                        .filter(ReleaseTask.project == project,
                                 ReleaseTask.env==env,
                                 ReleaseTask.service==service,
                                 ReleaseTask.job_id == job_id,
                                 ReleaseTask.status==1).first()
            # 启动一个任务，定期查询任务状态入库
            scheduler = DateScheduler()
            scheduler.add(update_release_status,kwargs={'job_name':job_name,'job_id':job_id,"id":result.id})
            scheduler.start()
            return json.dumps({"ok":"构建成功","job_id":job_id})
        else:
            error_log("当前JOB正在构建")
            return error_response("当前JOB正在构建")

# @releaseTask.route('/get_job_ids',methods=('GET','POST'))
# def get_job_ids():
#     data = json.loads(request.get_data().decode('utf-8'))
#     project = data.get("project")
#     service = data.get("service")
#     env = data.get("env")
#
#     results = db.session.query(ReleaseTask.job_id) \
#                 .filter(ReleaseTask.project==project,ReleaseTask.service==service,ReleaseTask.env==env) \
#                     .order_by(ReleaseTask.job_id.desc()).all()
#     job_ids = []
#     if results:
#         for item in results:
#             job_ids.append(item.job_id)
#     return json.dumps(job_ids)



@releaseTask.route('/get_service_task',methods=('GET','POST'))
def get_service_task():
    # select * from (select * from  release_task where project='ms' and service='flask-admin' order by job_id desc limit 999999) a group by a.env
    data = json.loads(request.get_data().decode('utf-8'))
    project = data.get("project")
    service = data.get("service")


    ids = db.session.query(func.max(ReleaseTask.job_id).label('max_job_id')) \
            .filter(ReleaseTask.project==project,ReleaseTask.service==service) \
                .group_by(ReleaseTask.env)
    max_job_id_list = []
    for item in ids:
        max_job_id_list.append(item.max_job_id)
    results = db.session.query(ReleaseTask).filter(ReleaseTask.project==project,ReleaseTask.service==service) \
                .filter(ReleaseTask.job_id.in_(max_job_id_list)).all()
    job_list = []
    if results:
        for item in results:
            job_list.append(model_to_dict(item))
    return json.dumps(job_list)
