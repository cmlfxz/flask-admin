from flask import Blueprint,g,request,current_app
from api.models.db import db
from api.models.user import User
from api.models.pressure import PressureSetting,PressureData
from api.models.transform import model_to_dict
from api.common.util import handle_input
# from api.models.models import User,Site
from sqlalchemy.exc import IntegrityError,InvalidRequestError
import json

pressure = Blueprint('pressure',__name__, url_prefix='/admin/pressure',template_folder='templates')

@pressure.route('/setting_add',methods={'GET','POST'})
def setting_add():
    error = None
    if request.method == 'POST':
        data = json.loads(request.get_data().decode('utf-8'))
        current_app.logger.debug('pressure.add收到的数据:{}'.format(data))
        setting = data.get('pressure_setting')
        name = setting.get('name')
        ptype = setting.get('ptype')
        target_detail = setting.get('target_detail')
        tools = setting.get('tools')
        tools_detail = setting.get('tools_detail')
        other = setting.get('other')
        try:
            pressure_setting = PressureSetting(ptype=ptype,name=name,target_detail=target_detail,tools=tools,tools_detail=tools_detail,other=other)
            db.session.add(pressure_setting)
            db.session.commit()
            return json.dumps({"ok":"添加站点成功"})
        except Exception as e:
            current_app.logger.error(e)
            error = 'Exception'   
    
    return json.dumps({"fail":error})

# 根据id查询站点信息 这一步非必须
@pressure.route('/setting_detail',methods={'GET','POST'})
def setting_detail():
    data = json.loads(request.get_data().decode('utf-8'))
    id = data.get('id')
    sql = PressureSetting.query.filter(PressureSetting.id==id)
    setting = sql.first()
    return json.dumps(model_to_dict(setting),indent=4)

@pressure.route('/setting_update', methods=('GET', 'POST'))
def setting_update():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('site.update收到的数据:{}'.format(data))
    setting = data.get('pressure_setting')
    id = setting.get('id')
    name = setting.get('name')
    ptype = setting.get('ptype')
    target_detail = setting.get('target_detail')
    tools = setting.get('tools')
    tools_detail = setting.get('tools_detail')
    other = setting.get('other')
    try:
        # Site.query.filter(Site.id==id).update({'name':name})
        PressureSetting.query.filter(PressureSetting.id==id).update({'ptype':ptype,'name':name,'target_detail':target_detail,'tools':tools,'tools_detail':tools_detail,'other':other})
        db.session.commit()
        return json.dumps({"ok":"更新成功"})
    except Exception as e:
        return json.dumps({"fail":"更新失败"})

@pressure.route('/setting_list',methods={'GET','POST'})
def setting_list():
    # admin 用户可以看到所有的站点，其他用户只能看自己写的站点
    sql = db.session.query(PressureSetting).order_by(PressureSetting.create_time.desc())
    setting_list = []
    results = sql.all()  
    if len(results) >0:
        for result in results:
            setting_list.append(model_to_dict(result))
    return json.dumps(setting_list)



@pressure.route('/data_add',methods={'GET','POST'})
def data_add():
    error = None
    if request.method == 'POST':
        data = json.loads(request.get_data().decode('utf-8'))
        # current_app.logger.debug('pressure.add收到的数据:{}'.format(data))
        pressure_data = data.get('pressure_data')
        pid = pressure_data.get('pid')
        pname = pressure_data.get('pname')
        name = pressure_data.get('name')
        try:
            pressure_data = PressureData(pid=pid,name=name,pname=pname)
            db.session.add(pressure_data)
            db.session.commit()
            return json.dumps({"ok":"添加站点成功"})
        except Exception as e:
            current_app.logger.error(e)
            error = 'Exception'   
    
    return json.dumps({"fail":error})

# 根据id查询站点信息 这一步非必须
@pressure.route('/data_detail',methods={'GET','POST'})
def data_detail():
    data = json.loads(request.get_data().decode('utf-8'))
    id = data.get('id')
    sql = PressureData.query.filter(PressureData.id==id)
    pressure_data = sql.first()
    return json.dumps(model_to_dict(pressure_data),indent=4)

@pressure.route('/data_update', methods=('GET', 'POST'))
def data_update():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('data_update收到的数据:{}'.format(data))
    pressure_data = data.get('pressure_data')
    id = pressure_data.get('id')
    pid = pressure_data.get('pid')
    pname = pressure_data.get('pname')
    name = pressure_data.get('name')
    parallel = pressure_data.get('parallel')
    request_num = pressure_data.get('request_num')
    data_length = pressure_data.get('data_length')
    other = pressure_data.get('other')
    pdata = pressure_data.get('pdata')
    try:
        PressureData.query.filter(PressureData.id==id). \
            update({'pid':pid,'pname':pname,'name':name,'parallel':parallel,'request_num':request_num,'data_length':data_length,'other':other,'pdata':pdata})
        db.session.commit()
        return json.dumps({"ok":"更新成功"})
    except Exception as e:
        current_app.logger.error("更新异常:"+str(e))
        return json.dumps({"fail":"更新失败:"+str(e)})

@pressure.route('/get_pressure_name',methods={'GET','POST'})
def get_pressure_name():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('get_pressure_name收到的数据:{}'.format(data))
    pid = handle_input(data.get('pid'))
    
    rset = db.session.query(PressureSetting.name).filter(PressureSetting.id == pid).first()
    name = ""
    if len(rset) >0:
        name  = rset.name
        return json.dumps(name)
    else:
        return json.dumps({"fail":"获取失败"})

@pressure.route('/data_list',methods={'GET','POST'})
def data_list():
    # admin 用户可以看到所有的站点，其他用户只能看自己写的站点
    sql = db.session.query(PressureData).order_by(PressureData.create_time.desc())
    data_list = []
    results = sql.all()  
    if len(results) >0:
        for result in results:
            data_list.append(model_to_dict(result))
    return json.dumps(data_list)

