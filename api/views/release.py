from flask import Blueprint,g,request,current_app
from api.models.db import db
from api.models.user import User
from api.models.project import Project
from api.models.worksheet import Release,Bug,Requirement,Version
from api.models.transform import model_to_dict
from api.views.bug import get_bug_by_id
from api.views.requirement import get_requirement_by_id
from api.common.util import handle_input,string_to_list,list_to_string

from sqlalchemy.exc import IntegrityError,InvalidRequestError
import json
from .auth import verify_token
import re


release = Blueprint('release',__name__, url_prefix='/flow/release',template_folder='templates')


@release.route('/release_add',methods={'GET','POST'})
def release_add():
    error = None
    if request.method == 'POST':
        data = json.loads(request.get_data().decode('utf-8'))
        current_app.logger.debug('release.add收到的数据:{}'.format(data))
        release = data.get('release')
        project = release.get('project')
        service_list = ",".join(release.get('service_list'))
        user = request.headers.get('username')
        try:
            item = Release(project=project,service_list=service_list,create_user=user,status='草稿')
            db.session.add(item)
            db.session.commit()
            return json.dumps({"ok":"添加站点成功"})
        except Exception as e:
            current_app.logger.error(e)
            error = '异常:'+str(e)   
    return json.dumps({"fail":error})

@release.route('/release_update', methods=('GET', 'POST'))
def release_update():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('release.update收到的数据:{}'.format(data))
    release = data.get('release')
    id = release.get('id')
    release_type = release.get('type')
    project = release.get('project')
    service_list = list_to_string(release.get('service_list'))
    requirements = list_to_string(release.get('requirements'))
    bugs = list_to_string(release.get('bugs'))
    description = release.get('description')
    status = release.get('status')
    review_user = release.get('review_user',None)
    if review_user:
        status = '待审核'
    try:
        Release.query.filter(Release.id==id).update({
            'project':project,
            'service_list':service_list,
            'release_type':release_type,
            'requirements':requirements,
            'bugs':bugs,
            'description':description,
            'status':status,
            'review_user': review_user
        })
        db.session.commit()
        return json.dumps({"ok":"更新成功"})
    except Exception as e:
        return json.dumps({"fail":"更新失败"})

@release.route('/release_update_status', methods=('GET', 'POST'))
def release_update_status():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('release_update_status收到的数据:{}'.format(data))
    id = data.get('id')
    action = data.get('action')
    if action == 'publish':
        status = '待生产验证'
    elif action == 'accept':
        status = '上线完成'
    else:
        status = '未定义'
    try:
        Release.query.filter(Release.id==id).update({
            'status':status,
        })
        db.session.commit()
        return json.dumps({"ok":"更新成功"})
    except Exception as e:
        return json.dumps({"fail":"更新失败"})

@release.route('/release_delete', methods=('GET', 'POST'))
def release_delete():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('release.delete收到的数据:{}'.format(data))
    id = handle_input(data.get('id'))
    try:
        release = Release.query.filter(Release.id==id).first()
        db.session.delete(release)
        db.session.commit()
        return json.dumps({"ok":"删除成功"})
    except Exception as e:
        return json.dumps({"fail":"删除失败","error":e})
    return None


@release.route('/release_list',methods={'GET','POST'})
@verify_token
def release_list():
    release_list = []
    results = db.session.query(Release) \
        .order_by(Release.create_time.desc()) \
            .all()
    if results:
        for item in results:
            release = model_to_dict(item)
            release['service_list'] = string_to_list(item.service_list)
            release['requirements'] = string_to_list(item.requirements)
            release['bugs'] = string_to_list(item.bugs)
            release_list.append(release)
    return json.dumps(release_list)

#根据项目查询数据库
@release.route('/release_select',methods={'GET','POST'})
def release_select():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('release_select收到的数据:{}'.format(data))
    select_item = data.get('select_item')
    project = select_item.get('project')
    results = db.session.query(Release) \
        .filter(Release.project==project) \
            .order_by(Release.create_time.desc()) \
                .all()
    release_list = []
    if results:
        for item in results:
            release = model_to_dict(item)
            release['service_list'] = string_to_list(item.service_list)
            release['requirements'] = string_to_list(item.requirements)
            release['bugs'] = string_to_list(item.bugs)
            release_list.append(release)
    return json.dumps(release_list)


#发布评审
@release.route('/release_review',methods={'GET','POST'})
def release_review():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('release_review:{}'.format(data))
    id = data.get('id')
    review_form = data.get('review_form')
    review_result = review_form.get('result')
    review_content = review_form.get('content')
    status = None
    if review_result=="核准发布":
        status = '待发布生产'
    elif review_result=="打回重改":
        status = '重新修改'
    elif review_result=="拒绝":
        status = '撤销发布'
    else:
        return json.dumps({"fail":"无法处理这种评审结果"})
    try:
        Release.query.filter(Release.id==id).update({
            'review_result':review_result,
            'review_content': review_content,
            'status':status
        })
        db.session.commit()
        return json.dumps({"ok":"更新发布成功"})
    except Exception as e:
        return json.dumps({"fail":"更新发布失败"})

@release.route('/release_detail',methods={'GET','POST'})
def release_detail():
    data = json.loads(request.get_data().decode('utf-8'))
    id = data.get('id')
    
    sql = Release.query.filter(Release.id==id)
    result = sql.first()
    
    if result:
        release = model_to_dict(result)
        release['service_list'] = string_to_list(result.service_list)
        bugs = requirements= []
        if result.bugs:
            bugs =   get_bug_by_id(result.bugs.split(',') )
        if result.requirements:
            requirements = get_requirement_by_id(result.requirements.split(','))
        release['bugs'] = bugs
        release['requirements'] = requirements
        return json.dumps(release,indent=4)
    else:
        return json.dumps({"fail":"查不到此发布详情"})


