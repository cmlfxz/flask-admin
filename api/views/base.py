from flask import Blueprint,request,current_app,jsonify,g,make_response
from api.common.util import *
from kubernetes import client,config
from kubernetes.client.rest import ApiException
from api.models.k8s import Cluster
from api.models.db import  db
from api.common.redis import MyRedis
from pprint import pprint

# 定义蓝图
base = Blueprint('base',__name__,url_prefix='/admin/base/')

def get_cluster_config(cluster_name):
    cluster_config = None
    pool = SingletonDBPool()
    session = pool.connect()
    if session == None:
        print("无法获取数据库连接")
    else:
        result = session.query(Cluster.cluster_config).filter(Cluster.cluster_name==cluster_name).first()
        cluster_config = result.cluster_config
    return cluster_config


@base.after_app_request
def after(resp):
    resp = make_response(resp)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Access-Control-Allow-Methods'] = 'GET,POST,OPTIONS,PATCH,DELETE'
    resp.headers['Access-Control-Allow-Headers'] = 'content-type,es_cluster_name,cluster_name,user,username,user_id,token'
    return resp

def error_response(msg,code):
    current_app.logger.error(msg)
    return make_response(json.dumps({"fail":msg}), code)

def set_config(cluster_name):
    if cluster_name == 'null' or not cluster_name:
        return error_response("header没有设置cluster_name",4001)
    current_app.logger.info("收到集群名:" + cluster_name)
    redis_cli = MyRedis()
    file_key = "{}_config_file".format(cluster_name)
    cache_name = redis_cli.get_key('cluster_name')
    if cluster_name == cache_name:
        # current_app.logger.info("此集群配置已缓存")
        config_file = redis_cli.get_key(file_key)
        if not set_k8s_config(config_file):
            return error_response("读取redis设置k8s集群配置失败",5000)
    else:
        cluster_config = get_cluster_config(cluster_name)
        if cluster_config and set_k8s_config(cluster_config):
            redis_cli.set_key('cluster_name', cluster_name, time=3 * 60 * 60)
            file_key = "{}_config_file".format(cluster_name)
            redis_cli.set_key(file_key, cluster_config)
        else:
            return error_response("读取MySQL设置k8s集群配置失败",5000)

def set_k8s_config(cluster_config):
    try:
        cluster_config = my_decode(cluster_config)
        tmp_filename = "kubeconfig"
        with open(tmp_filename, 'w+', encoding='UTF-8') as file:
            file.write(cluster_config)
        config.load_kube_config(config_file=tmp_filename)
        os.remove(tmp_filename)
        return True
    except Exception as e:
        current_app.logger.debug("集群配置文件解码失败:"+str(e))
    return False

@base.before_app_request
def load_header():
    if request.method == 'OPTIONS':
        pass
    elif request.method == 'POST':
        if request.headers.get("content-type",None) == "application/json;charset=UTF-8":
            try:
                if request.get_data():
                    print(request.get_data())
                    data = json.loads(request.get_data())
                    print("{}收到的数据:".format(request.url.rsplit('/')[-1]))
                    pprint(data)
            except Exception as e:
                return json.dumps({"fail":"数据json解码失败"})
        uri = "/"
        to_list = request.url.split('/')
        for index in range(len(to_list)):
            if index > 2 and index != len(to_list)-1:
                uri = uri + to_list[index]+"/"
            elif index == len(to_list)-1:
                uri = uri + to_list[index]
        if uri.startswith("/admin/istio"):
            try:
                cluster_name = request.headers.get('cluster_name')
                return set_config(cluster_name)
            except Exception as e:
                return  error_response("load_header异常",5000)
    else:
        return error_response("非法请求,支持post",405)
