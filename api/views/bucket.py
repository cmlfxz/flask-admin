from flask import Blueprint,request,current_app,jsonify,make_response
import os,json
from api.common.util import *
from api.models.transform import model_to_dict


bucket = Blueprint('bucket',__name__, url_prefix='/admin/bucket',template_folder='templates')

@bucket.route('/create_bucket',methods={'GET','POST'})
def create_bucket():
    data = json.loads(request.get_data().decode("utf-8"))
    current_app.logger.debug("key_list获取的数据{}:".format(data))
    bucket_name = data.get('bucket_name')
    
    pool = BucketPool()

    conn = pool.connect()
    if not conn:
        return make_response(json.dumps({"fail":"bucket服务器连接异常"}),666)
    try:
        conn.create_bucket(bucket_name)
    except Exception as e:
        print(e)
        return json.dumps({"fail":"创建失败"})
    return json.dumps({"ok":"创建成功"})

@bucket.route('/delete_bucket',methods={'GET','POST'})
def delete_bucket():
    data = json.loads(request.get_data().decode("utf-8"))
    current_app.logger.debug("delete_bucket获取的数据:{}".format(data))
    bucket_name = data.get('bucket_name')
    
    pool = BucketPool()
    conn = pool.connect()
    if not conn:
        return make_response(json.dumps({"fail":"bucket服务器连接异常"}),666)
    # 查询bucket是否含有数据，有的话不允许删除
    bucket = conn.get_bucket(bucket_name)
    has_key = False
    # <class 'boto.s3.bucketlistresultset.BucketListResultSet'> 
    for key in  bucket.list():
        if (key):
            has_key = True
            break
    if(has_key):
        return jsonify({"fail":"bucket非空，请先删除bucket数据"})
        
    try:
        conn.delete_bucket(bucket_name)
    except Exception as e:
        print(e)
        return json.dumps({"fail":"删除bucket失败"})
    return json.dumps({"ok":"删除bucket成功"})


@bucket.route('/get_all_buckets',methods={'GET','POST'})
def get_all_buckets():
    pool = BucketPool()
    conn = pool.connect()
    if not conn:
        return make_response(json.dumps({"fail":"bucket服务器连接异常"}),666)
    bucket_list = []
    for bucket in conn.get_all_buckets():
        # print(bucket.name,bucket.creation_date)
        item = {"name":bucket.name,"created":bucket.creation_date}
        bucket_list.append(item)
    return json.dumps(bucket_list,indent=4)


@bucket.route('/bucket_key_list',methods={'GET','POST'})
def bucket_key_list():
    data = json.loads(request.get_data().decode("utf-8"))
    current_app.logger.debug("bucket_key_list获取的数据:{}".format(data))
    bucket_name = data.get('bucket_name')
    
    pool = BucketPool()
    conn = pool.connect()
    if not conn:
        return make_response(json.dumps({"fail":"bucket服务器连接异常"}),666)
    bucket = conn.get_bucket(bucket_name)
    
    key_list = []
    for key in bucket.list():
        # print(key.name,key.size,key.last_modified

        #转成以MB为单位
        size = format_float(key.size/1024/1024)
        last_modified = utc_to_local(key.last_modified, utc_format='%Y-%m-%dT%H:%M:%S.%fZ')
        item = {}
        item["name"] =key.name 
        item["bucket"] =bucket_name 
        item["filename"] =key.filename 
        item["content_type"] =key.content_type 
        item["expiry_date"] = key.expiry_date 
        item["size"] = size 
        item["last_modified"] = last_modified 
        key_list.append(item)
    return json.dumps(key_list,indent=4)


@bucket.route('/delete_key',methods={'GET','POST'})
def delete_key():
    data = json.loads(request.get_data().decode("utf-8"))
    current_app.logger.debug("bucket_key_list获取的数据:{}".format(data))
    bucket_name = data.get('bucket_name')
    key_name = data.get('name')
    pool = BucketPool()
    conn = pool.connect()
    if not conn:
        return make_response(json.dumps({"fail":"bucket服务器连接异常"}),666)
    bucket = conn.get_bucket(bucket_name)
    
    has_key = False
    # <class 'boto.s3.bucketlistresultset.BucketListResultSet'> 
    for key in  bucket.list():
        if (key.name == key_name):
            has_key = True
            break
    if not has_key:
        return jsonify({"fail":"查无此对象"})
    
    bucket.delete_key(key)
    
    return json.dumps({"ok":"删除成功"})

def error_log(msg):
    current_app.logger.error(msg)


def put_object_to_bucket(bucket_name,file_name,file_path,expires=None):
    pool = BucketPool()
    conn = pool.connect()
    if not conn:
        msg = "bucket服务器连接异常"
        error_log(msg)
        return False,msg
    bucket = None
    try:
        bucket = conn.get_bucket(bucket_name)
    except Exception as e:
        print("找不到对应的bucket:",e)
        bucket = conn.create_bucket(bucket_name)
    if not bucket:
        msg = "创建bucket失败"
        error_log(msg)
        return False,msg
    key = bucket.new_key(file_name)

    try:
        with open(file_path, mode='r', encoding='UTF-8') as file:
            #这种方式图片文件都可以
            key.set_contents_from_filename(file_path)
            key.set_canned_acl('public-read')
            if not expires:
                expires = 0
            url = key.generate_url(int(expires), query_auth=False, force_http=True)
            print(url)
            #这种方式只适合文件
            # key.set_contents_from_string(file.read())
        return True,url
    except Exception as e:
        msg = "保存文件到bucket失败"
        error_log(msg)
        return False,msg

# f.filename: 9e5585c6739942f687da4a93486d082c.png
# f.name: file
# f.content_type:  image/png 
# f.content_length: 0
@bucket.route('/put_object',methods={'GET','POST'})
def put_object():
    try:
        f = request.files.get('file')  
        print(f)
        if not f :
            return jsonify({'fail':'文件不允许为空'})
        bucket_name = request.form['bucket_name']
        file_name= request.form['file_name']
        current_app.logger.debug("bucket_name:{}, file_name:{}".format(bucket_name,file_name))
    except Exception as e:
        print(e)
        bucket_name = 'blog-bucket'
        file_name = f.filename
    path =  "upload/bucket/"
    if(not os.path.exists(path)):
        os.makedirs(path)
    file_path = os.path.join(path,file_name)
    # print("文件路径:",file_path)
    f.save(file_path)
    ok,result = put_object_to_bucket(bucket_name,file_name,file_path)
    if ok:
        os.remove(file_path)
        return json.dumps({"ok":"上传文件成功",'download_url':result,"ftype":f.content_type})
    else:
        return json.dumps({"fail":"上传文件失败","reason":result})




@bucket.route('/custom_put_object',methods={'GET','POST'})
def custom_put_object():
    try:
        f = request.files.get('file')  
        print(f)
        if not f :
            return json.dumps({'fail':'文件不允许为空'})
        bucket_name = request.form['bucket_name']
        file_name= request.form['file_name']
        expires  = request.form['expires']
        print(bucket_name,expires,file_name)
    except Exception as e:
        print(e)
        bucket_name = 'custom-bucket'
    file_name = f.filename
    path =  "upload/bucket/"
    if(not os.path.exists(path)):
        os.makedirs(path)
    file_path = os.path.join(path,file_name)
    f.save(file_path)
    ok,result = put_object_to_bucket(bucket_name,file_name,file_path)
    print(result)
    if ok:
        os.remove(file_path)
        return json.dumps({"ok":"上传文件成功",'download_url':result,"ftype":f.content_type})
    else:
        return json.dumps({"fail":"上传文件失败","reason":result})