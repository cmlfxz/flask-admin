# https://elasticsearch-py.readthedocs.io/en/latest/
from flask import Blueprint, request,current_app,jsonify
import requests,json
from datetime import datetime
from api.common.util import   utc_to_local,http_request_v2
import urllib.parse
from .auth import verify_token

prom = Blueprint('prom',__name__,url_prefix='/admin/prom')

PROMETHEUS = 'http://192.168.11.51:21646/'

@prom.route('/get_health',methods=('POST','GET'))
def get_health():
    url = PROMETHEUS+'/-/healthy'
    status,res = http_request_v2(url)

    print(status,res)

    return json.dumps({"ok":res})

# @prom.route('/test',methods=('POST','GET'))
# def test():
#     now = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
#     url = PROMETHEUS+'/api/v1/query'+"?query=up&time="+now
#     status,res = http_request_v2(url)
#     # 2021-01-03 16:17:25.029258
#     # print(type(now), now)
#     if status == 200:
#         print(1)
#     else:
#         print (2)
#     return json.dumps(res)

@prom.route('/test',methods=('POST','GET'))
def test():
    # params= 'match[]=up{job="istiod"}&match[]=process_start_time_seconds{job="prometheus"}'
    params = 'match[]=*{job="node"}'
    url = PROMETHEUS + '/api/v1/series?'+params

    status,res = http_request_v2(url)
    print(res)
    return json.dumps({"ok":"ok"})


@prom.route('/test_query',methods=('POST','GET'))
@verify_token
def test_query():
    # params= 'match[]=up{job="istiod"}&match[]=process_start_time_seconds{job="prometheus"}'
    params = 'sum(irate(container_cpu_usage_seconds_total{id="/"}[1m]))/sum(machine_cpu_cores)*100'
    url = PROMETHEUS + '/api/v1/query?query='+params

    status,res = http_request_v2(url)
    # {"status":"success","data":{"resultType":"vector","result":[{"metric":{},"value":[1620570860.025,"10.314627443485664"]}]}}
    print(type(res),res)
    if status==200:
        data = json.loads(res)['data']
        value = data['result'][0]['value']
        v = value[1]
        print(v)
    return json.dumps({"ok":"ok"})

