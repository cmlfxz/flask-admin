from flask import Blueprint,request,current_app
from api.models.db import db
from api.models.user import User
from api.models.project import Project
from api.models.worksheet import Version
from api.models.transform import model_to_dict
from api.common.util import handle_input,string_to_list,list_to_string

import json
from .auth import verify_token
import re

version = Blueprint('version',__name__, url_prefix='/flow/version',template_folder='templates')


@version.route('/version_add',methods={'GET','POST'})
def version_add():
    error = None
    if request.method == 'POST':
        data = json.loads(request.get_data().decode('utf-8'))
        current_app.logger.debug('version.add收到的数据:{}'.format(data))
        version = data.get('version')
        project = version.get('project')
        content = version.get('content')
        try:
            item = Version(project=project,content=content)
            db.session.add(item)
            db.session.commit()
            return json.dumps({"ok":"添加成功"})
        except Exception as e:
            current_app.logger.error(e)
            error = '未知异常'+str(e)  
    return json.dumps({"fail":error})


@version.route('/version_update', methods=('GET', 'POST'))
def version_update():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('version.update收到的数据:{}'.format(data))
    version = data.get('version')
    id = version.get('id')
    content = version.get('content')
    try:
        Version.query.filter(Version.id==id).update({'content':content})
        db.session.commit()
        return json.dumps({"ok":"更新成功"})
    except Exception as e:
        return json.dumps({"fail":"更新失败"})


@version.route('/version_list',methods={'GET','POST'})
@verify_token
def version_list():
    sql = db.session.query(Version).order_by(Version.create_time.desc())
    version_list = []
    results = sql.all()  
    if len(results) >0:
        for result in results:
            version_list.append(model_to_dict(result))
    return json.dumps(version_list)

#根据项目查询版本信息
@version.route('/version_select',methods={'GET','POST'})
def version_select():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('version_select收到的数据:{}'.format(data))
    select_item = data.get('select_item')
    project = select_item.get('project')
    results = db.session.query(Version.create_time,Version.content) \
        .filter(Version.project==project) \
            .order_by(Version.create_time.desc()) \
                .all()
    version_list = []
    if len(results) >0:
        for result in results:
            item = {
                "time": result.create_time.strftime( '%Y-%m-%d %H:%M'),
                "content": result.content
            }
            version_list.append(item)
    return json.dumps(version_list)
