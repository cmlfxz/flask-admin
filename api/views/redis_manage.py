from flask import Blueprint, request,current_app,jsonify
import requests,json
from datetime import datetime
from api.models.db import db
from rediscluster  import RedisCluster


redis_manage = Blueprint('redis_manage',__name__,url_prefix='/admin/redis_manage')

class MyRedisCluster(object):
    def __init__(self):
        self.hosts = [
            {"host": "192.168.11.71", "port": 7000 },
            {"host": "192.168.11.72", "port": 7001 },
            {"host": "192.168.11.73", "port": 7002 },
        ]
        self.password = 'redis_user'
        self.rc =  RedisCluster(startup_nodes=self.hosts,decode_responses=True,password=self.password)

    def cluster_info(self):
        result = self.rc.cluster_info()
        return result

    def cluster_nodes(self):
        result = self.rc.cluster_nodes()
        return result

    def cluster_slots(self):
        result = self.rc.cluster_slots()
        return result


@redis_manage.route('/get_cluster_info',methods=('GET','POST'))
def get_cluster_info():
    client = MyRedisCluster()
    result = client.cluster_info()
    i = 0
    for key in result:
        if i==0:
            result = result[key]
            break
        i += 1
    return json.dumps(result)


def slots_human(slots):
    my_slots = {}
    for k in slots.keys():
        master = slots[k]['master']
        master_addr = master[0] + ":" + str(master[1])
        if not master_addr in my_slots.keys():
            my_slots[master_addr] = []
            my_slots[master_addr].append(k)
        else:
            my_slots[master_addr].append(k)
    return my_slots

@redis_manage.route('/get_cluster_nodes',methods=('GET','POST'))
def get_cluster_nodes():
    client = MyRedisCluster()
    result = client.cluster_nodes()
    info  = client.cluster_info()
    my_slots = slots_human(client.cluster_slots())
    node_list = []
    for item in result:
        address = item['host'] + ":" + str(item['port'])
        my_epoch  = info[address]['cluster_my_epoch']
        if not item['master']:
            slots = my_slots[address]
        else:
            slots = ''
        node_info = {
            "id":item['id'],
            "address": address,
            "flags": item['flags'],
            "master": item['master'],
            "node-epoch":my_epoch,
            "state": item.get('link-state'),
            "slots": str(slots),
            "slot-count": len(item['slots']),
        }
        node_list.append(node_info)
    return json.dumps(node_list)