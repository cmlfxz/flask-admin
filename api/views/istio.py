from flask import request,Blueprint,Response
import json,requests
from api.common.util import *
from kubernetes import client,config
from kubernetes.client.rest import ApiException
from api.controllers.istio import *
import yaml
import math

istio = Blueprint('istio',__name__, url_prefix='/admin/istio')



@istio.route('/get_custom_object', methods=('GET', 'POST'))
def get_custom_object():
    data = json.loads(request.get_data().decode("utf-8"))
    namespace = data.get("namespace").strip()
    name = data.get("name").strip() 
    plural = data.get("plural").strip()
    myclient = client.CustomObjectsApi()
    obj = myclient.get_namespaced_custom_object(group="networking.istio.io", version="v1alpha3",
                                                plural=plural,namespace=namespace,name=name)
    yamlName="data.yaml"
    with open(yamlName, 'w', encoding='UTF-8') as yaml_file:
        yaml.safe_dump(obj,yaml_file)
    yamlFile = open(yamlName, 'rb').read()
    response =  Response(yamlFile,content_type='application/octet-stream')
    response.headers['Content-Disposition'] = 'attachment; filename={}'.format(yamlName)
    return response

@istio.route('/update_custom_object', methods=('GET', 'POST'))
def update_custom_object():
    data = json.loads(request.get_data().decode("utf-8"))
    namespace = data.get("namespace").strip()
    name = data.get("name").strip() 
    plural = data.get("plural").strip() 
    body = data.get("body")
    # print(body)
    to_dict = yaml.load(body,yaml.FullLoader)

    myclient = client.CustomObjectsApi()
    result = myclient.patch_namespaced_custom_object(group="networking.istio.io", version="v1alpha3",
                                                     plural=plural,namespace=namespace,name=name,
                                                     body=to_dict)
    return json.dumps({'ok':result})

# 列出gateway
@istio.route('/get_gateway_list', methods=('GET', 'POST'))
def get_gateway_list():
    data = json.loads(request.get_data().decode("utf-8"))
    namespace = handle_input(data.get("namespace"))
    myclient = client.CustomObjectsApi()
    try:
        if namespace == "" or namespace == "all":
            obj = myclient.list_cluster_custom_object(group="networking.istio.io", version="v1alpha3",
                                                      plural="gateways")
        else:
            obj = myclient.list_namespaced_custom_object(namespace=namespace, group="networking.istio.io",
                                                         version="v1alpha3", plural="gateways")
    except ApiException as e:
        msg = get_exception(e)
        return json.dumps({'error': '获取列表失败', "msg": msg})
    gateways = obj['items']
    gateway_list = []
    for gateway in gateways:
        meta = gateway['metadata']
        spec = gateway['spec']
        name = meta['name']
        namespace = meta['namespace']
        time_str = meta['creationTimestamp']
        create_time = utc_to_local(time_str, utc_format='%Y-%m-%dT%H:%M:%SZ')
        selector = spec['selector']
        servers = spec['servers']
        domain_list = []
        for server in servers:
            domain = server['hosts']
            domain_list.append(domain)

        item = {
            "name": name,
             "namespace": namespace,
             "selector": selector,
             "servers": servers,
             "domain_list": domain_list,
             "create_time": create_time,
        }
        gateway_list.append(item)
    return json.dumps(gateway_list, indent=4)

# 列出vs
@istio.route('/get_virtual_service_list', methods=('GET', 'POST'))
def get_virtual_service_list():
    data = json.loads(request.get_data().decode("utf-8"))
    namespace = data.get("namespace").strip()
    myclient = client.CustomObjectsApi()
    # bug 没有vs的集群报错404，页面没具体显示
    try:
        if namespace == "" or namespace == "all":
            obj = myclient.list_cluster_custom_object(group="networking.istio.io", version="v1alpha3",
                                                      plural="virtualservices")
        else:
            obj = myclient.list_namespaced_custom_object(namespace=namespace, group="networking.istio.io",
                                                         version="v1alpha3", plural="virtualservices")
    except ApiException as e:
        msg = get_exception(e)
        return json.dumps({'error': '获取列表失败', "msg": msg})

    virtual_services = obj['items']
    virtual_service_list = []
    for virtual_service in virtual_services:
        meta = virtual_service['metadata']
        spec = virtual_service['spec']
        name = meta['name']
        namespace = meta['namespace']
        time_str = meta['creationTimestamp']
        create_time = utc_to_local(time_str, utc_format='%Y-%m-%dT%H:%M:%SZ')
        try:
            gateways = spec['gateways']
        except Exception as e:
            gateways = None
        hosts = spec['hosts']
        http = spec['http']
        item = {
            "name": name,
            "namespace": namespace,
            "gateways": gateways,
            "hosts": hosts,
            "http": http,
            "create_time": create_time,
        }
        virtual_service_list.append(item)
    return json.dumps(virtual_service_list, indent=4)

@istio.route('/update_vs', methods=('GET', 'POST'))
def update_vs():
    data = json.loads(request.get_data().decode('UTF-8'))
    print("接受到的数据:{}".format(data))
    namespace = data.get('namespace')
    vs_name = data.get('vs_name')
    new_weight = math.ceil(str_to_int(handle_input(data.get('new_weight'))))
    if (new_weight < 0 or new_weight > 100):
        return json.dumps({"fail": "新版本流量权重值需在1-100之间"})
    old_weight = 100 - new_weight
    return update_virtual_service(vs_name=vs_name, namespace=namespace,
        old_weight=old_weight, new_weight=new_weight)

# 添加超时
@istio.route('/update_timeout', methods=('GET', 'POST'))
def update_timeout():
    data = json.loads(request.get_data().decode('UTF-8'))
    print("接收到的数据:{}".format(data))
    namespace = data.get('namespace')
    vs_name = data.get('name')
    vs = get_vs_by_name(namespace, vs_name)
    if vs == None:
        return json.dumps({"error":"找不到该vs"})
    timeout_list = data.get('timeout_list')
    for item in timeout_list:
        index = item.get('index')
        timeout = item.get('timeout')
        vs['spec']['http'][index]['timeout'] = timeout
    try:   
        myclient = client.CustomObjectsApi()
        myclient.patch_namespaced_custom_object(group="networking.istio.io",
                version="v1alpha3",plural="virtualservices",name=vs_name,
                namespace=namespace, body=vs)
        return json.dumps({'ok': '更新成功'})
    except Exception as e:
        msg = get_exception(e)
        return json.dumps({'error': '更新超时异常', "msg": msg})

@istio.route('/delete_vs', methods=('GET', 'POST'))
def delete_vs():
    data = json.loads(request.get_data().decode('utf-8'))
    name  = data.get('name')
    namespace = data.get('namespace')
    myclient = client.CustomObjectsApi()
    try:
        body = client.V1DeleteOptions(propagation_policy='Foreground',grace_period_seconds=5)
        myclient.delete_namespaced_custom_object(group="networking.istio.io",
            version="v1alpha3",plural="virtualservices",namespace=namespace,
             name=name,body=body)
    except Exception as e:
        print(e)
        return json.dumps({"fail":"删除失败"})
    return json.dumps({"ok":"删除成功"})

@istio.route('/delete_dr', methods=('GET', 'POST'))
def delete_dr():
    data = json.loads(request.get_data().decode('utf-8'))
  
    namespace = handle_input(data.get('namespace'))
    list = data.get('name_list',[])
    if not list:
        return json.dumps({"fail":"参数错误"})
    myclient = client.CustomObjectsApi()
    for name in list:
        try:
            deleteOptions = client.V1DeleteOptions(propagation_policy='Foreground',
                                                   grace_period_seconds=5)
            myclient.delete_namespaced_custom_object(group="networking.istio.io",version="v1alpha3",
                                                     plural="destinationrules",namespace=namespace,
                                                     name=name,body=deleteOptions)
        except Exception as e:
            msg = get_exception(e)
            return json.dumps({'error': '删除'+name+'异常', "msg": msg})
    return json.dumps({"ok":"删除成功"})

@istio.route('/get_destination_rule_list', methods=('GET', 'POST'))
def get_destination_rule_list():
    data = json.loads(request.get_data().decode("utf-8"))
    namespace = data.get("namespace").strip()
    myclient = client.CustomObjectsApi()
    if namespace == "" or namespace == "all":
        obj = myclient.list_cluster_custom_object(group="networking.istio.io", version="v1alpha3",
                                                  plural="destinationrules")
    else:
        obj = myclient.list_namespaced_custom_object(namespace=namespace, group="networking.istio.io",
                                                     version="v1alpha3", plural="destinationrules")
    # obj是一个字典
    drs = obj['items']
    dr_list = []
    for dr in drs:
        meta = dr['metadata']
        spec = dr['spec']
        name = meta['name']
        namespace = meta['namespace']
        time_str = meta['creationTimestamp']
        create_time = utc_to_local(time_str, utc_format='%Y-%m-%dT%H:%M:%SZ')

        host = spec['host']
        subsets = trafficPolicy = None
        if 'subsets' in spec.keys():
            subsets = spec['subsets']
        if 'trafficPolicy' in spec.keys():
            trafficPolicy = spec['trafficPolicy']
        my_dr = {
            "name": name,
            "namespace": namespace,
            "host": host,
            "subsets": subsets,
            "trafficPolicy":trafficPolicy,
            "create_time":create_time,
        }
        dr_list.append(my_dr)
    return json.dumps(dr_list, indent=4)

# 熔断
@istio.route('/update_breaker', methods=('GET', 'POST'))
def update_breaker():
    data = json.loads(request.get_data().decode('utf-8'))
    namespace = data.get('namespace')
    dr_name = data.get('name')
    dr = get_dr_by_name(namespace,dr_name)
    myclient = client.CustomObjectsApi()

    breaker = data.get('breaker')
    connectionPool =  breaker.get('connectionPool')
    outlierDetection =  breaker.get('outlierDetection')

    if 'trafficPolicy' in dr['spec']:
        dr['spec']['trafficPolicy']['connectionPool']  =  connectionPool
        dr['spec']['trafficPolicy']['outlierDetection'] = outlierDetection
    else:
        dr['spec']['trafficPolicy'] = breaker
        print(dr['spec']['trafficPolicy'])
    try:
        myclient.patch_namespaced_custom_object(group="networking.istio.io",
            version="v1alpha3",plural="destinationrules",name=dr_name,
            namespace=namespace,body=dr)
    except Exception as e:
        print(e)
        return json.dumps({"异常":"更新失败"})
    return json.dumps({"ok": "更新成功"})
@istio.route('/delete_breaker', methods=('GET', 'POST'))
def delete_breaker():
    data = json.loads(request.get_data().decode('utf-8'))
    namespace = data.get('namespace')
    dr_name = data.get('name')
    dr = get_dr_by_name(namespace,dr_name)
    myclient = client.CustomObjectsApi()
    trafficPolicy = dr['spec']['trafficPolicy']
    if 'connectionPool' in trafficPolicy:
        trafficPolicy.pop("connectionPool")
    if 'outlierDetection' in trafficPolicy:
        trafficPolicy.pop("outlierDetection")
    print(trafficPolicy)
    if trafficPolicy:
        dr['spec']['trafficPolicy']= trafficPolicy
    else: 
        dr['spec'].pop('trafficPolicy')
    
    try:
        myclient.replace_namespaced_custom_object(group="networking.istio.io",
            version="v1alpha3",plural="destinationrules",name=dr_name,
            namespace=namespace, body=dr)
        
        return json.dumps({"ok":"删除熔断成功"})
    except Exception as e:
        msg = get_exception(e)
        return json.dumps({'error': '删除熔断异常', "msg": msg})

@istio.route('/update_loadBalancer', methods=('GET', 'POST'))
def update_loadBalancer():
    data = json.loads(request.get_data().decode('utf-8'))
    namespace = data.get('namespace')
    dr_name = data.get('name')
    dr = get_dr_by_name(namespace,dr_name)
    myclient = client.CustomObjectsApi()
    loadBalancer = data.get('loadBalancer')

    if 'trafficPolicy' in dr['spec']:
        dr['spec']['trafficPolicy']['loadBalancer']  =  loadBalancer
    else:
        dr['spec']['trafficPolicy'] = {'loadBalancer':loadBalancer}
        print(dr['spec']['trafficPolicy'])
    try:
        myclient.patch_namespaced_custom_object(group="networking.istio.io",
            version="v1alpha3",plural="destinationrules",name=dr_name,
            namespace=namespace,body=dr)
    except Exception as e:
        msg = get_exception(e)
        return json.dumps({'error': '更新loadBalance失败', "msg": msg})

    return json.dumps({"ok": "更新loadBalancer成功"})
@istio.route('/delete_loadBalancer', methods=('GET', 'POST'))
def delete_loadBalancer():
    data = json.loads(request.get_data().decode('utf-8'))
    namespace = data.get('namespace')
    dr_name = data.get('name')
    dr = get_dr_by_name(namespace,dr_name)
    
    myclient = client.CustomObjectsApi()

    trafficPolicy = dr['spec']['trafficPolicy']
    if 'loadBalancer' in trafficPolicy:
        trafficPolicy.pop("loadBalancer")
    print(trafficPolicy)
    if trafficPolicy:
        dr['spec']['trafficPolicy']= trafficPolicy
    else: 
        dr['spec'].pop('trafficPolicy')
    
    try:
        myclient.replace_namespaced_custom_object(group="networking.istio.io",
            version="v1alpha3", plural="destinationrules",name=dr_name,
            namespace=namespace,body=dr)
        
        return json.dumps({"ok":"删除负载均衡成功"})
    except Exception as e:
        msg = get_exception(e)
        return json.dumps({'error': '删除负载均衡异常', "msg": msg})
    

@istio.route('/update_versions', methods=('GET', 'POST'))
def update_versions():
    data = json.loads(request.get_data().decode('utf-8'))
    namespace = data.get('namespace')
    dr_name = data.get('name')
    dr = get_dr_by_name(namespace,dr_name)
    myclient = client.CustomObjectsApi()
    versions = data.get('versions')
    old_version =  versions.get('old')
    new_version =  versions.get('new')
    dr['spec']['subsets'] = [
        {
            "labels": {
                "version": old_version
            },
            "name": "old"
        },
        {
            "labels": {
                "version": new_version
            },
            "name": "new"
        }
    ]
    try:
         myclient.patch_namespaced_custom_object(group="networking.istio.io",
              version="v1alpha3",plural="destinationrules",name=dr_name,
              namespace=namespace,body=dr)
    except Exception as e:
        print(e)
        return json.dumps({"异常":"更新失败"})
    return json.dumps({"ok":"更新成功"})