from flask import Flask,jsonify,request,current_app,Blueprint
import json,requests
from api.common.util import *
from kubernetes import client,config
from api.controllers.istio_flow import *
import yaml
from api.common.k8s import set_config
import functools


def set_cluster(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        cluster_name = kwargs.get("cluster_name",None)
        print("装饰器获取到的集群名"+cluster_name)
        if not cluster_name:
            return "请求必须带上集群名称"
        ok,result = set_config(cluster_name)
        if not ok:
            return "设置k8s配置失败"
        return view(**kwargs)
    return wrapped_view


@set_cluster
def add_dr(**kwargs):
    project = kwargs.get('project',None)
    service = kwargs.get('service',None)
    old_version = kwargs.get('old_version',None)
    new_version = kwargs.get('new_version',None)
    print("{} {} {} {}".format(project,service,old_version,new_version))
    resources = IstioResources(project=project,service=service,env='prod',
                               old_version=old_version,new_version=new_version)
    resources.show()
    result = resources.add_dr()
    if not result:
        return False,'创建dr失败'
    return True,'创建dr成功'


@set_cluster
def add_vs_v2(**kwargs):
    try:
        project = kwargs.get('project',None)
        service= kwargs.get('service',None)
        old_weight= kwargs.get('old_weight',None)
        new_weight = kwargs.get('new_weight',None)
        release_type = int(kwargs.get('release_type'))
        service_type = int(kwargs.get('service_type'))
        domain = None
        if service_type < 2:
            domain = kwargs.get('domain',None)
    except KeyError:
        current_app.logger.debug("数据格式有误")
        return False,"数据格式有误"
    resources = None
    if release_type == 1:
        resources = IstioBlueGreenResources(project=project,service=service,env='prod',
                                   old_weight=old_weight,new_weight=new_weight,domain=domain)
    elif release_type == 2:
        match = kwargs.get('match',None)
        resources = IstioGrayResources(project=project,service=service,env='prod',
                                   old_weight=old_weight,new_weight=new_weight,match=match,domain=domain)
    else:
        return False,"不支持此种发布类型"
    resources.show()
    if service_type < 2:
        result = resources.add_nb_vs()
        if not result:
            return False,'创建nb vs失败'
    result = resources.add_vs()
    if not result:
        return False,'创建vs失败'
    return True,"创建vs成功"


@set_cluster
def update_vs_resource_v2(**kwargs):
    # data = json.loads(request.get_data().decode('utf-8'))
    try:
        project = kwargs.get('project',None)
        service= kwargs.get('service',None)
        old_weight= kwargs.get('old_weight',None)
        new_weight = kwargs.get('new_weight',None)
        release_type = int(kwargs.get('release_type'))
        service_type = int(kwargs.get('service_type'))
        domain = None
        if service_type < 2:
            domain =  kwargs.get('domain',None)
    except KeyError:
        current_app.logger.debug("数据格式有误")
        return False,"数据格式有误"
    resources = result = None
    if release_type == 1:
        resources = IstioBlueGreenResources(project=project,service=service,service_type=service_type,env='prod',
                                   old_weight=old_weight,new_weight=new_weight,domain=domain)
        result = resources.update_vs()
    elif release_type == 2:
        action = data['action']
        if action == 'new_take_over_traffic' or action == 'old_take_over_traffic':
            resources = IstioGrayResources(project=project,service=service,service_type=service_type,env='prod',
                                       old_weight=old_weight,new_weight=new_weight,domain=domain)
            result = resources.take_over_traffic()
        elif action=='recover_traffic' or action=='update_rule':
            match = kwargs.get('match',None)
            resources = IstioGrayResources(project=project,service=service,service_type=service_type,env='prod',
                                       old_weight=old_weight,new_weight=new_weight,domain=domain,match=match)
            result = resources.update_vs()
        else:
            return False,"不支持此更新动作"
    else:
        return False,"不支持此种发布类型"

    if not result:
        return False,'更新vs失败'
    return True,"更新vs成功"


@set_cluster
def delete_release_resources(**kwargs):

    try:
        project = kwargs.get('project',None)
        service= kwargs.get('service',None)
        old_version= kwargs.get('old_version',None)
        new_version = kwargs.get('new_version',None)
        release_status = int(kwargs.get('release_status'))
        release_type = int(kwargs.get('release_type'))
    except KeyError:
        current_app.logger.debug("数据格式有误")
        return False,"数据格式有误"

    print("release_status={}".format(str(release_status)))

    resources = None
    if release_type == 1:
        resources = IstioBlueGreenResources(project=project,service=service,env='prod',
                                            old_version=old_version,new_version=new_version,
                                            release_status=release_status)
    elif release_type == 2:
        match = kwargs.get('match',None)
        resources = IstioGrayResources(project=project,service=service,env='prod',
                                       old_version=old_version, new_version=new_version,
                                       release_status=release_status)
    else:
        return False,"不支持此种发布类型"
    result = resources.delete_resources()
    if not result:
        return False,'删除发布资源失败'
    return True,"删除发布资源成功"

@set_cluster
def get_service_deployments(**kwargs):
    namespace = kwargs.get('namespace',None)
    service = kwargs.get('service',None)
    print(namespace,service)
    labels = "app={}".format(service)
    myclient = client.AppsV1Api()
    deployments = myclient.list_namespaced_deployment(namespace=namespace,label_selector=labels)
    print(len(deployments.items))
    result = []
    if len(deployments.items) > 0:
        for item in deployments.items:
            result.append(item.metadata.name)
    return True,result


