from flask import  Blueprint,current_app,request
from api.models.project import Project,Env,Service
import json
from api.models.db import db
from api.common.util import handle_input,http_client
from api.models.transform import model_to_dict
from api.models.user import User
from sqlalchemy import or_
from api.views.istio_flow import get_service_deployments
from api.models.public import PublicTask,PublicLog,PublicDetail


project = Blueprint('project',__name__,url_prefix='/admin/project')


def result_to_list(obj):
    lists = []
    for item in obj:
        elem = item[0]
        lists.append(elem)
    return lists


# 获取项目的完整列表
@project.route('/get_project_list',methods=('GET','POST'))
def get_project_list():
    results  = db.session.query(Project).all()
    project_list = []
    for item in results:
        project_list.append(model_to_dict(item))
    return json.dumps(project_list)

# 获取环境的完整列表   
@project.route('/get_env_list',methods=('GET','POST'))
def get_env_list():
    results  = db.session.query(Env).all()
    env_list = []
    for item in results:
        env_list.append(model_to_dict(item))
    return json.dumps(env_list)



# 获取服务的完整列表
@project.route('/get_all_service_list',methods=('GET','POST'))
def get_all_service_list():
    results  = db.session.query(Service).all()
    service_list = []
    for item in results:
        service_list.append(model_to_dict(item))
    return json.dumps(service_list)

@project.route('/get_service_detail',methods=('GET','POST'))
def get_service_detail():
    data = json.loads(request.get_data().decode('utf-8'))
    id = int(data.get('id'))
    result = db.session.query(Service).filter(Service.id==id).first()
    return json.dumps(model_to_dict(result))

# 获取环境的名称列表
@project.route('/get_env_name_list',methods=('GET','POST'))
def get_env_name_list():
    results  = db.session.query(Env.name).all()
    env_names = result_to_list(results)
    current_app.logger.debug(env_names)
    return json.dumps(env_names)

# 根据项目获取服务
@project.route('/get_service_by_project',methods=('GET','POST'))
def get_service_by_project():
    data = json.loads(request.get_data().decode('utf-8'))
    project_name =  handle_input(data.get('project_name'))
    results = db.session.query(Service.name).filter(Service.project==project_name).distinct().all()
    service_name_list = []
    for service in results:
        service_name = service.name
        service_name_list.append(service_name)
    return json.dumps(service_name_list,indent=4)


# 获取项目的名称列表
@project.route('/get_project_name_list',methods=('GET','POST'))
def get_project_name_list():
    results  = db.session.query(Project.name).distinct().all()
    project_names = result_to_list(results)
    current_app.logger.debug(project_names)
    return json.dumps(project_names)


def get_env_by_id(ids):
    if len(ids) < 1 or not isinstance(ids,list):
        return []
    names = []
    # get env name by id 
    qnames = db.session.query(Env.name) \
        .filter(Env.id.in_(ids)) \
            .all() 
    if qnames:
        for item in qnames:
            names.append(item.name)
    return names

#根据项目获取环境名称
@project.route('/get_env_by_project',methods=('GET','POST'))
def get_env_by_project():
    data = json.loads(request.get_data().decode('utf-8'))
    project_name =  handle_input(data.get('project_name'))

    qres = db.session.query(Project.env_name) \
            .filter(Project.name==project_name) \
                .first()

    env_name_list = get_env_by_id(qres.env_name.split(','))

    return json.dumps(env_name_list,indent=4)


# 创建命名空间进入页面时获取初始信息
@project.route('/get_project_env',methods=('GET','POST'))
def get_project_env():
    results  = db.session.query(Project.name).distinct().all()
    if results:
        project_names = []
        for item in results:
            project_name = item.name
            project_names.append(project_name)  

        envs = db.session.query(Project.env_name) \
                .filter(Project.name==project_names[0]) \
                    .first()
        env_name_list = get_env_by_id(envs.env_name.split(','))
        return json.dumps({
            "project_names":project_names,
            "env_name_list":env_name_list
        })
    else:
        return json.dumps({'fail':"查不到相关数据"})



def get_deployments(project,service):
    namespace = "{}-prod".format(project)
    result = db.session.query(Env.clusters).filter(Env.name=="prod").first()
    cluster_name = None
    if result:
        cluster_name = result.clusters
    data ={
        "cluster_name":cluster_name,
        "namespace":namespace,
        "service": service
    }
    ok,deployments =get_service_deployments(**data)
    if ok:
        return deployments
    else:
        return []


# 根据项目获取服务
@project.route('/get_service_v3',methods=('GET','POST'))
def get_service_v3():
    data = json.loads(request.get_data().decode('utf-8'))
    project_name=  handle_input(data.get('project_name'))

    service_results = db.session.query(Service.id,Service.name) \
        .filter(Service.has_pipeline==1,Service.project==project_name).all()
    service_list = []
    if service_results:
        for item in service_results:
            service_name = item.name
            result = db.session.query(PublicDetail.status).filter(PublicDetail.service == service_name).filter(
                PublicDetail.status != 99).order_by(PublicDetail.create_time.desc()).first()
            release_status = None
            if result:
                release_status = result.status
            deployments = get_deployments(project_name,service_name)
            service = {
                'id': item.id,
                'name':item.name,
                "release_status":release_status,
                'deployments':deployments,
            }
            service_list.append(service)
    return json.dumps(service_list)


@project.route('/get_project_leader',methods=('GET','POST'))
def get_project_leader():
    data =  json.loads(request.get_data().decode('utf-8'))
    project = data.get('project',None)
    if project:
        qres = db.session.query(Project.leader) \
            .filter(Project.name == project) \
                .first()
        if qres:
            quser = db.session.query(User.username) \
                .filter(User.id == qres.leader) \
                    .first()
            if quser:
                return json.dumps({"leader":quser.username})
    return json.dumps({"fail":"获取项目负责人失败"})



