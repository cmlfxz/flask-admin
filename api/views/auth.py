import functools
from flask import Blueprint, request,g,current_app,jsonify,make_response,session
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer,SignatureExpired
import json,base64,sys
from flask_cors import *
from api.common.util import *
from api.models.db import db
from api.models.user import User
from api.models.blog import Blog
from api.common.redis import MyRedis
from pprint import pprint

auth = Blueprint('auth', __name__, url_prefix='/admin/auth',template_folder='templates')

CORS(auth, supports_credentials=True, resources={r'/*'})

# @auth.after_app_request
# def after(resp):
#     resp = make_response(resp)
#     resp.headers['Access-Control-Allow-Origin'] = '*'
#     resp.headers['Access-Control-Allow-Methods'] = 'GET,POST,OPTIONS,PATCH,DELETE'
#     resp.headers['Access-Control-Allow-Headers'] = 'x-requested-with,Content-Type,token,cluster_name,es_cluster_name,user,user_id,username,X-B3-TraceId,X-B3-SpanId,X-B3-Sampled'
#     return resp

# @auth.before_app_request
# def before():
#     if request.method == 'POST' :
#         # print(request.headers)
#         if request.headers.get("content-type",None) == "application/json;charset=UTF-8":
#             try:
#                 # print(request.headers['content-type'])
#                 if request.get_data():
#                     data = json.loads(request.get_data().decode('utf-8'))
#                     current_app.logger.debug("{}收到的数据:{}".format(request.url.rsplit('/')[-1],request.get_data().decode('utf-8')))
#                     # current_app.logger.debug(request.get_data().decode('utf-8'))
#             except Exception as e:
#                 current_app.logger.debug("数据json解码失败")
#                 # return json.dumps({"fail":"数据json解码失败"})
#     else:
#         pass

@auth.route('/login',methods=('GET','POST'))
def login():
    if request.method == 'POST':
        try:
            data = json.loads(request.get_data().decode('utf-8'))
            current_app.logger.debug("login收到的数据:{}".format(data))
            username = data.get("username").strip()
            password = data.get("password").strip()

            sql = User.query.filter(User.username==username)
            user =sql.first()

            error = None

            if user is None:
                error = 'Incorrect username.'
            # elif password != my_decode(user.password):
            elif password != user.password:
                error = 'Incorrect password.'
            print(error)
            if error is None:
                token = user.generate_auth_token()
                token_key = 'token'+str(user.id)
                session[token_key] = token
                return jsonify({'msg':'ok','token':token,"userId":user.id,"userRole":user.role})
            return jsonify({'msg':'fail','reason':'账号密码验证失败'})
        except Exception as e:
            print(str(e))
        return jsonify({'msg': 'fail', 'reason': '数据库连接失败'})

def verify_token(view):
    @functools.wraps(view)
    def wrapped_view(*args,**kwargs):
        try:
            token = request.headers['token']
            # print("token:",token)
        except Exception :
            return make_response(json.dumps({"fail":"缺少token"}),401)
        
        result = User.verify_auth_token(token)
        if result == 400:
            return make_response(json.dumps({"fail":"无效token"}),400)
        elif result == 401:
            return make_response(json.dumps({"fail":"token已过期"}),401)
        else:
            g.user = result
            # print("g.user:",g.user)
        return view(*args,**kwargs)
    return wrapped_view