from datetime import datetime
from flask import Blueprint,g,request,current_app
from api.models.db import db
from api.models.user import User
from api.models.blog import Blog
from api.common.util import handle_input
from api.models.transform import model_to_dict
from sqlalchemy.sql import and_, or_, not_
from sqlalchemy import func
import json

blog = Blueprint('blog',__name__,url_prefix='/admin/blog')

def get_dict(item):
    return item._asdict() 

def rset_to_list(results):
    post_list = []
    for item in results:
        to_dict = get_dict(item)
        username = to_dict['username']
        blog = to_dict['Blog']
        obj = {
            "user":username,
            "id":blog.id,
            "title":blog.title,
            "content":blog.content,
            "create_time":blog.create_time.strftime( '%Y-%m-%d %H:%M:%S'),
            "view_count":blog.view_count,
            "last_view_time":blog.last_view_time.strftime('%Y-%m-%d %H:%M:%S')
        }
        post_list.append(obj)
    return post_list

@blog.route('/add',methods={'GET','POST'})
def add():
    if request.method == 'POST':
        data = json.loads(request.get_data().decode('utf-8'))
        title = handle_input(data.get('title'))
        content = handle_input(data.get('content'))
        
        username = handle_input(request.headers['username'])
        print(username)
        user = User.query.filter(User.username == username).first()
        try:
            blog = Blog(title=title,content=content,author_id=user.id)
            db.session.add(blog)
            db.session.commit()
            return json.dumps({"ok":"添加文章成功"})
        except Exception as e:
            print(e)
        return json.dumps({"fail":"添加文章失败"})

@blog.route('/update', methods=('GET', 'POST'))
def update():
    data = json.loads(request.get_data().decode('utf-8'))
    id = handle_input(data.get('id'))
    title = handle_input(data.get('title'))
    content = handle_input(data.get('content'))
    try:
        Blog.query.filter(Blog.id==id).update({'title':title,'content':content})
        db.session.commit()
        return json.dumps({"ok":"更新文章成功"})
    except Exception as e:
        return json.dumps({"fail":"更新文章失败","error":e})

@blog.route('/update_view', methods=('GET', 'POST'))
def update_view():
    data = json.loads(request.get_data().decode('utf-8'))
    id = handle_input(data.get('id'))
    try:
        sql = Blog.query.filter(Blog.id==id)
        if sql.first():
            blog = sql.first()
            view_count = blog.view_count+1
            print(view_count)
            sql.update({'view_count':view_count,'last_view_time':datetime.now()})
            db.session.commit()
            return json.dumps({"ok":"更新文章访问成功"})
        else:
            return json.dumps({"fail":"文章不存在"})
    except Exception as e:
        return json.dumps({"fail":"更新文章访问失败","error":e})


@blog.route('/delete', methods=('GET', 'POST'))
def delete():
    data = json.loads(request.get_data().decode('utf-8'))
    id = handle_input(data.get('id'))
    try:
        blog = Blog.query.filter(Blog.id==id).first()
        db.session.delete(blog)
        db.session.commit()
        return json.dumps({"ok":"删除文章成功"})
    except Exception as e:
        return json.dumps({"fail":"删除文章失败","error":e})

@blog.route('/detail',methods={'GET','POST'})
def detail():
    data = json.loads(request.get_data().decode('utf-8'))
    id = data.get('id')
    sql = Blog.query.filter(Blog.id==id)
    result = sql.first()
    blog = model_to_dict(result)
    return json.dumps(blog,indent=4)



@blog.route('/v2/list',methods={'GET','POST'})
def list_v2():
    data = json.loads(request.get_data().decode('utf-8'))
    key = data.get('key',None)
    order = data.get('order',None)
    selectContent = data.get('selectContent',None)
    pageSize = int(request.args.get('pageSize',10))
    page = int(request.args.get('page',1))

    sql =  db.session.query(User.username,Blog).join(User,Blog.author_id==User.id)
    #构建查询条件部分
    if selectContent:
        condition = "%{}%".format(selectContent)
        sql = sql.filter((Blog.title+Blog.content).like(condition))
    #构建排序部分
    if key:
        if key == "view_count":
            order_key = Blog.view_count
        else:
            return json.dumps({"fail": "排序字段不支持"})
        if order == "desc":
            sql = sql.order_by(order_key.desc())
        else:
            sql  = sql.order_by(order_key.asc())

    total = len(sql.all())
    results  = sql.order_by(Blog.create_time.desc()).limit(pageSize).offset((page-1)*pageSize).all()
    post_list = rset_to_list(results)
    return json.dumps({"post_list":post_list,"total":total})
