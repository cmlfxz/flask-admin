from flask import Blueprint, request,current_app,jsonify
import requests,json
from datetime import datetime
from api.controllers.salt import SaltApi
salt = Blueprint('salt',__name__,url_prefix='/admin/salt')

@salt.route('/get_keys',methods=('POST','GET'))
def get_keys():
    salt = SaltApi()
    result,err = salt.all_key()
    if err != "":
        return json.dumps({"fail":"获取数据失败","error":err})
    return json.dumps({"ok":"获取数据成功","data":result})

@salt.route('/accept_keys',methods=('POST','GET'))
def accept_keys():
    salt = SaltApi()
    data = json.loads(request.get_data().decode('utf-8'))
    key = data.get('key')
    result, err = salt.accept_key(key)
    if err != "":
        return json.dumps({"fail":"操作失败","error":err})
    return json.dumps({"ok":"操作成功"})

@salt.route('/delete_keys',methods=('POST','GET'))
def delete_keys():
    salt = SaltApi()
    data = json.loads(request.get_data().decode('utf-8'))
    key = data.get('key')
    result, err = salt.delete_key(key)
    if err != "":
        return json.dumps({"fail":"操作失败","error":err})
    return json.dumps({"ok":"操作成功"})


@salt.route('/async_cmd',methods=('POST','GET'))
def async_cmd():
    salt = SaltApi()
    data = json.loads(request.get_data().decode('utf-8'))
    print(data)
    t = data.get('target',None)
    method = data.get('method','test.ping')
    arg = data.get('arg',None)
    target_type = data.get('target_type','glob')
    target = t
    if not target or not method:
        return json.dumps({"fail":"传参错误"})
    jid, err = salt.salt_async_command(target,target_type,method,arg,)
    if err != "":
        return json.dumps({"fail":"操作失败","error":err})
    result = salt.get_jid_ret(jid)
    return json.dumps({"ok":"操作成功","result":result})