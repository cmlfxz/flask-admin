from flask import Blueprint,request
from api.models.db import db
from api.models.project import Service,Env
from api.models.public import PublicTask,PublicLog,PublicDetail
from api.models.transform import model_to_dict
from api.common.util import string_to_list,list_to_string,http_client
import json
from datetime import datetime
from api.views.istio_flow import add_dr,add_vs_v2,update_vs_resource_v2,delete_release_resources

public = Blueprint('public',__name__, url_prefix='/flow/public',template_folder='templates')

# 1、创建  2、发版成功（测试新版） 3、发版失败 4、灰度测试（金丝雀，灰度）  11、回滚成功 12、回滚失败  13、下线失败 99、发布完成
Public_Create = 1
Public_OK = 2   # 蓝绿
Public_Fail = 3
Public_Gray_Test = 4   #灰度
Public_Gray_Fail = 5
Public_Gray_OK = 6
Public_New_OK = 7
Public_New_Fail = 8
Public_Roll_OK = 11
Public_Roll_Fail = 12
Public_Offline_Fail = 13
Public_End = 99

# 21、蓝绿发布(新版接管流量) 22、旧版接管流量  31、 灰度发布(含金丝雀) 32、新版接收所有流量 33、旧版就收所有流量 99、发布完成（下线操作）
BG_New_Take_Over = 21
BG_Old_Take_Over = 22
Gray_Test = 31
Gray_New_Take_Over = 32
Gray_Old_Take_Over = 33
# Public_End = 99

# action
PublicNew = "发新版"
RollOut = "回滚"

@public.route('/public_list',methods={'GET','POST'})
def public_list():
    sql = db.session.query(PublicTask).order_by(PublicTask.create_time.desc())
    public_list = []
    results = sql.all()
    if len(results) >0:
        for item in results:
            public = model_to_dict(item)
            if public['match']:
                public['match'] = json.loads(public['match'])
            public_list.append(public)
    return json.dumps(public_list)

@public.route('/public_detail_list',methods={'GET','POST'})
def public_detail_list():
    sql = db.session.query(PublicDetail).order_by(PublicDetail.create_time.desc())
    public_detail_list = []
    results = sql.all()
    if len(results) >0:
        for item in results:
            detail = model_to_dict(item)
            public_detail_list.append(detail)
    return json.dumps(public_detail_list)

@public.route('/public_log_list',methods={'GET','POST'})
def public_log_list():
    sql = db.session.query(PublicLog).order_by(PublicLog.create_time.desc())
    public_log_list = []
    results = sql.all()
    if len(results) >0:
        for item in results:
            log = model_to_dict(item)
            public_log_list.append(log)
    return json.dumps(public_log_list)


def record_public_log(release_id,message):
    log = PublicLog(release_id=release_id,message=message)
    try:
        db.session.add(log)
        db.session.commit()
    except Exception as e:
        print(e)

def update_public_status(public_id,status,match=None):
    sql = db.session.query(PublicTask).filter(PublicTask.id == public_id)
    if match != None:
        sql.update({
            "match":json.dumps(match),
            "status": status,
            "update_time": datetime.now()
        })
        return
    sql.update({
        "status": status,
        "update_time": datetime.now()
    })

def get_service_info(service):
    result = db.session.query(Service).filter(Service.name == service).first()
    domain = service_type = None
    if result:
        domain = "prod-" + result.domain
        service_type = result.type
    return domain,service_type

@public.route('/create_release_v2',methods={'GET','POST'})
def create_release_v2():
    data = json.loads(request.get_data().decode('utf-8'))
    project=data.get("project")
    services=data.get("services")
    service_details = data.get("service_details")
    details = json.dumps(service_details)
    release_type = int(data.get("release_type"))
    release = match = status = None
    new_weight = data.get('new_weight')
    old_weight = data.get('old_weight')
    if release_type ==1:
        release = PublicTask(
            project=project,
            service_list=list_to_string(services),
            public_type=release_type,
            status=Public_Create, #创建
            service_details = details,
        )
        status = BG_New_Take_Over
    elif release_type==2:
        match = data.get('match')
        release = PublicTask(
            project=project,
            service_list=list_to_string(services),
            public_type=release_type,
            status=Public_Create, #创建
            service_details = details,
            match=json.dumps(match),
        )
        status = Gray_Test
    else:
        return json.dumps({"fail":"未知发布类型"})

    db.session.add(release)
    db.session.flush()

    # 发布详情入库
    release_id = release.id
    for service in service_details:
        val = service_details[service]
        new_version = val['new_version']
        old_version = val['old_version']

        details = PublicDetail(
            release_id=release_id,
            project=project,
            service=service,
            new_version=new_version,
            old_version=old_version,
            new_weight=new_weight,
            old_weight=old_weight,
            public_type =release_type,
            status=status,
        )
        db.session.add(details)
        db.session.commit()

        domain,service_type = get_service_info(service)
        data = {
            "env":"prod",
            "project":project,
            "service": service,
            "old_version":old_version,
            "old_weight":old_weight,
            "new_version":new_version,
            "new_weight":new_weight,
            "domain":domain,
            "release_type": release_type,
            "service_type":service_type
        }
        if release_type == 2:
            data['match'] = match

        result = db.session.query(Env.clusters).filter(Env.name=="prod").first()
        cluster_name = None
        if result:
            cluster_name = result.clusters

        data['cluster_name'] = cluster_name
        ok,result = add_dr(**data)
        if not ok:
            message = "创建{} dr失败".format(service)
            record_public_log(release_id,message)
            update_public_status(release_id,Public_Fail)
            return json.dumps({"fail":message})
        #创建更新vs
        ok2,result = add_vs_v2(**data)
        if not ok2:
            message = "创建{} vs失败".format(service)
            record_public_log(release_id, message)
            update_public_status(release_id, Public_Fail)
            return json.dumps({"fail":message})
    if release_type == 1:
        update_public_status(release_id, Public_OK)
    elif release_type ==2 :
        update_public_status(release_id, Public_Gray_Test)
    return json.dumps({"ok":"创建批量发布成功"})

@public.route('/update_release_v2',methods={'GET','POST'})
def update_release_v2():
    data = json.loads(request.get_data().decode('utf-8'))
    # name=data.get("name")
    id = data.get("id")
    project=data.get("project")
    # 这个数据是从数据库赋值给前端的
    services=data.get("service_list")
    service_list = services.split(",")
    new_weight=data.get("new_weight")
    old_weight=data.get("old_weight")
    release_type = int(data.get("public_type"))
    action = None
    if release_type == 1:
        if new_weight == 100 and old_weight == 0:
            action = PublicNew
        elif new_weight == 0 and old_weight == 100:
            action = RollOut
        else:
            return json.dumps({"fail":"权重不对，请检查前端传参"})
        UpdateFail = False
        for service in service_list:
            result = db.session.query(Service).filter(Service.name==service).first()
            domain = service_type = None
            if result:
                domain = "prod-"+result.domain
                service_type = result.type
            data = {
                "env":"prod",
                "project":project,
                "service": service,
                "old_weight":old_weight,
                "new_weight":new_weight,
                "domain":domain,
                "release_type": release_type,
                "service_type":service_type
            }
            
            result = db.session.query(Env.clusters).filter(Env.name=="prod").first()
            cluster_name = None
            if result:
                cluster_name = result.clusters

            data['cluster_name'] = cluster_name
            #更新vs
            update_detail_sql = db.session.query(PublicDetail).filter(PublicDetail.service==service)
            ok,result = update_vs_resource_v2(**data)

            if not ok:
                message = "{} {}失败".format(service,action)
                record_public_log(id,message)
                #更新新版  发邮件或者钉钉

                UpdateFail = True
                break
            else:
                message = "{} {}成功".format(service, action)
                record_public_log(id,message)
                if action==PublicNew:
                    update_detail_sql.update({
                        "new_weight": new_weight,
                        "old_weight": old_weight,
                        "status":BG_New_Take_Over,
                        "update_time":datetime.now()

                    })
                else:
                    update_detail_sql.update({
                        "new_weight": new_weight,
                        "old_weight": old_weight,
                        "status":BG_Old_Take_Over,
                        "update_time":datetime.now()
                    })

        release_status = None
        #失败更新任务状态
        if UpdateFail:
            if action==PublicNew:
                release_status = Public_Fail
            else:
                release_status = Public_Roll_Fail
            update_public_status(id,release_status)
            return json.dumps({"fail":"{}失败".format(action)})
        else:
            # 成功更新任务状态
            if action==PublicNew:
                release_status = Public_OK
            else:
                release_status = Public_Roll_OK
            update_public_status(id,release_status)
            return json.dumps({"ok":"{}成功".format(action)})
    return json.dumps({"fail":"暂时不支持此种发布类型"})

action_service_status_map = {
    "new_take_over_traffic": Gray_New_Take_Over,
    "old_take_over_traffic": Gray_Old_Take_Over,
    "recover_traffic": Gray_Test,
    "update_rule": Gray_Test
}

update_fail_map = {
    "new_take_over_traffic": Public_New_Fail,
    "old_take_over_traffic": Public_Roll_Fail,
    "recover_traffic": Public_Gray_Fail,
    "update_rule": Public_Gray_Fail
}

update_ok_map = {
    "new_take_over_traffic": Public_New_OK,
    "old_take_over_traffic": Public_Roll_OK,
    "recover_traffic": Public_Gray_OK,
    "update_rule": Public_Gray_OK
}


@public.route('/update_gray_release',methods={'GET','POST'})
def update_gray_release():
    data = json.loads(request.get_data().decode('utf-8'))
    id = data.get("id")
    project=data.get("project")
    services=data.get("service_list")
    service_list = services.split(",")
    new_weight=data.get("new_weight")
    old_weight=data.get("old_weight")
    release_type = int(data.get("public_type"))
    action = data.get('action',None)
    match = data.get('match',None)

    updateFail = False
    for service in service_list:
        domain,service_type = get_service_info(service)
        data = {
            "env": "prod",
            "project": project,
            "service": service,
            "old_weight": old_weight,
            "new_weight": new_weight,
            "domain": domain,
            "release_type": release_type,
            "service_type": service_type,
            "action":action
        }
        if action == "recover_traffic" or action == 'update_rule':
            data['match'] = match
        
        result = db.session.query(Env.clusters).filter(Env.name=="prod").first()
        cluster_name = None
        if result:
            cluster_name = result.clusters

        data['cluster_name'] = cluster_name
        # 更新vs
        ok, result = update_vs_resource_v2(**data)
        if not ok:
            message = "{} {}失败".format(service, action)
            record_public_log(id, message)
            updateFail = True
            break
        else:
            message = "{} {}成功".format(service, action)
            record_public_log(id, message)
            service_status = action_service_status_map.get(action,None)
            update_detail_sql = db.session.query(PublicDetail).filter(PublicDetail.service == service) \
                .filter(PublicDetail.status != Public_End)
            update_detail_sql.update({
                "new_weight": new_weight,
                "old_weight": old_weight,
                "status": service_status,
                "update_time": datetime.now()
            })

    if updateFail:
        release_status = update_fail_map.get(action,None)
        update_public_status(id, release_status)
        return json.dumps({"fail": "{}失败".format(action)})
    else:
        # 成功更新任务状态
        release_status = update_ok_map.get(action,None)
        update_public_status(id, release_status,match=match)
        return json.dumps({"ok": "{}成功".format(action)})

@public.route('/offline_release_v2',methods={'GET','POST'})
def offline_release_v2():
    data = json.loads(request.get_data().decode('utf-8'))
    id = data.get("id")
    project=data.get("project")
    services=data.get("service_list")
    service_list = services.split(",")
    details = data.get("service_details")
    service_details = json.loads(details)
    release_type = int(data.get("public_type"))
    public_status = int(data.get("status"))

    offlineFail = False
    for service in service_details:
        val = service_details[service]
        new_version = val['new_version']
        old_version = val['old_version']
        # print(service, new_version, old_version)
        service_status = None
        detail_sql = db.session.query(PublicDetail).filter(PublicDetail.service==service)\
                        .filter(PublicDetail.status!=Public_End)
        result = detail_sql.order_by(PublicDetail.create_time.desc()).first()
        if result:
            service_status  = result.status
        data = {
            "env": "prod",
            "project": project,
            "service": service,
            "old_version":old_version,
            "new_version":new_version,
            "release_type": release_type,
            "release_status": service_status,
        }
        result = db.session.query(Env.clusters).filter(Env.name=="prod").first()
        cluster_name = None
        if result:
            cluster_name = result.clusters

        data['cluster_name'] = cluster_name
        ok,result = delete_release_resources(**data)
        if not ok:
            offlineFail = True
            message = "服务:{}下线失败".format(service)
            record_public_log(id,message)
            break
        else:
            message = "服务:{}下线成功".format(service)
            record_public_log(id,message)
            detail_sql.update({
                "status":Public_End,
                "update_time":datetime.now()
            })
    if offlineFail:
        update_public_status(id,Public_Offline_Fail)
        return json.dumps({"fail":"服务下线失败，请查看后台日志"})

    update_public_status(id, Public_End)
    return json.dumps({"ok":"下线成功"})



