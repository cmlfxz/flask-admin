from flask import Blueprint,g,request,current_app
from api.models.db import db
from api.models.setting import Menu,Operation
from api.models.user import User,Role

from api.common.util import handle_input,my_encode
from sqlalchemy.exc import IntegrityError,InvalidRequestError
from sqlalchemy.sql import func
from api.views.auth import verify_token
from api.models.transform import model_to_dict
from api.controllers.setting import *
import json


setting = Blueprint('setting',__name__, url_prefix='/admin/setting',template_folder='templates')


@setting.route('/get_user_resource',methods={'GET','POST'})
@verify_token
def get_user_resource():
    username = request.headers.get('username')
    role_name =  get_user_role(username)
    if not role_name:
        return json.dumps({"fail":"获取不到用户角色"})
    user_resource = {
        'menu_list':[],
        'op_list':[],
    }
    menus = get_user_menu(role_name)
    if menus:
        user_resource['menu_list'] = menus
    ops = get_user_operation(role_name)
    if ops:
        user_resource['op_list'] = ops
    return json.dumps(user_resource)

        
@setting.route('/get_menu_list',methods={'GET','POST'})
def get_menu_list():
    rset = db.session.query(Menu).order_by(Menu.create_time.desc()).all()
    
    menu_list = []
    for item in rset:
        menu = model_to_dict(item)
        menu_list.append(menu)
    return json.dumps(menu_list)

from pprint import pprint
@setting.route('/get_menu_tree',methods={'GET','POST'})
def get_menu_tree():
    root_menu_list = db.session.query(Menu).filter(Menu.enable==1).filter(Menu.parent_id == 0).all()
    pprint(root_menu_list)
    menu_tree = []
    if len(root_menu_list) > 0:
        for item in root_menu_list:
            menu = item_to_menu(item)
            get_clild_resource(menu.get('menu_id'),menu)
            menu_tree.append(menu)
    return json.dumps(menu_tree)

@setting.route('/menu_add',methods={'GET','POST'})
def menu_add():
    error = None
    if request.method == 'POST':
        data = json.loads(request.get_data().decode('utf-8'))
        current_app.logger.debug('menu.add收到的数据:{}'.format(data))
        menu = data.get('menu')
        name = menu.get('name')
        menu_id = menu.get('menu_id')
        parent_id = menu.get('parent_id')
        parent = menu.get('parent')
        icon = menu.get('icon')
        route_name = menu.get('route_name')
        route_path = menu.get('route_path')
        api = menu.get('api')
        tag = menu.get('tag')

        try:
            item = Menu(
                name=name,
                menu_id=menu_id,
                parent_id=parent_id,
                parent=parent,
                icon=icon,
                route_name=route_name,
                route_path=route_path,
                api=api,tag=tag
            )
            db.session.add(item)
            db.session.commit()
            return json.dumps({"ok":"添加菜单成功"})
        except Exception as e:
            current_app.logger.error(e)
            error = '数据插入异常'   
    return json.dumps({"fail":error})

@setting.route('/menu_update', methods=('GET', 'POST'))
def menu_update():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('menu.update收到的数据:{}'.format(data))

    menu = data.get('menu')
    id = menu.get('id')
    name = menu.get('name')
    menu_id = menu.get('menu_id')
    parent_id = menu.get('parent_id')
    parent = menu.get('parent')
    icon = menu.get('icon')
    route_name = menu.get('route_name')
    route_path = menu.get('route_path')
    api = menu.get('api')
    tag = menu.get('tag')
    try:
        Menu.query.filter(Menu.id==id).update({
            'name':name,
            'menu_id':menu_id,
            'parent_id':parent_id,
            'parent':parent,
            'icon':icon,
            'route_name':route_name,
            'route_path':route_path,
            'api': api,
            'tag': tag,
        })
        db.session.commit()
        return json.dumps({"ok":"更新成功"})
    except Exception as e:
        return json.dumps({"fail":"更新失败"})

@setting.route('/menu_delete', methods=('GET', 'POST'))
def menu_delete():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('menu.delete收到的数据:{}'.format(data))
    id = handle_input(data.get('id'))
    try:
        menu = Menu.query.filter(Menu.id==id).first()
        db.session.delete(menu)
        db.session.commit()
        return json.dumps({"ok":"删除成功"})
    except Exception as e:
        return json.dumps({"fail":"删除失败","error":e})

@setting.route('/get_menu_name',methods={'GET','POST'})
def get_menu_name():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('get_menu_name收到的数据:{}'.format(data))
    id = handle_input(data.get('id'))
    
    rset = db.session.query(Menu.name).filter(Menu.menu_id == id).first()
    name = ""
    if rset:
        name  = rset.name
        return json.dumps(name)
    else:
        return json.dumps({"fail":"获取菜单名失败"})

@setting.route('/menu_select', methods={'GET', 'POST'})
def menu_select():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug("menu select接收到的数据{}".format(data))
    selectContent = data.get('selectContent')
    condition = "%{}%".format(selectContent)
    # Menu.route_name
    sql = db.session.query(Menu).filter((Menu.name + func.ifnull(Menu.route_name,'')).like(condition)).order_by(Menu.create_time.desc())
    menu_list = []
    results = sql.all()
    if len(results) > 0:
        for menu in results:
            menu_list.append(model_to_dict(menu))
    return json.dumps(menu_list)

@setting.route('/get_new_menu_id',methods={'GET','POST'})
def get_new_menu_id():
    menu = db.session.query(Menu.menu_id).order_by(Menu.menu_id.desc()).first()
    return json.dumps({ "menu_id": menu.menu_id + 1 })

