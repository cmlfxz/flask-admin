from flask import Blueprint,request,current_app
from api.models.db import db
from api.models.cmdb import ServerInfo,ServerUpdateLog,ServerLog,ServerUser
from api.common.util import *
from api.common.host import  *
import json,datetime
from .auth import verify_token
from api.controllers.cmdb import  *
from concurrent.futures import ThreadPoolExecutor
executor = ThreadPoolExecutor(10)

from aliyunsdkcore.client import  AcsClient
from aliyunsdkcore.acs_exception.exceptions import  ClientException
from aliyunsdkcore.acs_exception.exceptions import  ServerException
# from aliyunsdkecs.request.v20140526 import DescribeInstancesRequest
from aliyunsdkecs.request.v20140526.DescribeInstancesRequest import DescribeInstancesRequest
from aliyunsdkecs.request.v20140526 import StopInstanceRequest
from aliyunsdkslb.request.v20140515.DescribeRegionsRequest import  DescribeRegionsRequest
from aliyunsdkbssopenapi.request.v20171214.QueryBillOverviewRequest import  QueryBillOverviewRequest
from aliyunsdkbssopenapi.request.v20171214.QueryAccountTransactionsRequest import  QueryAccountTransactionsRequest
from api.models.aliyun import CloudAccount,CloudKeyPair
from api.models.transform import model_to_dict


aliyun = Blueprint('aliyun', __name__, url_prefix='/admin/aliyun')

def get_aliyun_ram():
    pool = SingletonDBPool()
    conn = pool.connect()
    result = conn.query(CloudAccount).filter(CloudAccount.cloud_type == 1).first()
    print(result)
    if result:
        key = result.access_key_id
        secret = result.access_secret
        return key, secret
    else:
        print("数据库没有此云密钥配置")
        return None,None
class AliApi(object):
    def __init__(self,key=None,secret=None,region_id="cn-shenzhen"):
        if(not key or not secret):
            key,secret = get_aliyun_ram()
            self.key = key
            self.secret = secret
            self.region_id = region_id
            self.client = self.get_client()
    def get_client(self):
        return AcsClient(self.key,self.secret,self.region_id)

    def server_list(self,PageNum=1,PageSize=10):
        request = DescribeInstancesRequest()
        request.set_PageNumber(PageNum)
        request.set_PageSize(PageSize)
        response = self.client.do_action_with_exception(request)
        # print(str(response, encoding='utf-8'))
        return str(response, encoding='utf-8')

    def region_list(self):
        request = DescribeRegionsRequest()
        request.set_accept_format('json')
        response = self.client.do_action_with_exception(request)
        print(str(response, encoding='utf-8'))
        return str(response, encoding='utf-8')

    def bill_overview(self,BillingCycle="2021-01"):
        request = QueryBillOverviewRequest()
        request.set_accept_format('json')
        request.set_BillingCycle(BillingCycle)
        response = self.client.do_action_with_exception(request)
        print(str(response, encoding='utf-8'))
        return str(response, encoding='utf-8')

    #获取交易流水
    def account_transactions(self,PageNum=1,PageSize=10):
        request = QueryAccountTransactionsRequest()
        request.set_accept_format('json')

        request.set_PageNum(PageNum)
        request.set_PageSize(PageSize)

        response = self.client.do_action_with_exception(request)
        print(str(response, encoding='utf-8'))
        return str(response, encoding='utf-8')

@aliyun.route('/get_server_list', methods={'GET', 'POST'})
@verify_token
def get_server_list():
    pageSize = int(request.args.get('pageSize',10))
    page = int(request.args.get('page',1))
    region_id = request.args.get('region_id', "cn-shenzhen")
    aliApi = AliApi(region_id=region_id)
    try:
        return aliApi.server_list(PageNum=page,PageSize=pageSize)
    except Exception as e:
        print("异常:%r" % e)
    return json.dumps({"fail":"获取实例信息失败"})


@aliyun.route('/get_region_list', methods={'GET', 'POST'})
@verify_token
def get_region_list():
    aliApi = AliApi()
    try:
        return  aliApi.region_list()
    except Exception as e:
        print("异常:%r" % e)
    return json.dumps({"fail":"获取区域信息失败"})

@aliyun.route('/get_bill_overview', methods={'GET', 'POST'})
def get_bill_overview():
    aliApi = AliApi()
    try:
        month = datetime.datetime.now().strftime("%Y-%m")
        return  aliApi.get_bill_overview(BillingCycle=month)
    except Exception as e:
        print("异常:%r" % e)
    return json.dumps({"fail":"获取费用总览失败"})

@aliyun.route('/get_account_transactions', methods={'GET', 'POST'})
def get_account_transactions():
    pageSize = int(request.args.get('pageSize',10))
    page = int(request.args.get('page',1))
    aliApi = AliApi()
    try:
        return  aliApi.account_transactions(PageNum=page,PageSize=pageSize)
    except Exception as e:
        print("异常:%r" % e)
    return json.dumps({"fail":"获取交易流水失败"})


@aliyun.route('/get_cloud_key_pair', methods={'GET', 'POST'})
@verify_token
def get_cloud_key_pair():
    result = db.session.query(CloudKeyPair).filter(CloudKeyPair.tag=='aliyun').filter(CloudKeyPair.name=='Fr').first()
    if result:
        key_pair = model_to_dict(result)
        return json.dumps(key_pair)
    else:
        return json.dumps({"fail":"获取密钥对失败"})