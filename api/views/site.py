from flask import Blueprint,g,request,current_app
from api.models.db import db
from api.models.user import User
from api.models.site import Site
from api.common.util import handle_input
from api.models.transform import model_to_dict
from sqlalchemy.exc import IntegrityError,InvalidRequestError
import json

site = Blueprint('site',__name__, url_prefix='/admin/site',template_folder='templates')

@site.route('/add',methods={'GET','POST'})
def add():
    error = None
    if request.method == 'POST':
        data = json.loads(request.get_data().decode('utf-8'))
        current_app.logger.debug('site.add收到的数据:{}'.format(data))
        site = data.get('site')
        env = site.get('env')
        name = site.get('name')
        link = site.get('link')
        account = site.get('account')
        password = site.get('password')
        description = site.get('description')
        try:
            site = Site(env=env,name=name,link=link,account=account,password=password,description=description)
            db.session.add(site)
            db.session.commit()
            return json.dumps({"ok":"添加站点成功"})
        except Exception as e:
            current_app.logger.error(e)
            error = 'Exception'   
    
    return json.dumps({"fail":error})

@site.route('/detail',methods={'GET','POST'})
def detail():
    data = json.loads(request.get_data().decode('utf-8'))
    id = data.get('id')
    sql = Site.query.filter(Site.id==id)
    site = sql.first()
    return json.dumps(model_to_dict(site),indent=4)

@site.route('/update', methods=('GET', 'POST'))
def update():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('site.update收到的数据:{}'.format(data))
    site = data.get('site')
    id = site.get('id')
    name = site.get('name')
    env = site.get('env')
    link = site.get('link')
    account = site.get('account')
    password = site.get('password')
    description = site.get('description')
    try:
        Site.query.filter(Site.id==id).update({'env':env,'name':name,'link':link,'account':account,'password':password,'description':description})
        db.session.commit()
        return json.dumps({"ok":"更新站点成功"})
    except Exception as e:
        return json.dumps({"fail":"更新站点失败"})


@site.route('/delete', methods=('GET', 'POST'))
def delete():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('site.delete收到的数据:{}'.format(data))
    id = handle_input(data.get('id'))
    try:
        site = Site.query.filter(Site.id==id).first()
        db.session.delete(site)
        db.session.commit()
        return json.dumps({"ok":"删除站点成功"})
    except Exception as e:
        return json.dumps({"fail":"删除站点失败","error":e})


@site.route('/list',methods={'GET','POST'})
def list():
    sql = db.session.query(Site).order_by(Site.create_time.desc())
    site_list = []
    results = sql.all()  
    if len(results) >0:
        for result in results:
            site_list.append(model_to_dict(result))
    return json.dumps(site_list)

@site.route('/get_site_by_env_name',methods={'GET','POST'})
def get_site_by_env_name():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug('site.update收到的数据:{}'.format(data))
    env = data.get('env')
    name = data.get('name')
    results = db.session.query(Site).filter(Site.env == env,Site.name == name).all()
    if len(results) >0:
        return json.dumps({'msg':True})
    return json.dumps({'msg':False})


@site.route('/select', methods={'GET', 'POST'})
def siteSelect():
    data = json.loads(request.get_data().decode('utf-8'))
    current_app.logger.debug("site select接收到的数据{}".format(data))
    selectContent = data.get('selectContent')
    condition = "%{}%".format(selectContent)
    sql = db.session.query(Site).filter((Site.name + Site.link).like(condition)).order_by(Site.create_time.desc())
    print(sql)
    site_list = []
    results = sql.all()
    if len(results) > 0:
        for site in results:
            site_list.append(model_to_dict(site))
    print(site_list)
    return json.dumps(site_list)
    