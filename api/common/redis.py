from redis import Redis
from flask import current_app

# https://www.cnblogs.com/bainbian1234/p/9884179.html

class MyRedis(object):
    def __init__(self,db=0):
        try:
            # 获取redis信息
            host = current_app.config.get('REDIS_HOST')
            port = current_app.config.get('REDIS_PORT')
            password = current_app.config.get('REDIS_PASSWORD')
            self.redis = Redis(host=host,port=port,password=password,db=db)
        except Exception as e:
            current_app.logger.error('连接不上redis:'+str(e))
        
    def get_key(self,key):
        result = self.redis.get(key)
        if result:
            return result.decode()
        return None
    
    def set_key(self,key,value,time=None):
        try:
            result = self.redis.set(key,value,time)
            return True
        except Exception as e:
            current_app.logger.error("redis保存key失败:"+str(e))
        
        return False

    def delete_key(self,key):
        tag = self.redis.exists(key)
        
        if tag:
            self.redis.delete(key)
            return True
        else:
            current_app.logger.info('{}不存在'.format(key))
        
        return False
            
            
        
        
    