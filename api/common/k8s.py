
from flask import request,current_app,make_response
from kubernetes import client,config
from api.common.redis import MyRedis
from api.common.util import my_decode,SingletonDBPool
from api.models.k8s import Cluster
import os

def set_config(cluster_name):
    current_app.logger.info("收到集群名:" + cluster_name)
    redis_cli = MyRedis()
    file_key = "{}_config_file".format(cluster_name)
    cache_name = redis_cli.get_key('cluster_name')
    if cluster_name == cache_name:
        # current_app.logger.info("此集群配置已缓存")
        config_file = redis_cli.get_key(file_key)
        if not set_k8s_config(config_file):
            return error_response("读取redis设置k8s集群配置失败",5000)
    else:
        cluster_config = get_cluster_config(cluster_name)
        if cluster_config and set_k8s_config(cluster_config):
            redis_cli.set_key('cluster_name', cluster_name, time=3 * 60 * 60)
            file_key = "{}_config_file".format(cluster_name)
            redis_cli.set_key(file_key, cluster_config)
        else:
            return error_response("读取MySQL设置k8s集群配置失败",5000)
    return True,"设置成功"

def set_k8s_config(cluster_config):
    try:
        cluster_config = my_decode(cluster_config)
        tmp_filename = "kubeconfig"
        with open(tmp_filename, 'w+', encoding='UTF-8') as file:
            file.write(cluster_config)
        config.load_kube_config(config_file=tmp_filename)
        os.remove(tmp_filename)
        return True
    except Exception as e:
        current_app.logger.debug("集群配置文件解码失败:"+str(e))
    return False

def error_response(msg,code):
    current_app.logger.error(msg)
    return False,msg
    # return make_response(json.dumps({"fail":msg}), code)


def get_cluster_config(cluster_name):
    cluster_config = None
    pool = SingletonDBPool()
    session = pool.connect()
    result = session.query(Cluster.cluster_config).filter(Cluster.cluster_name==cluster_name).first()
    if result:
        return result.cluster_config
    else:
        return None