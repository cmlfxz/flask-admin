import json

import sys,re
import subprocess
import platform
import re
import socket

try:
    import commands
except Exception as e:
    print("import commands fail")
    

def ipToInt(ip):
    ip_items = ip.split('.')
    ip_int = 0
    for item in ip_items:
        ip_int = ip_int * 256 + int(item)
    return ip_int

def intToIp(ip_int):
    ip_items = ['0','0','0','0']
    for i in range(0,4):
        ip_items[3-i] = str(ip_int % 256)
        ip_int = int((int(ip_int) - int(ip_items[3-i])) / 256) 
    seq = '.'
    ip = seq.join(ip_items)
    return ip

#将掩码转成10进制
def exchange_maskint(mask_int):
    bin_arr = ['0' for i in range(32)]
    for i in range(mask_int):
        bin_arr[i] = '1'
    tmpmask = [''.join(bin_arr[i * 8:i * 8 + 8]) for i in range(4)]
    tmpmask = [str(int(tmpstr, 2)) for tmpstr in tmpmask]
    return '.'.join(tmpmask)

def get_ip_segment(ip,mask):
    sm = exchange_maskint(mask)
    ip_int = ipToInt(ip)
    sm_int = ipToInt(sm)
    total = 256 ** 4
    subnet_int = total - sm_int
    net_int = int(ip_int / subnet_int) * subnet_int
    neibor_ip = []
    for i in range(1,subnet_int - 1):
        neibor_int = net_int + i
        neibor_ip.append(intToIp(neibor_int))
    return neibor_ip

#检测端口开放
def portOpen(ip,port):
    sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sk.settimeout(1) #设置超时时间
    try:
      sk.connect((ip,port))
      return True
    except Exception:
      pass
    sk.close()   
    return False

#检测服务器是否可ping通
def alive(ip):
    pyVersion = platform.python_version()
    pyMajorVersion = pyVersion.split('.')[0]
    plat = sys.platform
    if re.match("win",plat):
        cmd="ping -n 1 -w 3  %s" % (ip)
    elif re.match("linux",plat):
        cmd="ping -c 1 -w 3  %s" % (ip)
    else:
        print("平台非win 非linux  byebye")
        return ''
    if (pyMajorVersion == '2'  and re.match("linux",plat)):
        (status,output)=commands.getstatusoutput(cmd)
    else:
        (status,output)=subprocess.getstatusoutput(cmd)
    if status == 0:
        return True

    return False
