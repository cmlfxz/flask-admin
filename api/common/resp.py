import json
from flask import make_response
# 直接返回给web前端
# make_response(render_template('not_found.html'), 404)
def ok_response(msg):
    return make_response(json.dumps({"code": 200, "msg": msg}), 200)

def success_response(data):
    return make_response(json.dumps({"code":200,"data":data}),200)

def fail_response(msg):
    return make_response(json.dumps({"code":500,"msg":msg}),500)

def param_error_response():
    return make_response(json.dumps({"code":400,"msg":"参数错误"}),400)

def unauthorized_response():
    return make_response(json.dumps({"code": 401, "msg": "接口权限验证失败"}), 401)

def not_acceptable_response():
    return make_response(json.dumps({"code":406,"msg":"请求参数不接受"}),406)

def not_found_response():
    return make_response(json.dumps({"code":404,"msg":"接口不存在"}),404)

def conflict_response():
    return make_response(json.dumps({"code":409,"msg":"冲突"}),409)