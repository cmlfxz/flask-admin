from flask import current_app,jsonify
import os,json,re
from datetime import date, datetime
import decimal
import pymysql 
import base64
import threading
import pytz
from DBUtils.PooledDB import PooledDB
import requests
import boto
import boto.s3.connection
from .host import alive,portOpen
import nacos
import yaml
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import configparser

dir_path = os.path.dirname(os.path.abspath(__file__))


def error_log(msg):
    current_app.logger.error(msg)

def error_response(msg):
    return json.dumps({"fail":msg})

def success_response(msg):
    return json.dumps({"ok":msg})

# 处理接收的json数据，如果前端传的不是整形数据，进一步转化需要再调用str_to_int()
def handle_input(obj):
    if obj == None:
        return None
    elif isinstance(obj,str):
        return (obj.strip())
    elif isinstance(obj,int):
        return obj
    else:
        print("未处理类型{}".format(type(obj)))
        return(obj.strip())

#['a','b','c'] => 'a','b','c'
def list_to_string(a):
    if not isinstance(a,list):
        return None
    if len(a) == 0:
        return None
    if isinstance(a[0],str):
        return ",".join(a)
    elif isinstance(a[0],int):
        return ",".join(map(str,a))
    else:
        print("列表数据类型无法处理"+type(a[0]))
        return None

# 'a,b,c,d'=> ['a','b','c','d']
def string_to_list(a):
    if not a:
        return []
    a_to_list = a.split(',')
    if len(a_to_list) == 0:
        return []
    try:
        int(a_to_list[0])
        return list(map(int,a.split(',')))
    except ValueError :
        return list(filter(None,a.split(',')))


def my_encode(a):
    return base64.b64encode(a.encode('utf-8')) 

def my_decode(a):
    return base64.b64decode(a).decode("utf-8")

def str_to_int(str):    
    # return str=="" ? 1 : int(str)  
    return 1 if str=="" else int(str)

def str_to_float(str):    
    return 1 if str=="" else float(str)

def format_float(num):
    return  float("%.2f" % num)


#参数是datetime
def time_to_string(dt):
    tz_sh = pytz.timezone('Asia/Shanghai')
    return  dt.astimezone(tz_sh).strftime("%Y-%m-%d %H:%M:%S")

def utc_to_local(utc_time_str, utc_format='%Y-%m-%dT%H:%M:%S.%fZ'):
    local_tz = pytz.timezone('Asia/Shanghai')
    local_format = "%Y-%m-%d %H:%M:%S"
    utc_dt = datetime.strptime(utc_time_str, utc_format)
    local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(local_tz)
    time_str = local_dt.strftime(local_format)
    return time_str

def get_config(file,group,config_name):
    config = configparser.ConfigParser()
    conf_file = os.path.join(dir_path, "..","..","config","ini",file)
    config.read(conf_file)
    config_value=config.get(group,config_name).strip(' ').strip('\'').strip('\"')
    return config_value


class SingletonDBPool(object):
    _instance_lock = threading.Lock()

    def __init__(self):
        file = "dev.ini"
        try:
            # 本地环境获取ENV会抛异常
            env = os.environ["ENV"]
            if re.match(r'test', env, re.M | re.I):
                file = "test.ini"
            elif re.match(r'prod', env, re.M | re.I):
                file = "prod.ini"
            print("即将加载的配置是:{}".format(file))
        except Exception:
            pass
        dialect = get_config(file,'mysql','dialect')
        mysql_driver = get_config(file,'mysql','driver')
        mysql_host = get_config(file,'mysql','host')
        mysql_port = int(get_config(file,'mysql','port'))
        mysql_username = get_config(file,'mysql','username')
        mysql_password =get_config(file,'mysql','password')
        mysql_database = get_config(file,'mysql','database')

        # print(dialect,mysql_driver,mysql_host,mysql_port,mysql_username,mysql_password,mysql_database)
        db_uri = "{}+{}://{}:{}@{}:{}/{}?charset=utf8".format(
            dialect, mysql_driver, mysql_username, mysql_password, mysql_host, mysql_port, mysql_database
        )

        self.engine = create_engine(db_uri, echo=False, pool_size=8, pool_recycle=60 * 30)

    def __new__(cls, *args, **kwargs):
        if not hasattr(SingletonDBPool, "_instance"):
            with SingletonDBPool._instance_lock:
                if not hasattr(SingletonDBPool, "_instance"):
                    SingletonDBPool._instance = object.__new__(cls, *args, **kwargs)
        return SingletonDBPool._instance

    def connect(self):
        # 创建session
        DbSession = sessionmaker(bind=self.engine)
        session = DbSession()
        return session

class BucketPool(object):
    _instance_lock = threading.Lock()

    def __init__(self):
        print("单例bucket连接初始化")
        host = current_app.config.get('BUCKET_ADDR')
        port = int(current_app.config.get('BUCKET_PORT') )
        # print(host,port)
        access_key = current_app.config.get('BUCKET_ACCESS_KEY')
        secret_key = current_app.config.get('BUCKET_SECRET_KEY')
        
        self.host = host
        self.port = port
        self.access_key = access_key
        self.secret_key = secret_key

        print(host,port,access_key,secret_key)

    def __new__(cls, *args, **kwargs):
        if not hasattr(BucketPool, "_instance"):
            with BucketPool._instance_lock:
                if not hasattr(BucketPool, "_instance"):
                    BucketPool._instance = object.__new__(cls, *args, **kwargs)
        return BucketPool._instance
    def connect(self):
        # 检查ceph
        if not alive(self.host) and not portOpen(self.host,self.port):
            print("ceph服务器没开或者已宕机")
            return None
        else:
            return boto.connect_s3(
                aws_access_key_id = self.access_key,
                aws_secret_access_key = self.secret_key,
                host = self.host,
                is_secure=False,               # uncomment if you are not using ssl
                calling_format = boto.s3.connection.OrdinaryCallingFormat(),
            )

        

def http_request(url=None,headers=None,data=None,timeout=None,method=None):
    print("url",url,"data:",data)
    res = None
    try:
        if method=='post':
            res = requests.post(url,data=data,headers=headers,timeout=timeout)
        else:
            res = requests.get(url,data=data,headers=headers,timeout=timeout)
    except Exception as e:
        print(e)
    if res and res.status_code == 200:
        return res.json()
    else:
        if res is not None and res.status_code:
            status = res.status_code  
            res = res.json()
        else: 
            status = 500
        return  {'status':status,'后台返回': res}

def http_request_v2(url=None,headers=None,data=None,timeout=None,method=None):
    print("url",url,"data:",data)
    res = None
    try:
        if method=='post':
            res = requests.post(url,data=data,headers=headers,timeout=timeout)
        elif method=='delete':
            res = requests.delete(url,data=data,headers=headers,timeout=timeout)
        else:
            res = requests.get(url,data=data,headers=headers,timeout=timeout)
    except Exception as e:
        current_app.logger.error(e)
    if res and res.status_code == 200:
        return 200,res.text
    else:
        if res is not None and res.status_code:
            status = res.status_code
        else: 
            status = 500
        return  status, res

def get_exception(e):
    if isinstance(e.body, dict):
        body = json.loads(e.body)
        message = body['message']
    else:
        message = e.body
    msg = {"status": e.status, "reason": e.reason, "message": message}
    print(msg)
    return msg


# flow迁移过来
def to_json(res):
    try:
        return res.json()
    except:
        return res.text

def http_client(url=None,data=None,headers=None,timeout=None,method=None):
    res = None
    if not isinstance(data, str):
        try:
            data = json.dumps(data)
        except:
            return 500,{"fail":"data数据无法转换成json字符串"}
    if  not timeout:
        timeout = 5
    try:
        if method=='post':
            res = requests.post(url,data=data,headers=headers,timeout=timeout)
        elif method=='delete':
            res = requests.delete(url,data=data,headers=headers,timeout=timeout)
        else:
            res = requests.get(url,data=data,headers=headers,timeout=timeout)
    except Exception as e:
        current_app.logger.error(e)
    if res and res.status_code == 200:
        return 200,to_json(res)
    else:
        if res is not None and res.status_code:
            status = res.status_code
            res = to_json(res)
        else:
            status = 500
        return status, {'fail': 'Sorry, service are currently unavailable.','response':res}