from flask import current_app
import requests
import json
# try:
#    import cookiejar
# except:
#    import http.cookiejar as cookielib

import ssl
from pprint import pprint
context = ssl._create_unverified_context()
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# salt_api = "https://192.168.11.201:8000/"

class SaltApi(object):
    """
    定义salt api接口的类
    初始化获得token
    """
    def __init__(self):
        self.url = current_app.config.get('SALT_URL',None)
        self.username = current_app.config.get('SALT_USER',None)
        self.password = current_app.config.get('SALT_PASSWORD',None)
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
            "Content-type": "application/json"
            # "Content-type": "application/x-yaml"
        }
        self.params = {'client': 'local', 'fun': '', 'tgt': ''}
        self.login_url = self.url + "login"
        self.login_params = {
            'username': self.username,
            'password': self.password,
            'eauth': 'pam',
        }
        result,err = self.get_data(self.login_url, self.login_params)
        if err == "":
            self.token = result['token']
        else:
            self.token = None
        self.headers['X-Auth-Token'] = self.token

    # 返回result,err
    def get_data(self, url, params):
        send_data = json.dumps(params)
        try:
            request = requests.post(url, data=send_data, headers=self.headers, verify=False)
        except requests.exceptions.ConnectionError:
            return  None,"获取数据失败,链接不上salt-api"
        except Exception as e:
            print(e)
            return None, "未知异常"
        response = request.json()
        result = dict(response)
        r = result['return'][0]
        # print("结果:")
        # pprint(result)
        if not isinstance(r,dict):
            print("get_data 返回非字典类型数据: ",result)
            return None,"get_data返回非字典类型数据"
        if isinstance(r,dict)and r.get('data',None):
            print("获取结果:")
            pprint(result)
        return r,""
    def salt_command(self, tgt, method, arg=None):
        """远程执行命令，相当于salt 'client1' cmd.run 'free -m'"""
        if arg:
            params = {'client': 'local', 'tgt': tgt, 'fun': method, 'arg': arg}
            print('命令参数: ', params['tgt'], params['fun'], params['arg'])
        else:
            params = {'client': 'local', 'tgt': tgt, 'fun': method}
            print('命令参数: ', params['tgt'], params['fun'])

        result = self.get_data(self.url, params)
        return result

    # 远程异步执行命令
    def salt_async_command(self, target, target_type=None,method=None, arg=None):  # 异步执行salt命令，根据jid查看执行结果
        # if arg:
        #     params = {
        #         'client': 'local_async',
        #         'fun': method,
        #         'tgt': target,
        #         'arg': arg
        #     }
        #     print('命令参数: ', params['tgt'], params['fun'], params['arg'])
        # else:
        #     params = {
        #         'client': 'local_async',
        #         'fun': method,
        #         'tgt': target,
        #         "tgt_type":"list",
        #     }
        #     print('命令参数: ', params['tgt'], params['fun'])
        params = {
            'client': 'local_async',
            'fun': method,
            'tgt': target,
            'arg': arg,
            'tgt_type':target_type,
        }
        print('命令参数: ', params['tgt'], params['fun'], params['arg'])
        # 这里不一定有jid bug
        result,err = self.get_data(self.url, params)
        print("结果: ",result,"error: ",err)
        if err != "":
            return None,err
        jid = result.get('jid',None)
        return jid,""


    def get_jid_ret(self, jid):  # 根据异步执行命令返回的jid查看事件结果
        params = {
            'client': 'runner',
            'fun': 'jobs.lookup_jid',
            'jid': jid
        }
        # print (params)
        result, err = self.get_data(self.url, params)
        return result

    def get_running_jobs(self):
        params = {'client': 'runner', 'fun': 'jobs.active'}
        result = self.get_data(self.url, params)
        print("job running:", result)
        return result

    # 返回数据结果和错误
    def all_key(self):
        params = {
            'client': 'wheel',
            'fun': 'key.list_all'
        }
        result,err = self.get_data(self.url, params)
        if err!="":
            return None,err
        data = result['data']
        # pprint(data)
        if data['success'] == True:
            r = data['return']
            keys = {
                "accepted": r['minions'],
                "unaccepted": r['minions_pre'],
                "denied": r['minions_denied'],
                "rejected": r['minions_rejected'],
            }
            return keys,""
        return None,"获取数据失败"

    def accept_key(self, target):
        '''
        如果你想认证某个主机 那么调用此方法
        '''
        params = {
            'client': 'wheel',
            'fun': 'key.accept',
            'match': target
        }
        result, err = self.get_data(self.url, params)
        if err != "":
            return None,err
        ret = result['data']['success']
        print(target, 'key accept:', ret)
        return ret,""

    def delete_key(self, target):
        params = {
            'client': 'wheel',
            'fun': 'key.delete',
            'match': target
        }
        result, err =  self.get_data(self.url, params)
        if err != "":
            return None,err
        ret = result['data']['success']
        print(target, 'key delete:', ret)
        return ret, ""


# def sync():
#     print('同步执行命令')
#     salt = SaltApi(salt_api)
#     print("token:", salt.token)
#     salt_target = '*'
#     salt_test = 'test.ping'
#     salt_method = 'cmd.run'
#     salt_params = 'free -m'
#     # 下面只是为了打印结果好看点
#     result1 = salt.salt_command(tgt=salt_target, method=salt_test)
#     for k, v in result1.items():
#         print(k, v)
#
#     print("\n\n")
#
#     result2 = salt.salt_command(tgt=salt_target, method=salt_method, arg=salt_params)
#     for k, v in result2.items():
#         print(k)
#         print(v)
#         print


# def async():
#     print('异步执行命令')
#     salt1 = SaltApi(salt_api)
#     salt_target = '*'
#     # salt_test = 'test.ping'
#     # salt_test = 'disk.usage'
#     salt_test = 'status.meminfo'
#     # salt_test = 'grains.items'
#     # 下面只是为了打印结果好看点
#     jid1 = salt1.salt_async_command(tgt=salt_target, method=salt_test)
#     result1 = salt1.get_jid_ret(jid1)
#     # for k, v in result1.items():
#     #     print(k, v)
#     print("\n\n")

    # salt_method = 'cmd.run'
    # salt_params = 'df -hT'
    # jid2 = salt1.salt_async_command(tgt=salt_target, method=salt_method, arg=salt_params)
    # result2 = salt1.get_jid_ret(jid2)
    # for k, v in result2.items():
    #     print(k)
    #     print(v)


# def key():
#     salt = SaltApi(salt_api)
#     salt.all_key()

    # salt.accept_key('192.168.11.80')
    # salt.delete_key('192.168.11.80')
    # salt.all_key()

# def job():
#     salt = SaltApi(salt_api)
#     salt.get_running_jobs()


# if __name__ == '__main__':
#     # sync()
#     async()
#     # key()
#     # job()
