from api.models.cmdb import ServerInfo, ServerUpdateLog, ServerLog
from api.common.util import *
from api.common.host import *
from flask import current_app
import json
import datetime
from pytz import utc
from apscheduler.schedulers.background import  BackgroundScheduler
from apscheduler.jobstores.sqlalchemy import  SQLAlchemyJobStore
from apscheduler.executors.pool import ThreadPoolExecutor,ProcessPoolExecutor
# import gevent

def list_to_str(arrayList):
    return ",".join(arrayList)

class ServerJob():
    def __init__(self):
        executors = {
            'default': ThreadPoolExecutor(2),
        }
        self.scheduler = BackgroundScheduler(executors=executors, timezone=utc)

    def add(self,func,args,kwargs):
        print(datetime.datetime.now() + datetime.timedelta(seconds=2))
        print(datetime.datetime.utcnow() + datetime.timedelta(seconds=5))

        self.scheduler.add_job(func=func, args=args,kwargs=kwargs, trigger='date',
                          run_date=datetime.datetime.utcnow() + datetime.timedelta(seconds=2))

    def start(self):
        self.scheduler.start()

class BactchOperateServer():
    def __init__(self,hosts=None):
        # 获取操作主机列表
        if hosts != None:
            self.hosts = hosts
        else:
            self.hosts = self.get_hosts()
        self.ansible_base_url = self.get_ansible_url()
        self.batch = get_update_batch()

    def get_hosts(self):
        return get_host_list()

    def get_ansible_url(self):
        return current_app.config.get('FLASK_ANSIBLE_URL')

    def get_log_batch(self):
        return get_update_batch()

    # 批量推送账号
    def push_user(self,server_user):
        push_url =  self.ansible_base_url+'/serverInfo/push_user'
        account  = server_user.get("account")
        save_update_log(self.batch, "批量更新账号，账号:{},主机列表总共{}条,分别是：{}".format(account, len(self.hosts),list_to_str(self.hosts)))
        try:
            data = json.dumps({
                "host_list": self.hosts,
                "server_user": server_user,
            })
            serverJob = ServerJob()
            serverJob.add(async_push_user,args=None,kwargs={'url':push_url,'data':data,'batch':self.batch})
            serverJob.start()
            return True
        except Exception as e:
            current_app.logger.error("push_user异常: {}".format(str(e)))
            return False
    # 批量删除账号
    def delete_user(self,account):
        delete_url = self.ansible_base_url +  '/serverInfo/delete_user'
        save_update_log(self.batch, "删除账号，账号：{} 主机列表总共{}条,分别是：{}".format(account, len(self.hosts),list_to_str(self.hosts)))
        try:
            data = json.dumps({
                "host_list": self.hosts,
                "account": account,
            })
            serverJob = ServerJob()
            serverJob.add(async_delete_user, args=None, kwargs={'url': delete_url, 'data': data, 'batch': self.batch})
            serverJob.start()
            return True
        except Exception as e:
            current_app.logger.error("async_delete_user请求ansible出错，异常：{}".format(str(e)))
            return False

    # 批量更新
    def update_sever(self):
        update_url = self.ansible_base_url + '/serverInfo/get_server_info'
        save_update_log(self.batch, "更新主机列表总共{}条,分别是：{}".format(len(self.hosts),list_to_str(self.hosts)))
        try:
            data = json.dumps({
                "host_list": self.hosts,
            })
            serverJob = ServerJob()
            serverJob.add(async_update_server, args=None, kwargs={'url': update_url, 'data': data, 'batch': self.batch})
            serverJob.start()
            return True
        except Exception as e:
            current_app.logger.error("async_update_user请求ansible出错，异常：{}".format(str(e)))
            return False

# 保存服务器基本信息
def save_host_info(ip):
    pool = SingletonDBPool()
    session = pool.connect()
    result = session.query(ServerInfo).filter(ServerInfo.private_ip == ip).first()
    if not result:
        now = datetime.datetime.now()
        server = ServerInfo(private_ip=ip, lock_status=0, expire_date=now, create_time=now)
        session.add(server)
        session.commit()

#保存初始化日志
def save_init_log(msg):
    pool = SingletonDBPool()
    session = pool.connect()
    now = datetime.datetime.now()
    log = ServerLog(msg=msg, create_time=now)
    session.add(log)
    session.commit()

# 保存更新日志
def save_update_log(batch, msg):
    try:
        pool = SingletonDBPool()
        session = pool.connect()
        log = ServerUpdateLog(batch=batch, msg=msg, create_time=datetime.datetime.now())
        session.add(log)
        session.commit()
    except Exception as e:
        print("更新异常:", e)

# 服务器初始化 通过ping 和检测 ssh端口 初始化服务器列表
def checkip(ip,port):
    # if alive(ip) or portOpen(ip, port):
    if alive(ip):
        msg = ip + "可达"
        save_init_log(msg)
        save_host_info(ip)
    else:
        msg = ip + "不可达"
        save_init_log(msg)

# 获取更新批次
def get_update_batch():
    pool = SingletonDBPool()
    session = pool.connect()
    sql = session.query(ServerUpdateLog.batch) \
        .order_by(ServerUpdateLog.batch.desc())
    if sql.first():
        batch_id = sql.first().batch
        return batch_id + 1
    else:
        return 0

# {'url':push_url,'data':data,'batch':self.batch}
def async_push_user(**kwargs):
    try:
        url = kwargs['url']
        data = kwargs['data']
        batch= kwargs['batch']
        result = http_request(url=url, data=data, method='post')
        success_hosts= result.get("success_host_list")
        fail_hosts = result.get("fail_host_list")
        unreachable_hosts = result.get("unreachable_host_list")
        save_update_log(batch, "更新账号成功共{}条，分别是:{}".format(len(success_hosts), list_to_str(success_hosts)))
        save_update_log(batch, "更新账号失败共{}条，分别是:{}".format(len(fail_hosts), list_to_str(fail_hosts)))
        save_update_log(batch, "更新账号不可达共{}条，分别是:{}".format(len(unreachable_hosts), list_to_str(unreachable_hosts)))
    except Exception:
        save_update_log(batch, "更新账号调用ansible失败")

def async_delete_user(**kwargs):
    try:
        url = kwargs['url']
        data = kwargs['data']
        batch= kwargs['batch']
        result = http_request(url=url, data=data, method='post')
        success_hosts= result.get("success_host_list")
        fail_hosts = result.get("fail_host_list")
        unreachable_hosts = result.get("unreachable_host_list")

        save_update_log(batch, "删除账号成功共{}条，分别是:{}".format(len(success_hosts), list_to_str(success_hosts)))
        save_update_log(batch, "删除账号失败共{}条，分别是:{}".format(len(fail_hosts), list_to_str(fail_hosts)))
        save_update_log(batch, "删除账号不可达共{}条，分别是:{}".format(len(unreachable_hosts), list_to_str(unreachable_hosts)))
    except Exception:
        save_update_log(batch, "删除账号调用ansible失败")

def get_host_list():
    hosts = []
    pool = SingletonDBPool()
    session = pool.connect()
    sql = session.query(ServerInfo).filter(ServerInfo.lock_status != 1)
    results = sql.all()
    if len(results) > 0:
        for server in results:
            host = server.private_ip
            hosts.append(host)
    # print(hosts)
    return hosts

def update_host_status(host,status):
    pool = SingletonDBPool()
    session = pool.connect()
    sql = session.query(ServerInfo).filter(ServerInfo.private_ip == host)
    sql.update({
        'status': status,
    })
    session.commit()

def update_host_info(info):
    pool = SingletonDBPool()
    session = pool.connect()

    # architecture = info.get('architecture')
    cpu_core = info.get('cpu_core')
    cpu_number = info.get('cpu_number')
    cpu_load = info.get('cpu_load')
    cpu_thread = info.get('cpu_thread')
    device_info = info.get('device_info')
    disk_info = info.get('disk_info')
    disk_mount_list = info.get('disk_mount_list')
    hostname = info.get('hostname')
    kernel_version = info.get('kernel_version')
    memory = info.get('memory')
    system_version = info.get('system_version')
    private_ip = info.get('private_ip')
    system = info.get('system')

    cpu = {
        "cpu_core": cpu_core,
        "cpu_number": cpu_number,
        "cpu_thread": cpu_thread
    }
    system_info = {
        "system": system,
        "system_version": system_version,
        "kernel_version": kernel_version
    }
    host = private_ip
    try:
        sql = session.query(ServerInfo).filter(ServerInfo.private_ip == host)
        # 服务器已存在数据库，则更新，否则添加
        if sql.first():
            print("查有{}".format(host))
            info = {
                'hostname': hostname,
                'system_info': json.dumps(system_info),
                'cpu': json.dumps(cpu),
                'cpu_load': json.dumps(cpu_load),
                'memory': json.dumps(memory),
                'device_info': json.dumps(device_info),
                'disk_info': json.dumps(disk_info),
                'disk_mount_list': json.dumps(disk_mount_list),
                'update_time': datetime.datetime.now(),
                'lock_status': 0,
                'status': '在线',
            }
            sql.update(info)
            session.commit()
            return True,host
        else:
            print("查无{}".format(host))
            info = ServerInfo(hostname=hostname,
                private_ip=private_ip,
                system_info=json.dumps(system_info),
                cpu=json.dumps(cpu),
                cpu_load=json.dumps(cpu_load),
                memory=json.dumps(memory),
                device_info=json.dumps(device_info),
                disk_info=json.dumps(disk_info),
                disk_mount_list=json.dumps(disk_mount_list),
                lock_status=0,
                status='在线')
            session.add(info)
            session.commit()
            return True,host
    except Exception as e:
        print("sql异常：", e)
    return False,host

def async_update_server(**kwargs):
    url = kwargs['url']
    data = kwargs['data']
    batch = kwargs['batch']
    pool = SingletonDBPool()
    session = pool.connect()
    try:
        result = http_request(url=url, data=data, timeout=600, method='post')
        serverInfoList = result.get("serverInfoList")

        fail_hosts = result.get("fail_host_list")
        unreachable_hosts = result.get("unreachable_host_list")
        save_update_log(batch, "获取失败共{}条，分别是:{}".format(len(fail_hosts), list_to_str(fail_hosts)))
        save_update_log(batch, "不可达共{}条，分别是:{}".format(len(unreachable_hosts), list_to_str(unreachable_hosts)))

        if len(fail_hosts) > 0:
            for host in fail_hosts:
                update_host_status(host,'忙碌')

        if len(unreachable_hosts) > 0:
            for host in unreachable_hosts:
                update_host_status(host, '关机')

        if len(serverInfoList) <= 0:
            save_update_log(batch, "获取主机数据成功为0条")
        else:
            success_hosts = []
            update_db_fail_hosts = []
            for serverInfo in serverInfoList:
                ok,host = update_host_info(serverInfo)
                if ok:
                    success_hosts.append(host)
                else:
                    update_db_fail_hosts.append(host)
            save_update_log(batch, "更新成功共{}条，分别是:{}".format(len(success_hosts), list_to_str(success_hosts)))
            save_update_log(batch, "更新失败共{}条，分别是:{}".format(len(update_db_fail_hosts), list_to_str(update_db_fail_hosts)))
        return True
    except Exception as e:
        print("出现异常11:", e)
        save_update_log(batch, "调用ansible更新主机异常：{}".format(str(e)))
    return False