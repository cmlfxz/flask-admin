from flask import Flask,jsonify,request,current_app
import json,requests
from api.common.util import *
from kubernetes import client,config
from kubernetes.client.rest import ApiException
import yaml

istio_group = 'networking.istio.io'
istio_version = 'v1alpha3'
vs_plural = 'virtualservices'
dr_plural = 'destinationrules'
gw_plural = 'gateway'


class MyGateWay(object):
    def __init__(self,namespace,name):
        self.namespace = namespace
        self.name = name

    def add_or_update(self,body):
        try:
            myclient = client.CustomObjectsApi()
            if not self.exists():
                myclient.create_namespaced_custom_object(group=istio_group,version=istio_version,
                                                                  plural=gw_plural, namespace=self.namespace,
                                                                  body=body)
            else:
                myclient.patch_namespaced_custom_object(group=istio_group,version=istio_version,
                                                        plural=gw_plural, namespace=self.namespace,
                                                        name=self.name, body=body)
            return True
        except Exception as e:
            print(e)
            return False

    def exists(self):
        field_selector = "metadata.name={}".format(self.name)
        results = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=gw_plural, namespace=self.namespace,
                                          field_selector=field_selector)
        if len(results['items']) > 0:
            return True
        else:
            return False

    def delete(self):
        try:
            myclient = client.CustomObjectsApi()
            myclient.delete_namespaced_custom_object(group=istio_group,version=istio_version,
                                                              plural=gw_plural,namespace=self.namespace,name=self.name,
                                                              body=client.V1DeleteOptions(propagation_policy='Foreground',
                                                              grace_period_seconds=5))
        except Exception as e:
            exp = get_exception(e)
            current_app.logger.debug("删除vs异常:{}".format(exp))
            return False
        return True

    def select(self):
        results = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=gw_plural,namespace=self.namespace)
        gw = None
        for item in results['items']:
            if item['metadata']['name'] == self.name:
                gw = item
                break
        return gw

class MyVirtualService(object):
    def __init__(self,namespace,name):
        self.namespace = namespace
        self.name = name

    def add_or_update(self,body):
        try:
            myclient = client.CustomObjectsApi()
            if not self.exists():
                myclient.create_namespaced_custom_object(group=istio_group,version=istio_version,
                                                                  plural=vs_plural, namespace=self.namespace,
                                                                  body=body)
            else:
                myclient.patch_namespaced_custom_object(group=istio_group,version=istio_version,
                                                        plural=vs_plural, namespace=self.namespace,
                                                        name=self.name, body=body)
            return True
        except Exception as e:
            print(e)
            return False

    def exists(self):
        field_selector = "metadata.name={}".format(self.name)
        results = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=vs_plural, namespace=self.namespace,
                                          field_selector=field_selector)
        if len(results['items']) > 0:
            return True
        else:
            return False

    def delete(self):
        try:
            myclient = client.CustomObjectsApi()
            result = myclient.delete_namespaced_custom_object(group=istio_group,version=istio_version,
                                                              plural=vs_plural,namespace=self.namespace,name=self.name,
                                                              body=client.V1DeleteOptions(propagation_policy='Foreground',
                                                              grace_period_seconds=5))
        except Exception as e:
            exp = get_exception(e)
            current_app.logger.debug("删除vs异常:{}".format(exp))
            return False
        return True

    def select(self):
        results = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=vs_plural,namespace=self.namespace)
        vs = None
        for item in results['items']:
            if item['metadata']['name'] == self.name:
                vs = item
                break
        return vs

class MyDestionationRule(object):
    def __init__(self,namespace,name):
        self.namespace = namespace
        self.name = name

    def add_or_update(self,body):
        try:
            myclient = client.CustomObjectsApi()
            if not self.exists():
                myclient.create_namespaced_custom_object(group=istio_group,version=istio_version,
                                                         plural=dr_plural, namespace=self.namespace,
                                                         body=body)
            else:
                myclient.patch_namespaced_custom_object(group=istio_group,version=istio_version,
                                                        plural=dr_plural, namespace=self.namespace,
                                                        name=self.name, body=body)
            return True
        except Exception as e:
            exp = get_exception(e)
            current_app.logger.debug("添加或者更新dr异常:{}".format(exp))
            return False

    def exists(self):
        field_selector = "metadata.name={}".format(self.name)
        results = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=dr_plural,namespace=self.namespace,
                                          field_selector=field_selector)
        if len(results['items']) > 0:
            return True
        else:
            return False

    def delete(self):
        try:
            myclient = client.CustomObjectsApi()
            result = myclient.delete_namespaced_custom_object(group=istio_group,version=istio_version,
                                                              plural=vs_plural,namespace=self.namespace,name=self.name,
                                                              body=client.V1DeleteOptions(propagation_policy='Foreground',
                                                              grace_period_seconds=5))
        except Exception as e:
            exp = get_exception(e)
            current_app.logger.debug("删除dr异常:{}".format(exp))
            return False
        return True

    def select(self):
        drs = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=vs_plural,namespace=self.namespace)
        dr = None
        for item in drs['items']:
            if item['metadata']['name'] == self.name:
                dr = item
                break
        return dr

def existsDr(namespace,name):
    field_selector = "metadata.name={}".format(name)
    results = client.CustomObjectsApi().list_namespaced_custom_object(group=istio_group,
            version=istio_version, plural=dr_plural,namespace=namespace,field_selector=field_selector)
    if len(results['items']) > 0:
        return True
    else:
        return False

def existsVs(namespace,name):
    field_selector = "metadata.name={}".format(name)
    results = client.CustomObjectsApi().list_namespaced_custom_object(group=istio_group,
            version=istio_version, plural=vs_plural,namespace=namespace,field_selector=field_selector)
    if len(results['items']) > 0:
        return True
    else:
        return False

def existsGw(namespace,name):
    field_selector = "metadata.name={}".format(name)
    results = client.CustomObjectsApi().list_namespaced_custom_object(group=istio_group,
            version=istio_version, plural=gw_plural,namespace=namespace,field_selector=field_selector)
    if len(results['items']) > 0:
        return True
    else:
        return False


def deploy_less_than_two(namespace,labels):
    myclient = client.AppsV1Api()
    results = myclient.list_namespaced_deployment(namespace=namespace,label_selector=labels)
    print(len(results.items))
    if len(results.items) < 2:
        return True
    else:
        return False

def get_dr_by_name(namespace,name):
    drs = client.CustomObjectsApi().list_namespaced_custom_object(group="networking.istio.io",
            version="v1alpha3", plural="destinationrules",namespace=namespace)
    dr = None
    for item in drs['items']:
        if item['metadata']['name'] == name:
            dr = item
            break
    return dr

def get_vs_by_name(namespace, vs_name):
    virtual_services = client.CustomObjectsApi().list_namespaced_custom_object(group="networking.istio.io",
                                                                               version="v1alpha3",
                                                                               plural="virtualservices",
                                                                               namespace=namespace)
    virtual_service = None
    for vs in virtual_services['items']:
        if vs['metadata']['name'] == vs_name:
            virtual_service = vs
            break
    return virtual_service

def get_gw_by_name(namespace, gw_name):
    gateways= client.CustomObjectsApi().list_namespaced_custom_object(group="networking.istio.io",
                                                                               version="v1alpha3",
                                                                               plural="gateway",
                                                                               namespace=namespace)
    gateway = None
    for gw in gateways['items']:
        if gw['metadata']['name'] == gw_name:
            gateway = gw
            break
    return gw

def update_virtual_service(vs_name, namespace, old_weight, new_weight):
    myclient = client.CustomObjectsApi()
    vs = get_vs_by_name(namespace, vs_name)
    if vs == None:
        return jsonify({"fail": "找不到该vs"})
    try:
        vs['spec']['http'][0]['route'][0]['weight'] = old_weight
        vs['spec']['http'][0]['route'][1]['weight'] = new_weight
        api_response = myclient.patch_namespaced_custom_object(group="networking.istio.io",
                                                               version="v1alpha3",
                                                               plural="virtualservices",
                                                               name=vs_name,
                                                               namespace=namespace,
                                                               body=vs)
        status = "{}".format(api_response['spec']['http'])
    except Exception as e:
        print(e)
        return jsonify({"fail": "可能非生产环境，没有设置流量权重"})

    return jsonify({"update_status": status})

