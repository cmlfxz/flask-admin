# from flask import current_app
from api.models.es import Es_Cluster,Index
from elasticsearch7 import Elasticsearch
# from api.models.db import db
from io import StringIO
from api.common.util import  SingletonDBPool
def error_log(msg):
    print(msg)

api_index = "prod-proxy-nginx-*"

class Elastic(object):
    def __init__(self,cluster_name):
        '''
        :param cluster_name:  'es_cluster1'
        '''
        pool= SingletonDBPool()
        session = pool.connect()
        result = session.query(Es_Cluster).filter(Es_Cluster.name == cluster_name).first()
        # print(result)
        if not result:
            error_log("数据库查无此集群配置")
            return
        cluster_addr = result.cluster_addr.split(',')
        cluster_port = int(result.cluster_port)
        account = result.account
        password = result.password
        # print(cluster_addr,cluster_port,account,password)
        try:
            if not account:
                self.es = Elasticsearch(hosts=cluster_addr,port=cluster_port)
            else:
                self.es = Elasticsearch(hosts=cluster_addr, port=cluster_port,http_auth=(account,password))
        except Exception as e:
            print(e)
    # /_cat/indices，查询所有index
    def index_list(self):
        results=[]
        index_text= self.es.cat.indices(index="*")
        f = StringIO(index_text)
        while True:
            s = f.readline()
            if s == '':
                break
            res = s.strip().split()
            if len(res) == 10:
                index = Index(health=res[0],status=res[1],index=res[2],uuid=res[3],pri=res[4],rep=res[5]
                        ,docs_count=res[6],docs_delete=res[7],store_size=res[8],pri_store_size=res[9])
                # print(index)
                results.append(index.to_json())
            else:
                print(len(res),res)
        return results

    def delete_index(self,idx):
        try:
            self.es.indices.delete(index=idx)
            return True
        except Exception as e:
            error_log("删除ES INDEX {}失败".format(idx))
            return False

    def get_index_mapping(self,idx):
        '''
        :param idx:
        :return:
        '''
        result = self.es.indices.get_mapping(index=idx)
        return result

    def search_data(self,idx,body):
        '''
        :param idx:
        :param body:
        :return:
        '''
        result = self.es.search(index=idx,body=body)
        return result

class TrafficStat():
    def __init__(self,from_time,to_time,es,index):
        self.from_time = from_time
        self.to_time = to_time
        self.es = es
        self.index = index

    def requesttime_ranks(self,values):
        body = {
            "_source": "clientip",
            "query": {
                "bool": {
                    "must": {"match_all": {}},
                    "filter": {
                        "range": {
                            "@timestamp": {
                                "gte": self.from_time,
                                "lte": self.to_time
                            }
                        }
                    }
                }
            },
            "size": 0,
            "aggs": {
                "requesttime_ranks": {
                    "percentile_ranks": {
                        "field": "requesttime",
                        "values": values
                    }
                }
            }
        }
        res = self.es.search_data(idx=self.index,body=body)

        total = res['hits']['total']['value']
        results  = res['aggregations']['requesttime_ranks']['values']

        return results

# def requesttime_ranks(from_time,to_time,values=[1.0]):
#     # cluster_name = request.headers.get('cluster_name')
#     # data = json.loads(request.get_data().decode("utf-8"))
#     # from_time = data.get('from')
#     # to_time = data.get('to')
#     body = {
#         "_source":"clientip",
#         "query":{
#             "bool":{
#                 "must":{"match_all":{}},
#                 "filter":{
#                     "range":{
#                         "@timestamp":{
#                             "gte":from_time,
#                             "lte":to_time
#                         }
#                     }
#                 }
#             }
#         },
#         "size":0,
#         "aggs":{
#             "requesttime_ranks":{
#                 "percentile_ranks":{
#                     "field": "requesttime",
#                     "values":values
#                 }
#             }
#         }
#     }
#     client = Elastic(cluster_name)
#     res = client.search_data(idx=api_index,body=body)
#
#     total = res['hits']['total']['value']
#     results  = res['aggregations']['requesttime_ranks']['values']
#
#     return results
