from flask import current_app
import threading
import minio
from api.common.util import alive,portOpen
class S3Interface(object):
    def create_bucket(self,bucket_name):
        pass
    def delete_bucket(self,bucket_name):
        pass
    def get_all_buckets(self):
        pass
    def bucket_key_list(self,bucket_name):
        pass
    def delete_bucket_key(self,bucket_name,key_name):
        pass
    def put_file_to_bucket(self,bucket_name,filename,filepath,content_type=None,expires=None):
        pass


class MinioS3(S3Interface):
    # client = None
    _instance_lock = threading.Lock()
    def __init__(self):
        self.host = current_app.config.get('MINIO_HOST')
        self.port = current_app.config.get('MINIO_PORT')
        self.addr = ddr = "{}:{}".format(self.host, self.port)
        self.access_key = current_app.config.get('MINIO_ACCESS_KEY')
        self.secret_key = current_app.config.get('MINIO_SECRET_KEY')
        self.client = self.get_client()

    def put_file_to_bucket(self,bucket_name,filename,filepath,content_type=None,expires=None):
        if not self.client.bucket_exists(bucket_name):
            self.client.make_bucket(bucket_name)
        result = self.client.fput_object(bucket_name=bucket_name,object_name=filename,file_path=filepath,content_type=content_type)
        return "http://{}/{}/{}".format(self.addr,bucket_name,filename)

    def __new__(cls, *args, **kwargs):
        if not hasattr(MinioS3, "_instance"):
            with MinioS3._instance_lock:
                if not hasattr(MinioS3, "_instance"):
                    MinioS3._instance = object.__new__(cls, *args, **kwargs)
        return MinioS3._instance

    def get_client(self):
        if not alive(self.host) and not portOpen(self.host,self.port):
            print("minio服务器没开或者已宕机")
            return None
        else:
            MINIO_CONF = {
                'endpoint': self.addr,
                'access_key': self.access_key,
                'secret_key': self.secret_key,
                'secure': False
            }
            return minio.Minio(**MINIO_CONF)

