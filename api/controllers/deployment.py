from flask import current_app
import json
from api.common.util import *
from kubernetes import client
from kubernetes.client.rest import ApiException
from pprint import  pprint

def string_to_int(string):
    if string == "" or string == 'null' or string== None:
        return None
    else:
        return int(string)

def debug_log(msg):
    current_app.logger.debug(msg)

def info_log(msg):
    current_app.logger.info(msg)

def error_log(msg):
    current_app.logger.error(msg)

def error_response(msg):
    return json.dumps({"fail":msg})

def success_response(msg):
    return json.dumps({"ok":msg})

def get_deployment(namespace, deploy_name):
    deployments = client.AppsV1Api().list_namespaced_deployment(namespace=namespace)

    deployment = None
    for deploy in deployments.items:
        if deploy.metadata.name == deploy_name:
            deployment = deploy
            break
    return deployment

class MyDeployment(object):
    def __init__(self,namespace,name):
        self.namespace = namespace
        self.name=name

    def delete(self):
        try:
            body = client.V1DeleteOptions(propagation_policy='Foreground',
                                          grace_period_seconds=2)
            client.AppsV1Api().\
                delete_namespaced_deployment(name=self.name,namespace=self.namespace,body=body)
            return True
        except ApiException as e:
            error_log(get_exception(e))
            return False

    def patch(self,deployment):
        try:
            client.AppsV1Api().patch_namespaced_deployment(
                name=self.name,
                namespace=self.namespace,
                body=deployment
            )
            return True
        except ApiException as e:
            error_log(get_exception(e))
            return False

    def replace(self,deployment):
        try:
            client.AppsV1Api().replace_namespaced_deployment(
                namespace=self.namespace,
                name=self.name,
                body=deployment
            )
            return True
        except ApiException as e:
            error_log(get_exception(e))
            return False

    def update_replicas(self,replicas):
        if not replicas:
            error_log("需要提供replicas")
            return False
        deployment = get_deployment(self.namespace, self.name)
        deployment.spec.replicas = replicas
        return self.patch(deployment)

    def update_image(self,image):
        deployment = get_deployment(self.namespace, self.name)
        deployment.spec.template.spec.containers[0].image = image
        return self.patch(deployment)
