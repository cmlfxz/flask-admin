from flask import Flask,jsonify,request,current_app
import json,requests
from api.common.util import *
from kubernetes import client,config
import yaml
from pprint import pprint
from api.controllers.deployment import  MyDeployment

istio_group = 'networking.istio.io'
istio_version = 'v1alpha3'
vs_plural = 'virtualservices'
dr_plural = 'destinationrules'
gw_plural = 'gateway'

class ReleaseResources(object):
    def __init__(self,**kwargs):
        self.project = kwargs.get('project')
        self.service = kwargs.get('service')
        self.domain = kwargs.get('domain') #new
        # self.port = kwargs.get('port')   #new
        self.service_type  = kwargs.get('service_type')  #new
        self.env = kwargs.get('env','prod')
        self.old_version = kwargs.get('old_version')
        self.old_weight = kwargs.get('old_weight')
        self.new_version = kwargs.get('new_version')
        self.new_weight = kwargs.get('new_weight')
        self.release_status = kwargs.get('release_status')
        self.namespace = "{}-{}".format(self.project,self.env)

class IstioResources(ReleaseResources):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def call_api_add_vs(self,http):
        name = "{}-vs".format(self.service)
        body = {
            "apiVersion": "networking.istio.io/v1alpha3",
            "kind": "VirtualService",
            "metadata": {
                "name": name,
                "namespace": self.namespace
            },
            "spec": {
                "hosts": [
                    self.service
                ],
                "http": http
            }
        }
        vs = MyVirtualService(self.namespace,name)
        return vs.add_or_update(body)

    def call_api_add_nb_vs(self,http):
        name = "{}-nb-vs".format(self.service)
        gateway = "{}-gateway".format(self.service)
        # print(name,gateway,self.domain)
        body = {
            "apiVersion": "networking.istio.io/v1alpha3",
            "kind": "VirtualService",
            "metadata": {
                "name": name,
                "namespace": self.namespace
            },
            "spec": {
                "hosts": [
                    self.domain
                ],
                "gateways": [
                    gateway
                ],
                "http": http
            }
        }
        vs = MyVirtualService(self.namespace,name)
        return vs.add_or_update(body)

    def show(self):
        print("IstioResources namespace:{}".format(self.namespace))

    def add_dr(self):
        name = "{}-dr".format(self.service)
        body = {
            "apiVersion": "networking.istio.io/v1alpha3",
            "kind": "DestinationRule",
            "metadata": {
                "name": name,
                "namespace": self.namespace
            },
            "spec": {
                "host": self.service,
                "subsets": [
                    {
                        "name": "old",
                        "labels": {
                            "version": self.old_version
                        }
                    },
                    {
                        "name": "new",
                        "labels": {
                            "version": self.new_version
                        }
                    }
                ]
            }
        }
        dr = MyDestionationRule(self.namespace,name)
        return dr.add_or_update(body)

    def add_vs(self):
        pass

    def add_gw(self):
        name = "{}-gateway".format(self.service)
        body ={
            "apiVersion": "networking.istio.io/v1beta1",
            "kind": "Gateway",
            "metadata": {
                "name": name,
                "namespace": self.namespace
            },
            "spec": {
                "selector": {
                    "istio": "ingressgateway"
                },
                "servers": [
                    {
                        "hosts": [
                            self.domain
                        ],
                        "port": {
                            "name": "http",
                            "number": 80,
                            "protocol": "HTTP"
                        }
                    }
                ]
            }
        }
        gw = MyGateWay(self.namespace,self.name)
        return gw.add_or_update(body)

    def switch_traffic(self):
        pass

    def delete_resources(self):
        pass

class IstioBlueGreenResources(IstioResources):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def show(self):
        print("IstioBlueGreenResources namespace:{}".format(self.namespace))

    def get_http(self):
        http = [{
            "route": [
                {
                    "destination": {
                        "host": self.service,
                        "subset": "old"
                    },
                    "weight": self.old_weight
                },
                {
                    "destination": {
                        "host": self.service,
                        "subset": "new"
                    },
                    "weight": self.new_weight
                }
            ]
        }]
        return http

    def add_vs(self):
        http = self.get_http()
        return self.call_api_add_vs(http)

    def add_nb_vs(self):
        http = self.get_http()
        return self.call_api_add_nb_vs(http)

    def update_vs(self):
        if self.service_type > 2:
            return self.add_vs()
        else:
            self.add_nb_vs()
            return self.add_vs()

    def update_nb_vs(self):
        return self.add_nb_vs()

    def delete_resources(self):
        # 先查询deployment的个数
        labels = "app={}".format(self.service)
        if deploy_less_than_two(self.namespace, labels):
            current_app.logger.error("deploy个数小于2，不允许删除")
            return False
        # 21 新版接管流量,删除旧版   22 旧版接管流量，删除新版
        deploy_name = version = None
        if self.release_status == 21:
           version =  self.old_version
        elif self.release_status == 22:
            version = self.new_version
        else:
            current_app.logger.error("发布状态不对")
            return False
        deploy_name = '{}-{}'.format(self.service, version)
        if not deploy_name:
            current_app.logger.error("要删除的部署名字不允许为空")
            return False
        else:
            myDeployment = MyDeployment(namespace=self.namespace, name=deploy_name)
            return myDeployment.delete()

class IstioGrayResources(IstioResources):
    def __init__(self,match=None,**kwargs):
        super().__init__(**kwargs)
        self.match = match

    def show(self):
        print("IstioGrayResources namespace:{}".format(self.namespace))

    def get_http(self):
        match_list = match_convert(self.match)
        pprint(match_list)
        http = [
            {
                "match": match_list,
                "route": [
                    {
                        "destination": {
                            "host": self.service,
                            "subset": "new"
                        }
                    }
                ]
            },
            {
                "route": [
                    {
                        "destination": {
                            "host": self.service,
                            "subset": "old"
                        }
                    }
                ]
            }
        ]
        return  http

    def add_vs(self):
        http = self.get_http()
        return self.call_api_add_vs(http)

    def add_nb_vs(self):
        http = self.get_http()
        return self.call_api_add_nb_vs(http)

    def take_over_traffic(self):
        subset = None
        if self.new_weight == 100 and self.old_weight == 0:
            subset = "new"
        elif self.new_weight == 0 and self.old_weight == 100:
            subset = "old"
        else:
            current_app.logger.error("接管流量的权重有误")
            return False
        http = [{
            "route": [
                {
                    "destination": {
                        "host": self.service,
                        "subset": subset
                    },
                },
            ]
        }]
        if self.service_type > 2:
            return self.call_api_add_vs(http)
        else:
            self.call_api_add_nb_vs(http)
            return self.call_api_add_vs(http)

    def update_vs(self):
        if self.service_type > 2:
            return self.add_vs()
        else:
            self.add_nb_vs()
            return self.add_vs()

    def update_nb_vs(self):
        return self.add_nb_vs()

    def delete_resources(self):
        labels = "app={}".format(self.service)
        if deploy_less_than_two(self.namespace, labels):
            current_app.logger.error("deploy个数小于2，不允许删除")
            return False
        # 21 新版接管流量,删除旧版   22 旧版接管流量，删除新版
        deploy_name = version = None
        if self.release_status == 32:
           version =  self.old_version
        elif self.release_status == 33:
            version = self.new_version
        else:
            current_app.logger.error("发布状态不对")
            return False
        deploy_name = '{}-{}'.format(self.service, version)
        if not deploy_name:
            current_app.logger.error("要删除的部署名字不允许为空")
            return False
        else:
            myDeployment = MyDeployment(namespace=self.namespace, name=deploy_name)
            return myDeployment.delete()

class MyGateWay(object):
    def __init__(self,namespace,name):
        self.namespace = namespace
        self.name = name

    def add_or_update(self,body):
        try:
            myclient = client.CustomObjectsApi()
            if not self.exists():
                myclient.create_namespaced_custom_object(group=istio_group,version=istio_version,
                                                                  plural=gw_plural, namespace=self.namespace,
                                                                  body=body)
            else:
                myclient.patch_namespaced_custom_object(group=istio_group,version=istio_version,
                                                        plural=gw_plural, namespace=self.namespace,
                                                        name=self.name, body=body)
            return True
        except Exception as e:
            print(e)
            return False

    def exists(self):
        field_selector = "metadata.name={}".format(self.name)
        results = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=gw_plural, namespace=self.namespace,
                                          field_selector=field_selector)
        if len(results['items']) > 0:
            return True
        else:
            return False

    def delete(self):
        try:
            myclient = client.CustomObjectsApi()
            result = myclient.delete_namespaced_custom_object(group=istio_group,version=istio_version,
                                                              plural=gw_plural,namespace=self.namespace,name=self.name,
                                                              body=client.V1DeleteOptions(propagation_policy='Foreground',
                                                              grace_period_seconds=5))
        except Exception as e:
            exp = get_exception(e)
            current_app.logger.debug("删除vs异常:{}".format(exp))
            return False
        return True

    def select(self):
        results = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=gw_plural,namespace=self.namespace)
        gw = None
        for item in results['items']:
            if item['metadata']['name'] == self.name:
                gw = item
                break
        return gw

class MyVirtualService(object):
    def __init__(self,namespace,name):
        self.namespace = namespace
        self.name = name

    def add_or_update(self,body):
        try:
            myclient = client.CustomObjectsApi()
            if not self.exists():
                myclient.create_namespaced_custom_object(group=istio_group,version=istio_version,
                                                                  plural=vs_plural, namespace=self.namespace,
                                                                  body=body)
            else:
                myclient.patch_namespaced_custom_object(group=istio_group,version=istio_version,
                                                        plural=vs_plural, namespace=self.namespace,
                                                        name=self.name, body=body)
            return True
        except Exception as e:
            print(e)
            return False

    def exists(self):
        field_selector = "metadata.name={}".format(self.name)
        results = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=vs_plural, namespace=self.namespace,
                                          field_selector=field_selector)
        if len(results['items']) > 0:
            return True
        else:
            return False

    def delete(self):
        try:
            myclient = client.CustomObjectsApi()
            result = myclient.delete_namespaced_custom_object(group=istio_group,version=istio_version,
                                                              plural=vs_plural,namespace=self.namespace,name=self.name,
                                                              body=client.V1DeleteOptions(propagation_policy='Foreground',
                                                              grace_period_seconds=5))
        except Exception as e:
            exp = get_exception(e)
            current_app.logger.debug("删除vs异常:{}".format(exp))
            return False
        return True

    def select(self):
        results = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=vs_plural,namespace=self.namespace)
        vs = None
        for item in results['items']:
            if item['metadata']['name'] == self.name:
                vs = item
                break
        return vs

class MyDestionationRule(object):
    def __init__(self,namespace,name):
        self.namespace = namespace
        self.name = name

    def add_or_update(self,body):
        try:
            myclient = client.CustomObjectsApi()
            if not self.exists():
                myclient.create_namespaced_custom_object(group=istio_group,version=istio_version,
                                                         plural=dr_plural, namespace=self.namespace,
                                                         body=body)
            else:
                myclient.patch_namespaced_custom_object(group=istio_group,version=istio_version,
                                                        plural=dr_plural, namespace=self.namespace,
                                                        name=self.name, body=body)
            return True
        except Exception as e:
            exp = get_exception(e)
            current_app.logger.debug("添加或者更新dr异常:{}".format(exp))
            return False

    def exists(self):
        field_selector = "metadata.name={}".format(self.name)
        results = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=dr_plural,namespace=self.namespace,
                                          field_selector=field_selector)
        if len(results['items']) > 0:
            return True
        else:
            return False

    def delete(self):
        try:
            myclient = client.CustomObjectsApi()
            result = myclient.delete_namespaced_custom_object(group=istio_group,version=istio_version,
                                                              plural=vs_plural,namespace=self.namespace,name=self.name,
                                                              body=client.V1DeleteOptions(propagation_policy='Foreground',
                                                              grace_period_seconds=5))
        except Exception as e:
            exp = get_exception(e)
            current_app.logger.debug("删除dr异常:{}".format(exp))
            return False
        return True

    def select(self):
        drs = client.CustomObjectsApi().\
            list_namespaced_custom_object(group=istio_group,version=istio_version,
                                          plural=vs_plural,namespace=self.namespace)
        dr = None
        for item in drs['items']:
            if item['metadata']['name'] == self.name:
                dr = item
                break
        return dr

def existsDr(namespace,name):
    field_selector = "metadata.name={}".format(name)
    results = client.CustomObjectsApi().list_namespaced_custom_object(group=istio_group,
            version=istio_version, plural=dr_plural,namespace=namespace,field_selector=field_selector)
    if len(results['items']) > 0:
        return True
    else:
        return False

def existsVs(namespace,name):
    field_selector = "metadata.name={}".format(name)
    results = client.CustomObjectsApi().list_namespaced_custom_object(group=istio_group,
            version=istio_version, plural=vs_plural,namespace=namespace,field_selector=field_selector)
    if len(results['items']) > 0:
        return True
    else:
        return False

def existsGw(namespace,name):
    field_selector = "metadata.name={}".format(name)
    results = client.CustomObjectsApi().list_namespaced_custom_object(group=istio_group,
            version=istio_version, plural=gw_plural,namespace=namespace,field_selector=field_selector)
    if len(results['items']) > 0:
        return True
    else:
        return False

op_system = {
    'Windows': '(Windows NT ([\\d.])+)',
    'Linux':'(Linux )',
    'Mac OS':'( Mac OS X ([\\d.])+)',
    'Android':'(Android)',
    'IOS':'(OS [\\d.]+)',
}

def agents_to_string(agents):
	i = 0
	results = None
	if len(agents) > 0:
		results = '.*'
		for agent in agents:
			agent_pattern = op_system[agent]
			results = results + agent_pattern
			if i != len(agents) - 1:
				results = results + '|'
			else:
				results = results + '.*'
			i = i + 1
	return results

def match_convert(match):
    my_matcher = {}
    headers = match[0].get("headers")

    user_agent = headers.get("user_agent")
    user_agent_value= user_agent.get("value")

    cookie = headers.get('cookie')
    cookie_type = cookie.get('type')
    cookie_value = cookie.get('value')

    custom_header   = headers.get('custom_header')
    custom_header_type= custom_header.get("type")
    custom_header_key = custom_header.get("key")
    custom_header_value = custom_header.get("value")

    uri = match[0].get("uri")
    uri_type = uri.get('type')
    uri_value = uri.get('value')

    agent_string = agents_to_string(user_agent_value)

    if  agent_string:
        if 'headers' not in my_matcher.keys():
            my_matcher['headers']={
                "User-Agent":{
                    "regex": agent_string
                }
            }
        else:
            my_matcher['headers']['User-Agent']={
                'regex':agent_string
            }
    if cookie_value:
        if 'headers' not in my_matcher.keys():
            my_matcher['headers']= {
                "cookie": {
                    cookie_type: cookie_value
                }
            }
        else:
            my_matcher['headers']['cookie']={
                cookie_type: cookie_value
            }
    if custom_header_value:
        if 'headers' not in my_matcher.keys():
            my_matcher['headers'] = {
                custom_header_key:{
                    custom_header_type: custom_header_value
                }
            }
        else:
            my_matcher['headers'][custom_header_key]={
                custom_header_type: custom_header_value
            }
    if uri_value:
        if 'uri' not in my_matcher.keys():
            my_matcher['uri']={
                uri_type:uri_value
            }
        else:
            my_matcher['uri'][uri_type]=uri_value

    match_list = []
    match_list.append(my_matcher)

    return match_list

def deploy_less_than_two(namespace,labels):
    myclient = client.AppsV1Api()
    results = myclient.list_namespaced_deployment(namespace=namespace,label_selector=labels)
    print(len(results.items))
    if len(results.items) < 2:
        return True
    else:
        return False

def get_dr_by_name(namespace,name):
    drs = client.CustomObjectsApi().list_namespaced_custom_object(group="networking.istio.io",
            version="v1alpha3", plural="destinationrules",namespace=namespace)
    dr = None
    for item in drs['items']:
        if item['metadata']['name'] == name:
            dr = item
            break
    return dr

def get_vs_by_name(namespace, vs_name):
    virtual_services = client.CustomObjectsApi().list_namespaced_custom_object(group="networking.istio.io",
                                                                               version="v1alpha3",
                                                                               plural="virtualservices",
                                                                               namespace=namespace)
    virtual_service = None
    for vs in virtual_services['items']:
        if vs['metadata']['name'] == vs_name:
            virtual_service = vs
            break
    return virtual_service

def get_gw_by_name(namespace, gw_name):
    gateways= client.CustomObjectsApi().list_namespaced_custom_object(group="networking.istio.io",
                                                                               version="v1alpha3",
                                                                               plural="gateway",
                                                                               namespace=namespace)
    gateway = None
    for gw in gateways['items']:
        if gw['metadata']['name'] == gw_name:
            gateway = gw
            break
    return gw

def update_virtual_service(vs_name, namespace, old_weight, new_weight):
    myclient = client.CustomObjectsApi()
    vs = get_vs_by_name(namespace, vs_name)
    if vs == None:
        return jsonify({"fail": "找不到该vs"})
    try:
        vs['spec']['http'][0]['route'][0]['weight'] = old_weight
        vs['spec']['http'][0]['route'][1]['weight'] = new_weight
        api_response = myclient.patch_namespaced_custom_object(group="networking.istio.io",
                                                               version="v1alpha3",
                                                               plural="virtualservices",
                                                               name=vs_name,
                                                               namespace=namespace,
                                                               body=vs)
        #
        status = "{}".format(api_response['spec']['http'])
    except Exception as e:
        print(e)
        return jsonify({"fail": "可能非生产环境，没有设置流量权重"})

    return jsonify({"update_status": status})

