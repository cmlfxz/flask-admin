from flask import current_app,g

from api.models.setting import Menu,Operation
from api.models.db import db
from api.models.user import User
from api.views.user import get_role_perm_menu,get_role_perm_operation
from api.models.transform import model_to_dict

def item_to_menu(item):
    menu = {}
    if isinstance(item,Menu):
        menu = {
            'menu_id': item.menu_id,
            'name': item.name,
            'to': item.route_name,
            'icon': item.icon,
            'enable': item.enable,
            'children': [],
        }
    return menu

# 
def item_to_operation(item):
    operation = {}
    if isinstance(item,Operation):
        operation = {
            'menu_id': item.id+ 100000,#(这里的菜单Id是根据功能id造出来的,作为菜单树的唯一标识)
            # 'menu_id': item.menu_id+10000,
            'name': item.name,
            'code': item.code,
        }
    return operation


def is_administrator(role_name):
    if role_name == "超管":
        return True
    return False

def get_user_role(username):
    user = db.session.query(User.role).filter(User.username==username).first()
    if user:
        role = user.role
        return role
    else:
        return None


def get_user_child_menu_by_pid(user_menu,pid):
    return user_menu.filter(Menu.parent_id == pid).all()
    # return db.session.query(Menu).filter(Menu.parent_id == pid).all()

def get_child_menu_by_pid(pid):
    return db.session.query(Menu).filter(Menu.enable==1).filter(Menu.parent_id == pid).all()

def get_operation_by_menu_id(mid):
    return db.session.query(Operation).filter(Operation.menu_id == mid).all()


# get_menu_tree调用,在编辑用户角色时
def get_clild_resource(pid,pmenu):
    '''
    :param pid: 父菜单id，可以查出所有的子菜单
    :param pmenu: 父菜单 pemnu.children = child_menu
    :return:
    '''
    menu_list = get_child_menu_by_pid(pid)
    if len(menu_list) > 0:
        for item in menu_list:
            menu = item_to_menu(item)
            if menu != None:
                pmenu['children'].append(menu)
                get_clild_resource(menu.get('menu_id'),menu)
    else:
        operation_list= get_operation_by_menu_id(pid)
        if operation_list:
            for op in operation_list:
                operation = item_to_operation(op)
                if operation != None:
                    pmenu['children'].append(operation)

# get_user_resource调用,在登录初始化阶段
def get_clild_menu(user_menu,pid,pmenu):
    '''
    :user_menu:  用户子菜单
    :param pid: 父菜单id，可以查出所有的子菜单
    :param pmenu: 父菜单 pemnu.children = child_menu
    :return:
    '''
    menu_list = get_user_child_menu_by_pid(user_menu,pid)
    if len(menu_list) > 0:
        for item in menu_list:
            menu = item_to_menu(item)
            if menu != None:
                pmenu['children'].append(menu)
                get_clild_menu(user_menu,menu.get('menu_id'),menu)
    else:
        return None

def get_user_menu(role_name):
    menu_tree = []
    user_menu  = None
    if is_administrator(role_name):
        user_menu  = db.session.query(Menu).filter(Menu.enable==1)
    else:
        role_perm_menu =  get_role_perm_menu(role_name)
        if  role_perm_menu:
            user_menu = db.session.query(Menu).filter(Menu.enable==1).filter(Menu.menu_id.in_(role_perm_menu))
        else:
            return None
    root_menus = user_menu.filter(Menu.parent_id==0).all()
    if len(root_menus) > 0:
        for item in root_menus:
            menu = item_to_menu(item)
            get_clild_menu(user_menu,menu.get('menu_id'),menu)
            menu_tree.append(menu)
    return menu_tree


def get_user_operation(role_name):
    op_list = []
    sql = None
    if is_administrator(role_name):
        sql = db.session.query(Operation)
    else:
        op_ids = get_role_perm_operation(role_name)
        if op_ids:
            sql =db.session.query(Operation).filter(Operation.id.in_(op_ids))
        else:
            return None
    rset = sql.all()
    if rset:
        for op in rset:
            op_list.append(model_to_dict(op))
    return op_list
