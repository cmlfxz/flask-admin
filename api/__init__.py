import os

from flask import Flask,g,Response,current_app
from flask_migrate import Migrate

import logging
from concurrent_log_handler import ConcurrentRotatingFileHandler
import re,json
import prometheus_client
from prometheus_client import Counter

from api.views.auth import auth
from api.views.blog import blog
from api.views.admin import admin
from api.views.site import site
from api.views.bucket import bucket
from api.views.cmdb import cmdb
from api.views.setting import setting
from api.views.pressure import pressure
from api.views.zabbix import zabbix
from api.views.es import es
from api.views.prom import prom
from api.views.aliyun import aliyun
from api.views.redis_manage import redis_manage
from api.views.user import user
from api.views.project import project
from api.views.file import file
from api.views.istio import istio
from api.views.base import base
from api.views.salt import salt
from api.views.releaseTask import releaseTask
from api.views.bug import bug
from api.views.requirement import requirement
from api.views.release import release
from api.views.version import version
from api.views.harbor import harbor
from api.views.public import public


import nacos,yaml
from api.models.db import db
request_total = Counter("http_requests_total","Total request cout of the service")

def setup_config(loglevel):
    if not os.path.exists('logs'):
        os.makedirs('logs')
    handler = ConcurrentRotatingFileHandler('logs/flask.log',mode='a',maxBytes=1024*1024,backupCount=5,)
    handler.setLevel(loglevel)

    logging_format = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(filename)s - %(funcName)s - %(lineno)d - %(message)s')

    handler.setFormatter(logging_format)
    return handler

def create_app():

    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object('config.default')
    env = ""
    try:
        # 本地环境获取ENV会抛异常
        env = os.environ["ENV"]
        object = ""
        if re.match(r'test', env, re.M | re.I):
            object = "config.test"
        elif re.match(r'prod', env, re.M | re.I):
            object = "config.prod"
        else:
            print("没有配置环境变量或者为本地开发环境")
            object = "config.dev"
        print("即将加载的配置是:{}".format(object))
        app.config.from_object(object)
    except Exception:
        print("获取不到ENV环境变量,加载开发配置")
        app.config.from_object("config.dev")

    @app.route('/metrics')
    def requests_count():
        return  Response(prometheus_client.generate_latest(request_total),mimetype='text/plain')


    @app.before_request
    def request_stat():
        request_total.inc()

    @app.errorhandler(404)
    def page_not_found(error):
        return json.dumps({"sorry":"404 Not Found"})

    #app初始化
    db.init_app(app)
    #日志初始化
    try:
        loglevel = app.config.get('LOG_LEVEL')
    except:
        loglevel = logging.WARNING
    handler = setup_config(loglevel)
    app.logger.addHandler(handler)

    #绑定数据库
    migrate = Migrate()
    migrate.init_app(app,db)

    #加载蓝图
    app.register_blueprint(auth)
    app.register_blueprint(blog)
    app.register_blueprint(admin)
    app.register_blueprint(site)
    app.register_blueprint(bucket)
    app.register_blueprint(cmdb)
    app.register_blueprint(setting)
    app.register_blueprint(pressure)
    app.register_blueprint(zabbix)
    app.register_blueprint(es)
    app.register_blueprint(prom)
    app.register_blueprint(aliyun)
    app.register_blueprint(redis_manage)
    app.register_blueprint(user)
    app.register_blueprint(project)
    app.register_blueprint(istio)
    app.register_blueprint(file)
    app.register_blueprint(base)
    app.register_blueprint(salt)
    #
    app.register_blueprint(releaseTask)
    app.register_blueprint(version)
    app.register_blueprint(bug)
    app.register_blueprint(requirement)
    app.register_blueprint(release)
    app.register_blueprint(harbor)
    app.register_blueprint(public)
    # app.register_blueprint(ini)
    app.add_url_rule('/',endpoint='index')

    #调试信
    app.logger.info(app.url_map)
    return app
