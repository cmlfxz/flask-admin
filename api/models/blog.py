from api.models.db import db
from datetime import datetime
from sqlalchemy.dialects import mysql
class Blog(db.Model):
    __tablename__ = 'blog'
    id = db.Column(db.Integer,primary_key=True)
    author_id  = db.Column(db.Integer)
    create_time = db.Column(db.DateTime,nullable=False, default=datetime.now)
    title = db.Column(db.String(100))
    content = db.Column(mysql.MEDIUMTEXT)
    tag = db.Column(db.String(500),comment="文章标签")
    view_count = db.Column(db.Integer,default=1,comment="访问次数")
    last_view_time = db.Column(db.DateTime,nullable=True, default=datetime.now)