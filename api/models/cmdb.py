from api.models.db import db
from datetime import datetime
from sqlalchemy import UniqueConstraint
import json

# 机房
# class Idc(db.Model):
#     __tablename__ = 'idc'
#     id = db.Column(db.Integer,primary_key=True)
#     name = db.Column(db.String(50))
#     location = db.Column(db.String(50),comment='地理位置') 
#     charge_person = db.Column(db.String(50),comment='负责人') 
#     create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)

#预检日志       
class ServerLog(db.Model):
    __tablename__ = 'server_log'
    id = db.Column(db.Integer,primary_key=True)
    msg  = db.Column(db.Text,comment='主机初始化日志')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)

# 更新日志
class ServerUpdateLog(db.Model):
    __tablename__ = 'server_update_log'
    id = db.Column(db.Integer,primary_key=True)
    batch = db.Column(db.Integer,comment='更新批次')
    msg  = db.Column(db.Text,comment='日志信息')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)


#系统账号 
class ServerUser(db.Model):
    __tablename__ = 'server_user'
    id = db.Column(db.Integer,primary_key=True)
    name  = db.Column(db.String(50),comment='系统账号')
    password  = db.Column(db.String(50),comment='账号密码')
    ssh_port = db.Column(db.Integer,comment='ssh端口')
    privite_key = db.Column(db.Text,comment='私钥')
    public_key  = db.Column(db.Text,comment='公钥')
    auth_type = db.Column(db.Integer,default=1,comment='登录校验方式1 password 2 key')
    cmd_filters  =  db.Column(db.Text,comment='需要过滤的命令')

class ServerInfo(db.Model):
    __tablename__ = 'server_info'
    id = db.Column(db.Integer,primary_key=True)
    hostname  = db.Column(db.String(50),comment='主机名')
    alias  = db.Column(db.String(50),comment='别名')
    private_ip = db.Column(db.String(32),comment='私有IP')
    public_ip = db.Column(db.String(32),comment='公有IP')
    ssh_port = db.Column(db.Integer,comment='SSH端口')
    cloud = db.Column(db.String(20),comment='云类型,私有云 2、阿里云 3、腾讯云 4、AWS 5、GoogleCloud 6、Azure 7、青云')
    idc = db.Column(db.String(50),comment='机房  阿里云-华东2')  
    region = db.Column(db.String(50),comment='区域，公有云特性 cn-shanghai')
    expire_date =  db.Column(db.DateTime,default=datetime.now,comment='到期时间')
    admin_user = db.Column(db.String(50),comment='管理账号')
    admin_password= db.Column(db.String(50),comment='管理密码')
    cert_id = db.Column(db.Integer,comment='公钥登录')
    tag = db.Column(db.String(200),comment='标签，物理主机 虚拟机 容器  ')
    
    system_info =  db.Column(db.String(200),comment='系统 版本 内核版本 例如 Linux Centos7  3.10.0')
    cpu =  db.Column(db.String(200),comment='CPU信息')
    cpu_load = db.Column(db.String(200),comment='CPU负载')
    memory =  db.Column(db.String(200),comment='内存信息')
    device_info = db.Column(db.String(500),comment='块设备信息')
    disk_info = db.Column(db.String(200),comment='磁盘使用情况')
    disk_mount_list = db.Column(db.String(500),comment='磁盘挂载信息')
    
    remark = db.Column(db.String(500),comment='备注，可以写隶属于什么业务，物理CPU个数，制造商 SN号 磁盘raid模式')
    status = db.Column(db.String(30),comment='状态1、在线 2、下线 3、停机')
    lock_status = db.Column(db.Integer,comment='状态1 加锁，更新跳过 ，其他可更新')
    create_time = db.Column(db.DateTime,default=datetime.now)
    update_time = db.Column(db.DateTime,default=datetime.now)
    __table_args__ = (
        UniqueConstraint('hostname','private_ip'),#主机和私有IP联合唯一
    )


# 可以保存服务器登录证书，网站https证书
# class ServerCert(db.Model):
#     __tablename__ = 'server_cert'  
#     id = db.Column(db.Integer,primary_key=True)
#     public_cert = db.Column(db.Text,comment='公钥')
#     private_key= db.Column(db.Text,comment='私钥')
#     ca_cert =  db.Column(db.Text,comment='CA证书')
#     expire_date =  db.Column(db.DateTime,nullable=False,default=datetime.now,comment='到期时间')
#     remark = db.Column(db.String(200),comment='备注 服务器证书 网站https证书（域名）')
    
#线上域名管理 
# class Domain(db.Model):
#     __tablename__ = 'domain'  
#     id = db.Column(db.Integer,primary_key=True)
#     name = db.Column(db.String(40),comment='域名www.baidu.com')
#     provider = db.Column(db.String(40),comment='提供商')
#     provider_url = db.Column(db.String(200),comment='提供商购买网址')
#     cert_id =  db.Column(db.Integer,comment='https证书ID，对应 server_cert表 id')
#     expire_date =  db.Column(db.DateTime,nullable=False,default=datetime.now,comment='到期时间')
#     remark = db.Column(db.String(200),comment='备注 服务器证书 网站https证书（域名）')


class Record(db.Model):
    __tablename__ = 'record'  
    id = db.Column(db.Integer,primary_key=True)
    host = db.Column(db.String(100),comment='主机地址')
    username = db.Column(db.String(100),comment='登录主机')
    filename = db.Column(db.String(100),comment='文件名')
    download_url = db.Column(db.String(300),comment='文件下载地址')
    start_time =  db.Column(db.String(100),comment='起始时间')
    end_time =  db.Column(db.String(100),comment='结束时间')
    