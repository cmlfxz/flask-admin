from api.models.db import db
from datetime import datetime
from sqlalchemy import UniqueConstraint
import json

from api.models.db import db
from datetime import datetime

# 压测设置
class PressureSetting(db.Model):
    __tablename__ = 'pressure_setting'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(50),comment="压测命名")
    ptype = db.Column(db.String(50),comment="压测类型  基准压测  虚拟压测（模仿真实环境）")
    target_detail = db.Column(db.String(1024),comment="服务器配置详情")
    tools =  db.Column(db.String(40),comment="压测工具")
    tools_detail =  db.Column(db.Text,comment="工具详情，使用介绍")
    other =  db.Column(db.Text,comment="额外信息")
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)



class PressureData(db.Model):
    __tablename__ = 'pressure_data'
    id = db.Column(db.Integer,primary_key=True,comment="数据id")
    name =  db.Column(db.String(100),comment="测试项目")
    pid = db.Column(db.Integer,comment="压测id")
    pname = db.Column(db.String(50),comment="压测名字")
    parallel = db.Column(db.String(30),comment="并发线程/用户数")
    request_num = db.Column(db.String(30),comment="请求次数")
    data_length = db.Column(db.String(50),comment="数据长度")
    other = db.Column(db.Text,comment="额外信息")
    pdata = db.Column(db.Text,comment="压测数据")
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
