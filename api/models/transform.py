from sqlalchemy.orm import class_mapper
from datetime import datetime 
from datetime import date

def model_to_dict(model):
    model_dict = {}
    for key, column in class_mapper(model.__class__).c.items():
        val = getattr(model, key, None)
        if isinstance(val,datetime):
            val = val.strftime('%Y-%m-%d %H:%M:%S')
            #val = val.strftime('%Y-%m-%d')
        elif isinstance(val, date):
            val = val.strftime("%Y-%m-%d")
        model_dict[column.name] = val
    return model_dict
