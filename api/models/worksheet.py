from api.models.db import db
from datetime import datetime
from sqlalchemy import UniqueConstraint



class Release(db.Model):
    __tablename__ = 'release'
    id = db.Column(db.Integer,primary_key=True)
    project  = db.Column(db.String(100))
    service_list = db.Column(db.String(100),comment='服务列表，以逗号分隔')
    release_type = db.Column(db.String(100),comment='发布类型1、灰度发布 2、蓝绿发布 3、滚动更新')  
    requirements= db.Column(db.String(100),comment='关联需求')
    bugs= db.Column(db.String(100),comment='bug')
    description = db.Column(db.Text,comment='发布内容')
    create_user = db.Column(db.String(100),comment='发布用户')
    review_user = db.Column(db.String(100),comment='评审人')
    review_result= db.Column(db.String(100),comment='评审结果')
    review_content  = db.Column(db.String(100),comment='评审内容')
    # review_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
    status = db.Column(db.String(100),comment='状态 1、待审核 2、待发布  3、发布失败 4、发布成功 5、重新修改 6、撤销发布 ')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)

class ReleaseTask(db.Model):
    '''任务表'''
    __tablename__ = 'release_task'
    id = db.Column(db.Integer, primary_key=True)
    project = db.Column(db.String(100),comment='发布项目名')
    env = db.Column(db.String(20),comment='发布环境')
    service = db.Column(db.String(100),comment='发布服务名')
    job_id = db.Column(db.Integer,comment="jenkins job Id")
    start_time = db.Column(db.DateTime,default=datetime.now,comment='开始时间')
    duration =  db.Column(db.Integer,comment="持续时间，以毫秒为单位")
    # end__time = db.Column(db.DateTime,default=datetime.now,comment='结束时间')
    status = db.Column(db.Integer, comment='状态 1、正在构建 2、构建失败  3、取消构建 99、构建成功  ')

# class GrayRelease(db.Model):
#     __tablename__ = 'gray_release'
#     id = db.Column(db.Integer,primary_key=True)
#     name = db.Column(db.String(100),unique=True,comment='发布名称')
#     project  = db.Column(db.String(100))
#     service = db.Column(db.String(100),comment='服务')
#     release_type = db.Column(db.Integer,comment='发布类型1、蓝绿发布 2、灰度发布  3、滚动更新') 
#     old_version =  db.Column(db.String(20),comment='老版本') 
#     old_weight =  db.Column(db.Integer,comment='老版本流量权重') 
#     new_version =  db.Column(db.String(20),comment='新版本') 
#     new_weight =  db.Column(db.Integer,comment='新版本流量权重') 
#     gray_traffic_type = db.Column(db.Integer,comment='灰度流量下发类型1、按流量下发 2、按请求内容下发') 
#     match =  db.Column(db.String(500),comment='按请求内容下发时，可以设置header uri等匹配项 匹配方式可以为完全匹配exact 正则匹配regex 前缀匹配prefix') 
#     release_status = db.Column(db.Integer,comment='21、蓝绿发布(新版接管流量) 22、旧版接管流量  31、 灰度发布 32、新版接收所有流量 33、旧版就收所有流量 99、发布完成（下线操作）') 
#     create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
#     update_time = db.Column(db.DateTime,nullable=False,default=datetime.now,comment='更新时间')
        
class Bug(db.Model):
    __tablename__ = 'bug'
    id = db.Column(db.Integer,primary_key=True)
    project  = db.Column(db.String(100),comment='项目')
    env = db.Column(db.String(20),comment='环境')
    service_list = db.Column(db.String(100),comment='服务/模块列表')
    require_id = db.Column(db.String(100),comment='关联需求id')
    name = db.Column(db.String(100),comment='bug名称')
    content = db.Column(db.Text,comment='bug内容')
    create_user = db.Column(db.String(100),comment='发布用户')
    status = db.Column(db.String(100),comment='状态 1、创建 2、开发处理 3、测试 4、已上线 5、已修复')
    deal_user = db.Column(db.String(100),comment='处理用户')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)


class Requirement(db.Model):
    __tablename__ = 'requirement'
    id = db.Column(db.Integer,primary_key=True)
    project  = db.Column(db.String(100),comment='项目')
    service_list = db.Column(db.String(100),comment='服务/模块列表')
    name = db.Column(db.String(100),comment='需求名称')
    content = db.Column(db.Text,comment='需求内容')
    create_user = db.Column(db.String(100),comment='创建用户')
    status = db.Column(db.String(100),comment='状态 1、创建 2、研发 3、测试 4、已上线 ')
    # audit_user = db.Column(db..String(100,comment='审批用户')
    research_user = db.Column(db.String(100),comment='开发用户')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)

class Version(db.Model):
    __tablename__ = 'version'
    id = db.Column(db.Integer,primary_key=True)
    project = db.Column(db.String(100),comment='project')
    content = db.Column(db.Text,comment='版本内容')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)