from api.models.db import db
from datetime import datetime


class Project(db.Model):
    __tablename__ = 'project'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(50))
    env_name = db.Column(db.String(50))
    leader = db.Column(db.String(50),comment="项目负责人,来自user表")
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
    create_user = db.Column(db.String(50))

    
class Env(db.Model):
    __tablename__ = 'env'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(50),unique=True)
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
    create_user = db.Column(db.String(50))
    clusters = db.Column(db.Text)


class Service(db.Model):
    __tablename__ = 'service'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(50))
    project= db.Column(db.String(50),comment='项目名')
    type = db.Column(db.Integer,comment='服务等级 0: 网关 1: 1级服务 2: 2级服务 只要有东西流量，就不能将服务定义为0、1 ，0、1两种服务只有南北流量')
    port = db.Column(db.Integer,comment="服务端口")
    git_url = db.Column(db.String(200),comment='git地址')
    has_pipeline = db.Column(db.Integer,default=0,comment="是否有流水线")
    is_multibranch = db.Column(db.Integer,default=0,comment="是否是多分支流水线")
    status =  db.Column(db.String(10),default='0',comment='状态,1 在线 0 下架')
    domain = db.Column(db.String(100),comment='域名')
    version = db.Column(db.Float,default=0.1,comment="服务的运行版本")
    replicas = db.Column(db.Integer,default=1,comment="副本数")
    create_user =  db.Column(db.String(40),default='admin',comment='创建用户')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
    update_time = db.Column(db.String(40),default=datetime.now,comment='更新时间')