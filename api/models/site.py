from api.models.db import db
from datetime import datetime
from sqlalchemy import UniqueConstraint

class Site(db.Model):
    __tablename__ = 'site'
    id = db.Column(db.Integer,primary_key=True)
    env = db.Column(db.String(20))
    name  = db.Column(db.String(100))
    link = db.Column(db.String(300))
    account = db.Column(db.String(100))
    password = db.Column(db.String(100))
    description = db.Column(db.Text)
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
    __table_args__ = (
        UniqueConstraint('env','name'),#环境和名字联合唯一
    )
