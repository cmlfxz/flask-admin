from api.models.db import db
from datetime import datetime


class Project(db.Model):
    __tablename__ = 'project'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(50))
    env_name = db.Column(db.String(50))
    leader = db.Column(db.String(50),comment="项目负责人,来自user表")
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
    create_user = db.Column(db.String(50))
