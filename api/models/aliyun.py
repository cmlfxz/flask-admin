from api.models.db import db
from datetime import datetime
class CloudAccount(db.Model):
    __tablename__ = 'cloud_account'
    id = db.Column(db.Integer,primary_key=True)
    cloud_type=db.Column(db.Integer,comment="1:aliyun,2:aws,3:gcp,4:tencent,5:huawei")
    access_key_id = db.Column(db.String(100),comment="Key")
    access_secret = db.Column(db.String(100),comment="Secret")
    tag = db.Column(db.String(100), comment="标签，一个公司同一个云厂商有多个账号，可以用来标识")
    create_time = db.Column(db.DateTime, nullable=False, default=datetime.now)
    def __repr__(self):
        return "cloud_type:{},access_key_id:{},access_secret:{}".format(self.cloud_type,self.access_key_id,self.access_secret)
class CloudKeyPair(db.Model):
    __tablename__ = 'cloud_key_pair'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(100),comment="Key的名字")
    content = db.Column(db.Text,comment="密钥对的内容(base64)")
    passphrase = db.Column(db.Text,comment="密钥密码")
    tag = db.Column(db.String(100), comment="aliyun,aws,gcp,tencent,huawei")
    create_time = db.Column(db.DateTime, nullable=False, default=datetime.now)
    def __repr__(self):
        return "name:{},content:{}".format(self.name,self.content)