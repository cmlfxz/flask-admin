from api.models.db import db
from datetime import datetime
from sqlalchemy import UniqueConstraint


# 创建=>(更新完dr,vs,gw)=>成功 2 => 回滚 => 成功 11  =>下线 99
#                       =>灰度 4         => 失败 12
#                       =>失败 3
class PublicTask(db.Model):
    __tablename__ = 'public_task'
    id = db.Column(db.Integer,primary_key=True)
    project  = db.Column(db.String(100))
    service_list = db.Column(db.String(100),comment='服务列表，以逗号分隔')
    service_details = db.Column(db.String(300),comment='服务列表，以逗号分隔')
    public_type = db.Column(db.Integer,comment='发布类型1、灰度发布 2、蓝绿发布 3、滚动更新 4、金丝雀发布')
    match = db.Column(db.String(500), comment='按请求内容下发时，可以设置header uri等匹配项 匹配方式可以为完全匹配exact 正则匹配regex 前缀匹配prefix')
    status = db.Column(db.Integer,comment='状态1、创建  2、发版成功（测试新版） 3、发版失败 4、灰度测试（金丝雀，灰度）  11、回滚成功 12、回滚失败  99、发布完成')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
    update_time = db.Column(db.DateTime, nullable=False, default=datetime.now, comment='更新时间')

class PublicDetail(db.Model):
    __tablename__ = 'public_detail'
    id = db.Column(db.Integer,primary_key=True)
    release_id = db.Column(db.Integer,comment='发布任务Id')
    project  = db.Column(db.String(100))
    service = db.Column(db.String(100),comment='服务')
    old_version =  db.Column(db.String(20),comment='老版本')
    old_weight =  db.Column(db.Integer,comment='老版本流量权重')
    new_version =  db.Column(db.String(20),comment='新版本')
    new_weight =  db.Column(db.Integer,comment='新版本流量权重')
    public_type = db.Column(db.Integer, comment='发布类型1、灰度发布 2、蓝绿发布 3、滚动更新 4、金丝雀发布')
    status = db.Column(db.Integer,comment='21、蓝绿发布(新版接管流量) 22、旧版接管流量  31、 灰度发布(含金丝雀) 32、新版接收所有流量 33、旧版就收所有流量 99、发布完成（下线操作）')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
    update_time = db.Column(db.DateTime,nullable=False,default=datetime.now,comment='更新时间')

class PublicLog(db.Model):
    __tablename__ = 'public_log'
    id = db.Column(db.Integer,primary_key=True)
    release_id = db.Column(db.Integer,comment='发布任务Id')
    message  = db.Column(db.Text,comment="日志内容")
    create_time = db.Column(db.DateTime, nullable=False, default=datetime.now)