from api.models.db import db
from datetime import datetime
import json

# elasticsearch集群管理表
class Es_Cluster(db.Model):
    __tablename__ = 'es_cluster'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(50))
    # 拆分成集群地址和端口
    # url = db.Column(db.String(50),comment='地址')
    cluster_addr =  db.Column(db.String(50),comment='地址')
    cluster_port = db.Column(db.Integer,comment='端口')
    account = db.Column(db.String(50),comment='es账号') 
    password = db.Column(db.String(50),comment='es密码') 
    remark = db.Column(db.String(200),comment='备注') 
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)


class Index(object):
    # def __init__(self,health,status,index,uuid,pri,rep,docs_count,docs_delete,store_size,pri_store_size):
    def __init__(self, **kwargs):
        self.health = kwargs.get('health')
        self.status =  kwargs.get('status')
        self.index =  kwargs.get('index')
        self.uuid =  kwargs.get('uuid')
        self.pri =  kwargs.get('pri')
        self.rep =  kwargs.get('rep')
        self.docs_count =  kwargs.get('docs_count')
        self.docs_delete =  kwargs.get('docs_delete')
        self.store_size =  kwargs.get('store_size')
        self.pri_store_size =  kwargs.get('pri_store_size')
    def to_json(self):
        return {
            'health':self.health,
            'status':self.status,
            'index':self.index,     
            'uuid':self.uuid,       
            'pri':self.pri,   
            'rep':self.rep,    
            'docs_count':self.docs_count,
            'docs_delete':self.docs_delete,     
            'store_size':self.store_size,       
            'pri_store_size':self.pri_store_size,   
        }

    def __str__(self):
        return "health: {} status: {} index: {} pri: {} rep: {} doc_count:{}".format(self.health,self.status,self.index, self.pri,self.rep,self.docs_count)

