from api.models.db import db
from datetime import datetime
from sqlalchemy import UniqueConstraint
import json


class Menu(db.Model):
    __tablename__ = 'menu'
    id = db.Column(db.Integer,primary_key=True)
    menu_id = db.Column(db.Integer,unique=True)
    parent_id = db.Column(db.String(50),default=0,nullable=False,comment='父菜单ID')
    parent = db.Column(db.String(50),comment='父菜单名字')
    name = db.Column(db.String(50),unique=True,comment='菜单名字')
    icon = db.Column(db.String(100),comment='图标名字')
    route_name = db.Column(db.String(200),comment='路由名字')
    route_path = db.Column(db.String(200),comment='路由地址')
    api = db.Column(db.String(200),comment='API地址')
    tag = db.Column(db.String(200),comment='标签')
    enable = db.Column(db.Integer,default=1,comment='1 启用，0 禁用')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)

# 操作功能
class Operation(db.Model):
    __tablename__ = 'operation'
    id = db.Column(db.Integer,primary_key=True)
    menu_id = db.Column(db.Integer,comment='菜单id')
    name = db.Column(db.String(50),comment='功能名称')
    code = db.Column(db.String(50),unique=True,comment='编码')
    create_time = db.Column(db.DateTime,nullable=False,default=datetime.now)
