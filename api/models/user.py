from api.models.db import db
from flask import current_app
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer,SignatureExpired

class User(db.Model):
    __tablename__='user'

    id = db.Column(db.Integer,primary_key=True)
    username = db.Column(db.String(40))
    password = db.Column(db.String(128))
    group =  db.Column(db.String(16),comment='用户组')
    phone =  db.Column(db.String(16),comment='电话')
    email = db.Column(db.String(64),comment='邮件')
    qq = db.Column(db.String(20),comment='QQ')
    weixin = db.Column(db.String(20),comment='微信')
    dingding = db.Column(db.String(48),comment='钉钉')
    remark = db.Column(db.String(256),comment='备注')
    role  = db.Column(db.String(40),comment='角色')
    
    def generate_auth_token(self,expiration = 30*86400):
        key = current_app.config['SECRET_KEY']
        serializer = Serializer(secret_key=key,expires_in=expiration,algorithm_name='HS256')
        return serializer.dumps({'id':self.id})
    
    #静态方法可以不输入self作为参数
    @staticmethod
    def verify_auth_token(token):
        key = current_app.config['SECRET_KEY']
        serializer = Serializer(secret_key=key,algorithm_name='HS256')
        try:
            data = serializer.loads(token)
        except SignatureExpired:
            print("签名已过期")
            return 401
        except BaseException:
            print("无效签名")
            return 400
        user = User.query.get(data['id'])
        current_app.logger.info(user)
        
        return user
    
#角色表
class Role(db.Model):
    __tablename__='role'

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(40),comment='角色名称')
    perm_group = db.Column(db.Text,comment='权限组')
    perms  = db.Column(db.Text,comment='权限列表,是一个已权限id组成的字符串,逗号分隔,只保存key')
    perms_all = db.Column(db.Text,comment='所有权限id列表,保存key和半开key,可以用这个构建出树状结构')
    

#用户id和系统用户对应的表
class Server_User_User(db.Model):
    __tablename__='server_user_user'

    id = db.Column(db.Integer,primary_key=True)
    user_id = db.Column(db.Integer,comment='用户ID')
    server_user_id = db.Column(db.Integer,comment='登录服务器的系统账号ID，非root')

