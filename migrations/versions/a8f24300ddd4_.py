"""empty message

Revision ID: a8f24300ddd4
Revises: 7656a77a031b
Create Date: 2020-12-28 22:14:31.296955

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a8f24300ddd4'
down_revision = '7656a77a031b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('es_cluster',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=50), nullable=True),
    sa.Column('url', sa.String(length=50), nullable=True, comment='地址'),
    sa.Column('account', sa.String(length=50), nullable=True, comment='es账号'),
    sa.Column('password', sa.String(length=50), nullable=True, comment='es密码'),
    sa.Column('create_time', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('es_cluster')
    # ### end Alembic commands ###
