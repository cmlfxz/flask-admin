"""empty message

Revision ID: 547ce88bf952
Revises: 4169d26b4b96
Create Date: 2020-10-26 21:36:18.658619

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '547ce88bf952'
down_revision = '4169d26b4b96'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('menu', sa.Column('parent', sa.String(length=50), nullable=True, comment='父菜单名字'))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('menu', 'parent')
    # ### end Alembic commands ###
