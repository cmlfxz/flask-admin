"""empty message

Revision ID: c9c21ec479b3
Revises: 79d00af66bcb
Create Date: 2021-03-03 16:38:36.860191

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c9c21ec479b3'
down_revision = '79d00af66bcb'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('service', sa.Column('domain', sa.String(length=100), nullable=True, comment='域名'))
    op.add_column('service', sa.Column('update_time', sa.String(length=40), nullable=True, comment='更新时间'))
    op.add_column('service', sa.Column('version', sa.Integer(), nullable=True, comment='服务的运行版本'))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('service', 'version')
    op.drop_column('service', 'update_time')
    op.drop_column('service', 'domain')
    # ### end Alembic commands ###
