"""empty message

Revision ID: 20f3044e34ce
Revises: e33bc8548d79, f61d1f9623b7
Create Date: 2020-10-16 22:29:51.865192

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '20f3044e34ce'
down_revision = ('e33bc8548d79', 'f61d1f9623b7')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
