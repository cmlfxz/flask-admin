"""empty message

Revision ID: e89b4d2268fe
Revises: 0df3a8b8381b
Create Date: 2021-02-28 17:14:51.511312

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e89b4d2268fe'
down_revision = '0df3a8b8381b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('project', sa.Column('leader', sa.String(length=50), nullable=True, comment='项目负责人,来自user表'))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('project', 'leader')
    # ### end Alembic commands ###
