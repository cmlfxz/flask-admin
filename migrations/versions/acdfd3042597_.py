"""empty message

Revision ID: acdfd3042597
Revises: ca5303a0c971
Create Date: 2020-10-26 12:24:42.371116

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'acdfd3042597'
down_revision = 'ca5303a0c971'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('menu',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('parent_id', sa.String(length=50), nullable=True, comment='父菜单ID'),
    sa.Column('name', sa.String(length=50), nullable=True, comment='菜单名字'),
    sa.Column('route_name', sa.String(length=200), nullable=True, comment='路由名字'),
    sa.Column('route_path', sa.String(length=200), nullable=True, comment='路由路径'),
    sa.Column('api', sa.String(length=200), nullable=True, comment='关联的API'),
    sa.Column('tag', sa.String(length=200), nullable=True, comment='标签'),
    sa.Column('create_time', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.alter_column('server_update_log', 'batch',
               existing_type=mysql.INTEGER(display_width=11),
               comment='更新批次',
               existing_nullable=True)
    op.alter_column('server_update_log', 'create_time',
               existing_type=mysql.DATETIME(),
               nullable=False)
    op.alter_column('server_update_log', 'msg',
               existing_type=mysql.TEXT(),
               nullable=True,
               comment='日志信息')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('server_update_log', 'msg',
               existing_type=mysql.TEXT(),
               nullable=False,
               comment=None,
               existing_comment='日志信息')
    op.alter_column('server_update_log', 'create_time',
               existing_type=mysql.DATETIME(),
               nullable=True)
    op.alter_column('server_update_log', 'batch',
               existing_type=mysql.INTEGER(display_width=11),
               comment=None,
               existing_comment='更新批次',
               existing_nullable=True)
    op.drop_table('menu')
    # ### end Alembic commands ###
