"""empty message

Revision ID: c4791a741736
Revises: 56d289215255
Create Date: 2021-04-05 15:32:29.552948

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c4791a741736'
down_revision = '56d289215255'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('server_user', sa.Column('cmd_filters', sa.Text(), nullable=True, comment='需要过滤的命令'))
    op.add_column('server_user', sa.Column('login_auth_type', sa.Integer(), nullable=True, comment='登录校验方式1 password 2 key'))
    op.add_column('server_user', sa.Column('privite_key', sa.Text(), nullable=True, comment='私钥'))
    op.add_column('server_user', sa.Column('public_key', sa.Text(), nullable=True, comment='公钥'))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('server_user', 'public_key')
    op.drop_column('server_user', 'privite_key')
    op.drop_column('server_user', 'login_auth_type')
    op.drop_column('server_user', 'cmd_filters')
    # ### end Alembic commands ###
