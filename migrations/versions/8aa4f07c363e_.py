"""empty message

Revision ID: 8aa4f07c363e
Revises: 3d0af885d67d
Create Date: 2021-01-22 18:48:48.989784

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '8aa4f07c363e'
down_revision = '3d0af885d67d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('cloud_account',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('cloud_type', sa.Integer(), nullable=True, comment='1:aliyun,2:aws,3:gcp,4:tencent,5:huawei'),
    sa.Column('acceess_key_id', sa.String(length=100), nullable=True, comment='Key'),
    sa.Column('access_secret', sa.String(length=100), nullable=True, comment='Secret'),
    sa.Column('create_time', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.drop_table('yun_account')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('yun_account',
    sa.Column('id', mysql.INTEGER(display_width=11), autoincrement=True, nullable=False),
    sa.Column('acceess_key_id', mysql.VARCHAR(length=100), nullable=True, comment='Key'),
    sa.Column('access_secret', mysql.VARCHAR(length=100), nullable=True, comment='Secret'),
    sa.Column('create_time', mysql.DATETIME(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    mysql_default_charset='utf8',
    mysql_engine='InnoDB'
    )
    op.drop_table('cloud_account')
    # ### end Alembic commands ###
