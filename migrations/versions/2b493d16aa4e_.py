"""empty message

Revision ID: 2b493d16aa4e
Revises: e70955683390
Create Date: 2021-05-21 23:58:45.278158

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '2b493d16aa4e'
down_revision = 'e70955683390'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('task_manage', 'cron',
               existing_type=mysql.VARCHAR(length=100),
               comment='cron表达式',
               existing_comment='标签',
               existing_nullable=True)
    op.alter_column('task_manage', 'job_id',
               existing_type=mysql.INTEGER(display_width=11),
               type_=sa.String(length=300),
               existing_comment='作业Id',
               existing_nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('task_manage', 'job_id',
               existing_type=sa.String(length=300),
               type_=mysql.INTEGER(display_width=11),
               existing_comment='作业Id',
               existing_nullable=True)
    op.alter_column('task_manage', 'cron',
               existing_type=mysql.VARCHAR(length=100),
               comment='标签',
               existing_comment='cron表达式',
               existing_nullable=True)
    # ### end Alembic commands ###
