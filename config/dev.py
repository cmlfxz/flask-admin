import  os
import logging
# from redis import Redis
from datetime import timedelta

SECRET_KEY = b'feaf0ca3870de645'

REDIS_HOST='localhost'
REDIS_PORT=6689
REDIS_PASSWORD='redis_user'
# SESSION_REDIS =  Redis(host=REDIS_HOST,port=REDIS_PORT)

SEND_FILE_MAX_AGE_DEFAULT = timedelta(seconds=1)

DIALECT = 'mysql'
MYSQL_DRIVER = 'pymysql'
MYSQL_USERNAME = 'dev_user'
MYSQL_PASSWORD = 'abc123456'
MYSQL_HOST = 'localhost'
MYSQL_PORT = '52100'
MYSQL_DATABASE = 'tutorial'

SQLALCHEMY_DATABASE_URI = '{}+{}://{}:{}@{}:{}/{}?charset=utf8'.format(
    DIALECT,MYSQL_DRIVER,MYSQL_USERNAME,MYSQL_PASSWORD,MYSQL_HOST,MYSQL_PORT,MYSQL_DATABASE
)


LOG_LEVEL = logging.DEBUG